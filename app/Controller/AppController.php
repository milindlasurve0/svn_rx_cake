<?php
/**
 * 
 * 
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	function beforeFilter() {
		if(!(isset($_REQUEST["noredirect"]) && $_REQUEST["noredirect"]== "yes" )){
		$agent = $_SERVER['HTTP_USER_AGENT'];
		$type = "";
		if(stripos($agent,"Android")){
			$type = "android";
		}
		else if(stripos($agent,"Windows Phone 8")){
			$type = "win8";
		}
		else if(stripos($agent,"Windows Phone")){
			$type = "win";
		}
		else if(stripos($agent,"MIDP")){
			$type = "java";
		}
		else if(stripos($agent,"iPhone")){
			$type = "iphone";
		}
		
		if(in_array($type,array("android","win8","win","java"))){
			//$this->redirect('http://'.$_SERVER['HTTP_HOST'].'/download.php?type='.$type);
		}
		else if(in_array($type,array("iphone"))){
			$this->redirect('https://rx.pay1.in');
		}
		}
	}
	
		
}
