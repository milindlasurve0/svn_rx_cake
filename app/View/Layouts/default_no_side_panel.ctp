<?php
/**
 * 
 * @author        Vinay Rathore
 * @package       app.View.Layouts

 */
?>
<!DOCTYPE html>
<html>
    <head>
		
        <?php echo $this->Html->charset(); ?>
        <title id="page_title">
           Panel for Retailers | Retail Network
        </title>
		<meta name="description" content="Pay1 has more than 500 Channel Partner across the country. India's first and only Cash-To-Digital retail network
			  "></meta>
        <LINK REL="SHORTCUT ICON" HREF="/images/favicon.ico?322"/>
        <?php
        //echo $this->Html->meta('icon');

        //echo $this->Html->css('cake.generic');
        echo $this->Html->css('bootstrap.min.css');
        echo $this->Html->css('snap.css');
        echo $this->Html->css('style.css');
        echo $this->Html->css('jquery-data-tables.css');

        //echo $this->fetch('meta');
        //echo $this->fetch('css');
        //echo $this->fetch('script');
        ?>
        <?php
        echo $this->Html->script('jquery.js');
        //echo $this->Html->script("http://code.jquery.com/ui/1.10.4/jquery-ui.js");
        echo $this->Html->script('bootstrap.min.js');
        echo $this->Html->script('script.js');
        echo $this->Html->script('plugin.js');
        echo $this->Html->script('snap.js');
        echo $this->Html->script('bootstrap-datepicker.js');
        echo $this->Html->script('jquery-data-tables.js');
	    

        echo $this->Html->script('rx/pay1.config.js?2');
        echo $this->Html->script('rx/pay1.core.js');
        echo $this->Html->script('rx/pay1.auth.js');
        echo $this->Html->script('rx/pay1.reports.js?2');
		
        //echo $this->Html->js('bootstrap.min.js');
        //echo $this->Html->js('script.js'); 
        //echo $this->Html->js('plugin.js'); 
        //echo $this->Html->js('snap.js'); 
        ?>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54897637-5', 'auto');
  ga('send', 'pageview');

		</script>
    </head>
    <!--<body role="document" style="">
        <div id="container">
            <div id="header">
                <h1><?php //echo $this->Html->link($cakeDescription, 'http://cakephp.org');  ?></h1>
            </div>
            <div id="content">
    
    <?php //echo $this->Session->flash();  ?>
    
    <?php //echo $this->fetch('content');  ?>
            </div>
            <div id="footer">
    <?php /* echo $this->Html->link(
      $this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
      'http://www.cakephp.org/',
      array('target' => '_blank', 'escape' => false)
      ); */
    ?>
            </div>
        </div>
    <?php //echo $this->element('sql_dump');  ?>
    </body>-->

    <body role="document" style="">
        <?php echo $this->element('Common/include-header'); ?>
        <section>
            <div id="content" class="container" style="overflow:hidden">    
                <?php echo $this->fetch('content'); ?>
            </div>
        </section>




        <?php echo $this->element('Common/include-modal-login'); ?>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->


 <?php echo $this->element('Common/include-footer'); ?>
    </body>

</html>
