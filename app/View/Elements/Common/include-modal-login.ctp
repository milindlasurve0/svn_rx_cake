<div  class="modal fade login" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                <form id="login_modal" class="form-signin" role="form">
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <input name="mobile_no" id="mobile_no" type="number" class="form-control" placeholder="Mobile No." pattern="\d{10,10}" title="10 Digit Mobile No"  autofocus value="">
                    <input name="pin" id="pin" type="password" class="form-control" pattern=".{4,25}" title="4 Characters Minimum" placeholder="Password"  value=""/>
                    <button id="login_modal_submit" class="btn btn-lg btn-primary btn-block login-btn" type="submit" data-loading-text="Loading..." >Sign in</button>
                </form>
            </div>
        </div>
    </div>
</div>

 <!--Send & Resend OTP Modal--> 
<div  class="modal fade otp" id="otp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ShowOkResendButton()">&times;</button>
                <h4 class="modal-title">New Device/Browser detected</h4>
            </div>
            <div class="modal-body" style="width: 342px; padding-bottom: 59px;">
                    <h2 class="form-signin-heading">Enter OTP sent on your mobile number</h2>
                    <input name="OtpMobNum" id="OtpMobNum" type="hidden" class="form-control" title="OTPMobNum">
                    <input name="otpnumber" id="otpnumber" type="text" class="form-control" maxlength="6" placeholder="OTP" pattern="\d{10,10}" title="OTP"  autofocus value="">        
                    
                    <a name="ResendOtp_modal_submit" id="ResendOtp_modal_submit" href="#" onclick="return ResendOtpforUserDeviceMapping();" style="float: right; padding-right: 7px; visibility: hidden;">Get OTP on Call</a>
                    
                    <button name="VerifyOtp_modal_submit" id="VerifyOtp_modal_submit" class="btn btn-lg btn-primary btn-block login-btn" onclick="VerifyOtpforUserDeviceMapping()"  data-loading-text="Loading..." style="float: left; margin-top: 12px;">OK</button>
                    <!--<button name="ResendOtp_modal_submit" id="ResendOtp_modal_submit" class="btn btn-lg btn-primary btn-block otp-btn" onclick="ResendOtpforUserDeviceMapping()"  data-loading-text="Loading..." style="float: right; margin-top: 12px; visibility: hidden;">Get OTP on Call </button>-->
            </div>
            <div class="modal-footer" style="padding-left: 41px;padding-right: 35px;">
<!--                <a name="SetNewMobileNum" id="SetNewMobileNum" href="#" onclick="return SetNewMobileNumforUserDeviceMapping();">
                    <small><b style="padding-right: 30px;">Please contact customer care on 022 - 67242288 in case you have not received OTP.</b></small></a>-->

                    <p>Please contact customer care on </p>
                    <center><b> 022-67242288 </b></center>
                    <p>in case you have not received OTP.</p>
            </div>
        </div>
    </div>
</div>
<!--End Send & Resend OTP Modal-->  

<!-- Modal -->
<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="failed_heading"><img src="<?php echo Configure::read('AWS_URL'); ?>/images/transactionSuccess.png">Transaction Successful</h4>
            </div>
            <div class="modal-body" >
                <span id="success_description">
                </span>
                <div class="form-inline">
                    <input type="submit" class="form-control default-btn" value="Ok" data-dismiss="modal" aria-hidden="true">
                </div>
            </div>
        </div>
    </div>
</div>   

<!-- Modal -->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="success_heading"><img src="<?php echo Configure::read('AWS_URL'); ?>/images/transactionFailed.png">Transaction Failed</h4>
            </div>
            <div class="modal-body" >
                <span id="failed_description">
                </span>
                <div class="form-inline">
                    <input type="submit" class="form-control default-btn" value="Ok" data-dismiss="modal" aria-hidden="true">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Verify OTP Modal -->
<div class="modal fade" id="verify_otp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="success_heading"><img src="<?php echo Configure::read('AWS_URL'); ?>/images/transactionFailed.png">OTP sent</h4>
            </div>
            <div class="modal-body" >
                <span id="verify_otp_description">
                </span>
                <div class="form-inline">
                    <input type="submit" class="form-control default-btn" value="Ok" data-dismiss="modal" aria-hidden="true">
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="collect_cash" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-body" style="height:400px;">
      <button type="button" class="close" data-dismiss="modal"><img src="<?php echo Configure::read('AWS_URL'); ?>/img/ic_close.png"/></button>
      <div class="row">
       <span style="position: absolute;top: 42px;left: 213px;font-size: 31px;" id='pg_mobile'>9876541230</span>
       </div>
       <div class="row" style="margin-top: 36px;color: #636363;font-size: 16px;">
	       <div class="col-lg-4"></div>
	       <div class="col-lg-2">REF ID</div>
	       <div class="col-lg-2" id="pg_ref_id">10042278</div>
	       <div class="col-lg-4"></div>
       </div>
        <div class="row" style="margin-top: 20px;color: #636363;font-size: 15px;">
	       <div class="col-lg-4"></div>
	       <div class="col-lg-2" id="pg_image">REF ID</div>
	       <div class="col-lg-2" id="pg_partner">BOOKMYSHOW</div>
	       <div class="col-lg-4"></div>
       </div>
       <div class="row" style="margin-top: 36px;font-size: 16px;">
	       <div class="col-lg-2"></div>
	       <div class="col-lg-4" style="font-weight:600;">AMOUNT TO BE PAID BY CUSTOMER</div>
	       <div class="col-lg-3" style="font-weight:bold;" id="pg_amount">Rs. 5000</div>
	       <div class="col-lg-3"></div>
       </div>
       <div class="row">
       	<button id="pg_collect" type="submit" style="position: absolute;top: 250px;left: 240px;color: white;background-color: #22c064;" class="btn">CASH COLLECTED</button>
       </div>
       <div class="row" style="position: absolute;top: 296px;left: 217px;font-size: 10px;font-weight: 600;" id="pg_expiry">
       	The REF ID will expire after 7pm 
       </div>
       <div class="row" style="position: absolute;top: 342px;left: 200px;font-size: 13px;">
       	Note: This transaction will not be refunded.
       </div>
      </div>
    </div>

  </div>
</div>

<div id="collected" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    
      <div class="modal-body" style="height:400px;">
      <button type="button" class="close" data-dismiss="modal"><img src="<?php echo Configure::read('AWS_URL'); ?>/img/ic_close.png"/></button>
      <span style="position: absolute;top: 100px;left: 200px;font-size: 21px;color: #22C064;font-weight: 600;">CONGRATULATIONS!</span>
      <span style="position: absolute;top: 180px;left: 188px;">Your transaction was successful.</span>
		<span style="position: absolute;top: 240px;left: 200px;font-size: 15px;">Thank you for collecting cash.</span>
		<button type="button" style="position: absolute;top: 300px;left: 272px;color: white;background-color: #22c064;" class="btn" data-dismiss="modal">CLOSE</button>
  </div>
</div>
 </div>
</div>

<script>
         
        //Resend OTP on User Mobile Number for for User Device Mapping
        function ResendOtpforUserDeviceMapping(){
            var resendOTP = true;
            var lat = PayOne.core.cookie.get("lat");  //User Latitude
            var lng = PayOne.core.cookie.get("lng");  //User Longitude
            var mobile = document.getElementById("OtpMobNum").value;
            var num_len = mobile.toString().length;
            if(num_len != 10){
             alert("Your Mobile Number is doesn't exist !!");
             document.getElementById("otpnumber").value = '';
            }else{
             alert("OTP sent to your Mobile number !!");
             document.getElementById('ResendOtp_modal_submit').style.visibility='hidden'; // hide Ok Button
             setTimeout(function(){ 
              document.getElementById('ResendOtp_modal_submit').style.visibility='visible'; // show Resend Button
             }, 10000);
              var status = PayOne.auth.login.submit(lat,lng,resendOTP);
              if(status == false){
              document.getElementById("otpnumber").value = '';  
             }
            }
        }

        //Verify User OTP of Mobile Number for User Device Mapping
        function VerifyOtpforUserDeviceMapping(){
            var mobile = document.getElementById("OtpMobNum").value;
            var otp = document.getElementById("otpnumber").value;
            var n = otp.toString().length;
            if(n != 6){
             $('#verify_otp_description').html("Authentication failure. OTP did not match.");
             $('#verify_otp').modal("show");
             document.getElementById("otpnumber").value = '';
            }else{
            //Calling verifyOTPofUserDevice() of pay1.auth.js to Verify User OTP for User Device Mapping
            var status = PayOne.auth.verifyOTPofUserDevice(mobile,otp);
             if(status == false){
              document.getElementById("otpnumber").value = '';  
             }
            }
        }
        
        //Show Ok and hide Resend button for User Device Mapping
        function ShowOkResendButton(){
            document.getElementById('VerifyOtp_modal_submit').style.visibility='visible'; // show
            document.getElementById('ResendOtp_modal_submit').style.visibility='hidden'; // hide
        }
        
 
</script>