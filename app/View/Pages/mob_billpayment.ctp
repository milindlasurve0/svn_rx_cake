<script>
    var deviceUuid = "";
    $(function() {
    
        $('#mobileBillPayment').on('submit', function (event) {
            event.preventDefault();
            mobBillPayment(1);
        });
        if($("#operatorType").val() == "mobile"){     
            $.each(PayOne.config.operators.mobBillPayment , function(i ,v){
                if(v.id == $("#operatorId").val() && !v.spcl ){
                    $(".stvOrTopUp_radio2class").hide();
                }            
            });
        }
        function mobileValidate(y){
            var y = y.trim();
            if(y == ""){alert("Enter mobile number");return 0;
            }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
            }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
            }
            return 1;
        }

        function amtValidate(y) {
            var err = '';
            var y = y.trim();
            if(y == ""){alert("Plese enter proper amount");return 0;
            }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
            }
            return 1;
        }

        function mobBillPayment(cnfFlag){
            // prevPage = fromLocation;

            var mobileNo = $("#mobileNo").val().trim();
            var rechargeAmt = $("#rechargeAmt").val().trim();
            if(mobileValidate(mobileNo) == '0') return;	
            if(amtValidate(rechargeAmt) == '0')return;

            var stv = $("input[name='stvOrTopUp_radio']:checked").val();

            var service_charge = rechargeAmt * 0.005;
            service_charge = (service_charge < 5) ? 5 : service_charge;
            if(cnfFlag == 1){
                var cnf = "Operator: "+$('#operatorName').val()
                +"\nNumber: "+mobileNo
                +"\nAmount: Rs."
                +rechargeAmt+" + " + Math.round(service_charge * 1.145 * 100) / 100 + "(0.5% + service charge)\n";
                var r=confirm(cnf+"Press OK to confirm");
                if(r==false){
                    return false;
                }
            }
            var oprID = parseInt($("#operatorId").val()) - 35;
            $('#mobBillPaymentSubmit').button('loading');
            $.ajax({
                url: PayOne.config.urls.api,
                type: "POST",
                data: {
                    method:"mobBillPayment",
                    device_type:"web",
                    mobileNumber:mobileNo,
                    operator:oprID,
                    subId:mobileNo,
                    amount:rechargeAmt,/*circle:$('#mob-rec-cir').val(),*/
                    special:stv,
                    type:"flexi"
                    
                },
                dataType: 'jsonp',
                jsonp: 'root',
                timeout: 50000,
                success: function(data, status){ 
                    $('#mobBillPaymentSubmit').button('reset');
                    $.each(data, function(i,item){				
                        if(item.status == 'failure'){                       
                            PayOne.core.failChk(item.code,item.description,true);
                       
                        }else{
                            if(item.status == 'success'){
                                var desc = 'Recharge request sent successfully';
                                $('#success_description').html(desc);
                                $('#success').modal("show");
                                //alert($('#mobileNo').attr('value', ""));
                                $("#mobileNo").val("");
                                $("#rechargeAmt").val("");
                                //$('#MRODd').attr('value', '');
                                //$('#MROtitle').html('');
                                //showHide('page1','navMobile');
                                $('#retailerBal').html(item.balance);
                                PayOne.core.cookie.set("balance", item.balance, 1);
                                setTimeout(function(){
                                   window.location="/mobile_billpayment";  //redirect to main page
                                }, 3000);
                            }
                        }
                    });
                },
                error: function(){
                    $('#mobBillPaymentSubmit').button('reset');
                    //$('#rec_now_loader').html(tmp);
                }
            });
        }
    
    });
</script>

<div class="breadcrumb"><a href="/<?php echo $type; ?>"><?php echo ucfirst($type); ?></a> &gt; <?php echo $operatorName; ?> &gt; Recharge</div>
<form role="form" id="mobileBillPayment">
    <div class="form-group">
        <input type="hidden" name="operatorId" id="operatorId" value="<?php echo $operatorId; ?>" />
        <input type="hidden" name="operatorName" id="operatorName" value="<?php echo $operatorName; ?>" />
        <input type="hidden" name="operatorType" id="operatorType" value="<?php echo $type; ?>" />
        <?php if ($type == "mobile") { ?>
            <input style="width: 40px ; float:left;" type="radio" class="form-control" id="stvOrTopUp_radio1" name="stvOrTopUp_radio" value="0" placeholder="" checked="true"/><label for="stvOrTopUp_radio1" style="float:left;">Top Up</label>
            <input style="width: 40px; float:left;" type="radio" class="form-control stvOrTopUp_radio2class" id="stvOrTopUp_radio2" name="stvOrTopUp_radio" value="1" placeholder=""/><label for="stvOrTopUp_radio2" class="stvOrTopUp_radio2class">STV</label>
        <?php } ?>
    </div>
    <div class="form-group">
        <label for="mobileNo"  for="mobileNo">Enter Postpaid Mobile Number</label>
        <input type="text" class="form-control" id="mobileNo" placeholder="" pattern="^\d{10,10}" title="10 Digit Mobile No" />
    </div>
    <div class="form-group">
        <label for="rechargeAmt">Enter Amount</label>
        <input type="text" class="form-control" id="rechargeAmt" placeholder="" pattern="^\d{1,5}(\.\d{1,2})?$" title="Amount should be a no"/>
    </div>                        

    <div class="checkPlanForm">
        <div class="form-group">
            <label for="mobileCircle">Select Circle</label>
            <select class="form-control" id="mobileCircle">
<!--                <option></option>
                <option>Airtel</option>
                <option>Vodafone</option>
                <option>Tata Docomo</option>
                <option>MTS</option>-->
            </select>
        </div>

        <div class="form-group">
            <label for="mobileCatagory">Select Catagory</label>
            <select class="form-control" id="mobileCatagory">
<!--                <option></option>
                <option>Airtel</option>
                <option>Vodafone</option>
                <option>Tata Docomo</option>
                <option>MTS</option>-->
            </select>
        </div>

        <div class="form-group">
            <label for="mobilePlan">Select Plan</label>
            <select class="form-control" id="mobilePlan">
<!--                <option></option>
                <option>Airtel</option>
                <option>Vodafone</option>
                <option>Tata Docomo</option>
                <option>MTS</option>-->
            </select>
        </div>
    </div>
    <!--<p class="selectedPlan"></p>
    <p><a href="#" class="checkPlan">Check Your Plans &gt;&gt;</a></p> -->
    <button id="mobBillPaymentSubmit" data-loading-text="Processing..." type="submit" class="form-control default-btn">Proceed</button>
</form>

