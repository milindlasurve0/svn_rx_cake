<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>
mapLocation = {lat:PayOne.core.cookie.get("lat"),lng:PayOne.core.cookie.get("lng")};
 
$(function(){
    
    
    $("#addLine").val(PayOne.core.cookie.get("street"));
    $("#area").val(PayOne.core.cookie.get("area"));
    $("#city").val(PayOne.core.cookie.get("city"));
    $("#state").val(PayOne.core.cookie.get("state"));
    $("#pincode").val(PayOne.core.cookie.get("pincode"));
    
    function geoSuccess(position){
        var lat = (PayOne.core.cookie.get("lat")==null || PayOne.core.cookie.get("lat") == 0 || PayOne.core.cookie.get("lat") == "") ? position.coords.latitude :  PayOne.core.cookie.get("lat") ;
        var lng = (PayOne.core.cookie.get("lng")==null || PayOne.core.cookie.get("lng") == 0 || PayOne.core.cookie.get("lng") == "") ? position.coords.longitude :  PayOne.core.cookie.get("lng") ;
        mapLocation = {lat:lat,lng:lng};
          var latlng = new google.maps.LatLng(lat, lng);
          var myOptions = {
            zoom: 15,
            center: latlng,
            mapTypeControl: false,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

          var marker = new google.maps.Marker({
              position: latlng, 
              map: map, 
              title:"You are here! (at least within a "+position.coords.accuracy+" meter radius)"
          });
          google.maps.event.addListener(map, 'click', function(event) {
              mapLocation.lat = event.latLng.k;
              mapLocation.lng = event.latLng.A;
              marker.setPosition(mapLocation);
          });
        
        //--------------------------------
    }
    
    function geoError(msg){
        google.maps.event.addDomListener(window, 'load', initialize);
    }
     navigator.geolocation.getCurrentPosition(geoSuccess,geoError);
     //google.maps.event.addDomListener(window, 'load', initialize);
    
    
     $('#fetch_map_location').on('click', function (event) {
        event.preventDefault();
        var ret =  getAreaByLatLng(mapLocation.lat , mapLocation.lng);
     });
     
     $('#save_location').on('click', function (event) {
        event.preventDefault();
        //var ret =  getAreaByLatLng(mapLocation.lat , mapLocation.lng); 
        
        if($("#addLine").val().trim()==""){
            alert("Address can not be empty .");
            return;
        }else if($("#area").val().trim()==""){
            alert("Area can not be empty .");
            return;
            
        }else if($("#city").val().trim()==""){
            alert("City can not be empty .");
            return;
            
        }else if($("#state").val().trim()==""){
            alert("State can not be empty .");
            return;
            
        }
       
        
        $.ajax({
                url: PayOne.config.urls.api,
                type: "POST",
                data: {
                    method:"updateRetailerAddress",
                    mobile:PayOne.core.cookie.get("mobile"),
                    address:$("#addLine").val(),
                    area:$("#area").val(),
                    city:$("#city").val(),
                    state:$("#state").val(),
                    pincode:$("#pincode").val(),
                    longitude:mapLocation.lng,
                    latiitude:mapLocation.lat                   
                },
                dataType: 'jsonp',
                jsonp: 'root',
                timeout: 50000,
                success: function(data, status){ 
                    $('#mobBillPaymentSubmit').button('reset');
                    $.each(data, function(i,item){				
                        if(item.status == 'failure'){                       
                            PayOne.core.failChk(item.code,item.description,true);

                        }else{
                            if(item.status == 'success'){
                              
                                PayOne.core.cookie.set("street", item.description.address, 1);
                                PayOne.core.cookie.set("city", item.description.city, 1);
                                PayOne.core.cookie.set("pincode", item.description.pincode, 1);
                                PayOne.core.cookie.set("state", item.description.state, 1);
                                PayOne.core.cookie.set("area", item.description.area, 1);
                                PayOne.core.cookie.set("area_id", item.description.area_id, 1);
                                PayOne.core.cookie.set("lat",item.description.latitude, 1);
                                PayOne.core.cookie.set("lng",item.description.longitude, 1);
                                mapLocation.lat = item.description.latitude;
                                mapLocation.lng = item.description.longitude;
                                alert("Address updated successfully .");
                                                                
                            }
                        }
                    });
                },
                error: function(){
                    $('#mobBillPaymentSubmit').button('reset');
                }
            });
     });
     
});
 function getAreaByLatLng(lat,lng){
             $.get( "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=true", function( data ) {
               var ret = {
                    "area_name":"",
                    "street_number":"",
                    "route" : "",
                    "city_name": "",
                    "state_name":"",
                    "country_name":"",
                    "pincode":""
                }
                $.each(data.results[0].address_components , function(i , arr){
                    if(arr.types[0] == "sublocality_level_1"){
                        ret.area_name = arr.long_name;
                    }else if(arr.types[0] == "locality"){
                        ret.city_name  = arr.long_name;

                    }else if(arr.types[0] == "administrative_area_level_1"){
                        ret.state_name = arr.long_name;
                    }
                    else if(arr.types[0] == "administrative_area_level_2"){
                        ret.extra = arr.long_name;
                    }
                    else if(arr.types[0] == "country"){
                        ret.country_name  = arr.long_name;

                    }else if(arr.types[0] == "street_number"){
                        ret.street_number  = arr.long_name;

                    }else if(arr.types[0] == "route"){
                        ret.route  = arr.long_name;

                    }else if(arr.types[0] == "postal_code"){
                        ret.pincode  = arr.long_name;

                    }
                   
                });
                
                if(ret.area_name == ""){
                    ret.area_name = ret.city_name;
                    if(ret.extra!="")
                        ret.city_name = ret.extra;
                }

                ret.formatted_address = data.results[0].formatted_address;
                ret.lat  = data.results[0].geometry.location.lat;
                ret.lng  = data.results[0].geometry.location.lng;

                ret.geoURL  = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=true";

                var addLine = "",c="";
                if(ret.street_number != "" && ret.route){
                    c=",";
                }
                addLine = ret.street_number +c+ret.route;
                $("#addLine").val(addLine);
                $("#area").val(ret.area_name);
                $("#city").val(ret.city_name);
                $("#state").val(ret.state_name);
                $("#pincode").val(ret.pincode);       
                
                return ret;
            });

            
            }
    </script>

<div class="breadcrumb">Setting &gt; Update Setting</div>
                        <div id="map-canvas"></div>
                        <form role="form" id="updatePin">
                          
                          <div class="form-group">
                            <label for="oldPin">Enter Address</label>
                            <input type="text" class="form-control" id="addLine" placeholder=" ">
                          </div>
                          <div class="form-group">
                            <label for="oldPin">Enter Area</label>
                            <input type="text" class="form-control" id="area" placeholder=" ">
                          </div>
                          <div class="form-group">
                            <label for="newPin">Enter City</label>
                            <input type="text" class="form-control" id="city" placeholder=" ">
                          </div>
                          <div class="form-group">
                            <label for="newPin">Enter State</label>
                            <input type="text" class="form-control" id="state" placeholder=" ">
                          </div>
                          <div class="form-group">
                            <label for="newPinContirm">Enter Pincode</label>
                            <input type="text" class="form-control" id="pincode" placeholder=" ">
                          </div>
                          <div class="form-inline">
                          	<button id="save_location" class="form-control default-btn">Save</button>
                            <button id="fetch_map_location" class="form-control default-btn">Fetch the location</button>
                          </div>
                        </form>   