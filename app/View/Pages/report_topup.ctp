<script>

$(function() {
    
    var htmlStr = "";
    $.each(PayOne.config.operators.mobRecharge , function(i ,v){
       htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/mobile/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p>"+v.name+"</p></a>"
    });
   // $("#mobile").html(htmlStr);
    
    //$('#report_transactions_tables').dataTable();
    report_topup_tables = $('#report_topup_tables').DataTable({"aaSorting": []});
    //$('#report_dth_transactions_tables').dataTable();
    //$('#report_vas_transactions_tables').dataTable();
    
    //----------for TopUp ---------
    $('#topUpDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#topUpDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});
    
   
    $('#searchTopUpData').on("click",function (e) {   
        
        //alert("Hello");return;
        var topUpDate1 = $("#topUpDate1").val();  
        var topUpDate2 = $("#topUpDate2").val();  
        
        //var items_per_page = $("#items_per_page").val();
        //var page_no = $("#page_no").val();
        var eleBodyId = "#report_topup_table_body";
        topUpDate1 = topUpDate1.split("-").reverse().join("-");
        topUpDate2 = topUpDate2.split("-").reverse().join("-");
        PayOne.reports.renderTopUpData(topUpDate1,topUpDate2,true,eleBodyId , report_topup_tables);//pageNo = 0 AND items_per_page = 0            
    });
        
    var date = new Date();
    var dt =  (date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate())+"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+date.getFullYear() ;  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
    
    var callLogin = false;
    
    var eleBodyId = "#report_topup_table_body";
    dt = dt.split("-").reverse().join("-");
   
    PayOne.reports.renderTopUpData(dt,dt,true,eleBodyId , report_topup_tables);//pageNo = 0 AND items_per_page = 0
    
});
</script>
<div class="breadcrumb">Report &gt; Transaction</div>                        
                        <div id="myTabContent" class="tab-content">
                          <div class="tab-pane fade active in" id="mobile">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                             <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="topUpDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="topUpDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                                <span id="searchTopUpData" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>-->
      <!--  <button id="searchTopUpData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button> -->
        <input id="searchTopUpData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>
                            </div>
							<div class="table-responsive">
                                    <table class="table table-striped table-hover order-column" id="report_topup_tables" cellspacing="0" width="100%">
                                      <thead>
<!--                                          <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          </tr>-->
                                          <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Distributor</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Opening Bal</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Closing Bal</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_topup_table_body">
                                        
                                      </tbody>
                                    </table>
                            </div>
                          </div>
                          
                          
                </div>
            