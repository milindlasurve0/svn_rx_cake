<!--<link rel="stylesheet" type="text/css" media="screen" href="http://malsup.com/jquery/block/block.css?v3"/>
<script src="http://malsup.github.io/jquery.blockUI.js"></script>-->
<script>



$(function() {
    
    var htmlStr = "";
    $.each(PayOne.config.operators.mobRecharge , function(i ,v){
       htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/mobile/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p>"+v.name+"</p></a>"
    });
    $("#mobile").html(htmlStr);
    
    report_earnings_tables = $('#report_earnings_tables').DataTable({
            "aaSorting": [],
            "createdRow": function ( row, data, index ) {
                //if ( data[5].replace(/[\$,]/g, '') * 1 > 4000 ) {
                    //$('td', row).eq(5).addClass('highlight');
                    row.className = "gradeX";
                //}
            }
    });
   
    
    $('#transDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#transDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});

    $('#searchEarningData').on("click",function (e) {         
        var transDate1 = $("#transDate1").val();  
        var transDate2 = $("#transDate2").val();  
       // var items_per_page = $("#items_per_page").val();
	   
	    transDate1 = transDate1.split("-").reverse().join("-");  
        transDate2 = transDate2.split("-").reverse().join("-");
        var page_no = $("#page_no").val();
        PayOne.reports.renderEarningData(transDate1,transDate2,0,0,"#earnReportTableBody" ,report_earnings_tables);//pageNo = 0 AND items_per_page = 0            
    });
	
	
	    var transDate1 = $("#transDate1").val();  
        var transDate2 = $("#transDate2").val();  
	   
	    transDate1 = transDate1.split("-").reverse().join("-");  
        transDate2 = transDate2.split("-").reverse().join("-");
        
    // var date = new Date();
   //  var dt =  (date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate())+"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+date.getFullYear() ;  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
    var callLogin = false;
    var items_per_page = $("#items_per_page").val();
	
    PayOne.reports.renderEarningData(transDate1,transDate2,0,0,"#earnReportTableBody" ,report_earnings_tables);//pageNo = 0 AND items_per_page = 0
    
});
</script>

            			<div class="breadcrumb">Reports > Earnings</div>
<!--                        <ul id="myTab" class="nav nav-tabs">
                          <li class=""><a href="#earningPrevious" class="earningPrevious"><span></span></a></li>
                          <li class="active"><a href="#earningDate1" data-toggle="tab">Feb 21-Feb 27</a></li>
                          <li class=""><a href="#earningDate2" data-toggle="tab">Feb 28-Mar 5</a></li>
                          <li class=""><a href="#earningDate3" data-toggle="tab">Mar 06-Mar 12</a></li>
                          <li class=""><a href="#earningNext" class="earningNext"><span></span></a></li>
                        </ul>-->
                        <div id="searchPanel" class="form-inline StatusSearch" style="width: auto;">
                            <span >Date</span><input style="margin-left:10px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                            <span >To</span><input   style="margin-left:10px;" id="transDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                            <span id="searchEarningData" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>-->
                 <!-- <button id="searchEarningData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button>-->
                  <input id="searchEarningData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>	
                        </div>
<!--                        <div class="form-inline StatusSearch" style="width: auto;">
                            <span >Items Per Page</span>
                            <select id="items_per_page" class="form-control" >                                
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>
                            
                        </div>-->
                        <div id="myTabContent" class="tab-content" style="padding-top: 0px;">
                          <div class="tab-pane fade active in" id="earningDate1">
							<div class="table-responsive">
									<table class="table table-striped table-hover order-column" id="report_earnings_tables" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Date</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Sale</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Earnings</th>
                                        </tr>
                                      </thead>
                                      <tbody id="earnReportTableBody">                                        
                                      </tbody>
                                    </table>
                            </div>
						  </div>
                        </div>
