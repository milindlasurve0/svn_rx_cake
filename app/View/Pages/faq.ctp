<style type="text/css">

.faq h2{
font-size:14px;
font-family:"Encode Sans Narrow Medium";
margin:0;
padding:10px 0 5px 0;
color:#513D98;
}

.faq h3{
font-size:13px;
font-family:"Encode Sans Narrow Medium";
margin:0;
padding:10px 0 5px 0;
}

.faq ol{
margin:0 0 10px 0;
padding-left:20px;
}

.faq li{
font-size:13px;
text-align:justify;
margin:0 0 2px 0;
font-family:"Encode Sans Normal";
}
</style>
<div class="faq" id="" style="overflow-y: scroll; height:700px;">
<h2>General &amp; Account Related Queries:</h2>

<ol>
	<li><h3>Why should I use Pay1?</h3>
	Life becomes simpler if you make payments in just a click and avoid standing in queues.PAY1 provides recharges of telecom, data card, DTH and bill payment. Other than Online payment option we also give you a choice to go local and choose offline payment option by visiting our merchants.</li>
	<li>
	<h3>What is the offline payment option?</h3>
	PAY1 has its reach at various locations. You can visit our merchants and buy PAY1 wallet cash from them. Go to the Wallet Icon on the navigation bar. Check out the "Top Up Wallet" Tab under that go to the "Dealer Locator" and find your nearest locator.
	</li>
	<li>
	<h3>Which operator does PAY1 support?</h3>
	PAY1 supports all the major operators for recharges and bill payment to suffice your needs.
	</li>
	<li>
	<h3>Is it mandatory to sign up?</h3>
	Registration is a quick &amp; useful process which helps you out to keep a track of your transactions' history. You can also avail the benefit of the coupons if you have an account with us.
	</li>
	<li>
	<h3>How do I register myself?</h3>
	You just need to click on the Sign-Up button displaying on the first page of our App/website.Enter your mobile number, e-mail id, DOB and create password for PAY1 account. And you will be registered.
	</li>
	<li><h3>
	What if I forgot my password?</h3>
	In this case you need to e-mail us on <a href="mailto:listen@pay1.in">listen@pay1.in</a>  with a subject line of "Forgot Password - &lt;your mobile number&gt;" and we will revert back to you on the same
	</li>
	
	<li>
	<h3>How do I edit my profile?</h3>
	It is very simple. First login and go to my profile tab, select profile and here you can edit your profile. Save the changes at the end of process.
	</li>
</ol>

<h2>Recharge/Bill Payment Related Queries:</h2>

<ol>
	<li><h3>How do I recharge/pay bills?</h3>
	It is simple just follow below given steps:<br />
	Step1: Go to the Main Menu<br />
	Step2: Choose Mobile Tab for Recharge / Bills for Bill payment.<br />
	Step3: Enter your mobile no., operator's Name and recharge/bill value.<br />
  Step4: Chose your payment option- Debit/Credit Card, Net Banking, Coupon Code, offline Pay1 Wallet method. (Sign up, if you're a first time user.)</li>
	<li><h3>
	What should I do if my recharge is not done?</h3>
	Usually you get a confirmation within 2 min after the payment is successfully done for your mobile number/DTH/Datacard. If it takes more than 2 hours we request you, go to the Support Tab under My Profile, you can register your complaint by choosing one of the below given ways:<br />
	a)  Fill up the query form.<br />
	b)  Mail us at <a href="mailto:listen@pay1.in">listen@pay1.in</a><br />
	c)  Quick Intimation via Missed Call, we will automatically take your complaint of last transaction and update you for the same as soon as possible.
  </li>
	<li><h3>
	How long does it take to get my recharge/Bill Payment done?</h3>
	The Recharges happen immediately within 2 min as soon as you finish up with your transaction and make a payment. In case of Bill payments, it will happen with 48 hours. You will also get a confirmation from your operator via SMS
	</li>
	<li><h3>
	Can I also recharge for my friends and family?</h3>
	Yes! You can recharge for your closed ones just how you recharge for yourself within the same account.
	</li>
	<li><h3>
	Do I need to pay any extra charge/s for using PAY1?</h3>
	Definitely not for recharging your mobile number. You will get the benefit of the Top-up plan you have selected as per your operator.<br />
	But in case of Bill payments, we charge Rs. 10 extra as a convenience fees per transaction.
	</li>
	<li><h3>
	What if my recharge / bill payment got Failed?</h3>
	This could have happened because of various possible reasons:<br />
	a)  If you have chosen a wrong Mobile operator<br />
	b)  Mobile Operator's server was temporarily unavailable<br />
	c)  Invalid amount entered was not accepted by you Mobile Operator<br />
	d)  If PAY1 was unable to connect you to the mobile operator due to some temporarily network issue.
  </li>
	<li><h3>
	What if my recharge/ bill payment transaction is unsuccessful?</h3>
	Not to worry at all. The same amount will get refunded immediately within your account and you can reuse it for further transactions.
	</li>
	<li><h3>
	What if my transaction got failed and I have not got the refund back?</h3>
	In such case, go to the Support Tab under My Profile, you can register your complaint by choosing one of the below given ways:<br />
	d)  Fill up the query form.<br />
	e)  Mail us at <a href="mailto:listen@pay1.in">listen@pay1.in</a><br />
	f)  Quick Intimation via Missed Call, we will automatically take your complaint of last transaction and update you for the same as soon as possible.
  </li>
	<li><h3>What do I do to recharge my mobile no. if I have switched my operator through MNP?</h3>
	Kindly follow these steps:<br />
	Step1: Enter the Mobile No.<br />
	Step2: Select your operator (incase it shows your previous operator)<br />
	Step3: Enter the recharge value<br />
	Step4: Login or sign up if visiting for the first time.<br />
	Step5: Select your Payment method (Debit/Credit Card, Net banking, Pay1 Wallet).
	</li>
	<li><h3>
	What if I get any unexpected or Zero Talk Time?</h3>
	The talk time that you receive after doing a recharge from PAY1 mobile application is defined by the mobile operators and we have no control over it. Different recharge values give different amounts of talk time. Sometimes the recharge values that you enter could be a special recharge that might give you free SMS, free data usage or reduced call rate without giving you any talk time.It is recommended that for such cases, you check directly with your mobile operator/s.
	</li>
	<li><h3>
	What if I have recharged on a wrong mobile no/Subscriber ID?</h3>
	We suggest you to carefully put entries in the text box. If you have recharged against a wrong number &amp; if that same number/subscriber ID is active or valid, the benefit will go to the same respective number/ID. We are sorry that we will not be able to get the refund back in that scenario as it is in not our control.
	</li>
</ol>

<h2>Payment &amp; Wallet Related Queries:</h2>

<ol>
	<li><h3>What is PAY1 Wallet?</h3>
	It is a safe&amp; secure virtual wallet which is easily accessible to process your utility payments. It allows you to add money in the wallet and that can be used further to make all the transactions on PAY1 Mobile/WEB Application. It is a Hassel free process that eliminates unnecessary sync with your debit/credit card or net baking gateways.</li>
	<li><h3>
	How do I get cash in my PAY1 Wallet?</h3>
	Once you have logged in, go to the Wallet on the navigation bar. It will suggest you the three ways to top-up your PAY1 wallet.<br/>

	a)  Dealer Locator: Visit the nearest PAY1 Merchant to load cash in your Wallet (It is exactly similar how you recharge your mobile no. &amp; get balance in your account).<br />
	b)  Through Debit/Credit Card: Load your wallet through Net Banking, Debit, Credit card.<br />
	c)  Using Coupon: You can also load PAY1 cash by using Coupon/s issued by PAY1 for the purpose of recharge/bill payments/buying virtual goods etc by putting up the Coupon Code in the Box.
  </li>
	<li><h3>
	Is it Safe to opt for Online payment option?</h3>
	Absolutely! Payu is our payment gateway partner and that the same system is used by most Indian e-commerce portals. All the transactions are executed securely and no critical information regarding your payment is ever accessible by any employee or third party.
	</li>
	<li><h3>
	What is CVV, CV2 and 3D security?</h3>
	CVV or CV2 are the three digit number mentioned at the back of your card. This is meant for security purpose. The 3D security Code is like a password used only for online transactions through Debit/Credit Cards. You can check your bank's website for details. This is specially designed for the safe transaction. CVV is the last three digits mentioned at the back side of your debit/credit card.
  </li>
	<li><h3>
	What if Amount has been deducted from my bank account and not credited in my PAY1 Wallet?</h3>
	Do not panic if it happens.<br />
	In such case it happens mostly when the payment has not been made by your bank to us, the same amount will get refunded to your account by your bank as per their norms. In Vice Versa case, kindly mail us: <a href="mailto:listen@pay1.in">listen@pay1.in</a> and we will respond to you as soon as possible.
	</li>
	<li><h3>
	Is it necessary to have cash in wallet while initiating any transaction?</h3>
	If you are choosing an online payment option or having a coupon code than definitely not. But if you are opting for offline payment option than you must visit our nearest merchant first to PAY1 Cash in you PAY1 wallet.
	</li>
	<li><h3>
	What is the maximum limit of PAY1 Wallet to keep cash?</h3>
	As per RBI guidelines you can maintain your wallet upto Rs.5,000/- at one time. And once you provide your Pan Card and ID &amp; address proof to us it can be exceeded upto Rs. 50,000/-.
	</li>
</ol>
</div>