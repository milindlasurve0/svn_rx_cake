<script>

$(function() {
    
    var htmlStr = "";
    $.each(PayOne.config.operators.walletTopup , function(i ,v){
       htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/wallet/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p style='width:120%'>"+v.name+"</p></a>"
    });
    $("#mobile").html(htmlStr);
    
    
    transStatusTable  = $('#transStatusTable').DataTable(
    {"aaSorting": []}
//    {
//        "createdRow": function ( row, data, index ) {
//            alert("Created");
//            //if ( data[5].replace(/[\$,]/g, '') * 1 > 4000 ) {
//                //$('td', row).eq(5).addClass('highlight');
//                row.className = "gradeX";
//            //}
//        }
//    }
    );
    lastTenTransTable = $('#lastTenTransTable').DataTable({"aaSorting": []});
    
    
    $('#transDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#transDate1').datepicker().on('changeDate', function (ev) {
        PayOne.reports.renderTransStatus('wallet',4,$(this).val(),true,'#transStatusTableBody',transStatusTable);
        $(this).datepicker('hide');
    });
    /*$('#transDate2').datepicker({
		"format": "dd-mm-yyyy"        
	});*/
    
    Date.daysBetween = function( date1, date2 ) {
      //Get 1 day in milliseconds
      var one_day=1000*60*60*24;

      // Convert both dates to milliseconds
      var date1_ms = date1.getTime();
      var date2_ms = date2.getTime();

      // Calculate the difference in milliseconds
      var difference_ms = date2_ms - date1_ms;

      // Convert back to days and return
      return Math.round(difference_ms/one_day); 
    }
    
    $('#searchReversal').click(function (e) {
                   
                    var date_from = $('#transDate1').val();
                    //var date_to   = $('#transDate2').val();
                    if(date_from == '' ){
                            alert("Please select proper date.");
                    } else {
                            //$('#date_err').hide();
                            var date_from_arr = date_from.split("-");
                            var dt = date_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                            PayOne.reports.renderTransStatus("wallet",5,dt,true,"#transStatusTableBody",transStatusTable);                            
                    }
        
    });
    
    
    
    $('#myTab a').on("click",function (e) {        
        var hash  = $(this).attr('href');        
        if(hash=="#details"){
            PayOne.reports.lastTran(5,"#lastTenTransTableBody",lastTenTransTable);// 1 for mobile
        }else if(hash=="#status"){
            //var dt =  date.getFullYear() +"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+(date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate());  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
            var dt = $('#transDate1').val().split("-").reverse().join("-");
            //var date = new Date();
            //var dt =  date.getFullYear() +"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+(date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate());  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
            var callLogin = true;
            PayOne.reports.renderTransStatus("wallet",5,dt,callLogin,"#transStatusTableBody",transStatusTable);
        }  
    });
    $('#searchByNo').on("click",function (e) {         
        var no = $("#searchNo").val();  
        if(no == ""){
            alert("Please enter a valid mobile no.");return;
        }
        PayOne.reports.lastTranSearchByNo(5,no,"#lastTenTransTableBody",lastTenTransTable);// 1 for mobile                    
    });
        
    
    
    
    $('#walletRecharge').on('submit', function (event) {
         event.preventDefault();
         walletRecharge(1);
     });
    function walletRecharge(cnfFlag){
       // prevPage = fromLocation;

        var mobileNo = $("#mobileNo").val().trim();
        var rechargeAmt = $("#rechargeAmt").val().trim();
        if(PayOne.core.utils.mobileValidate(mobileNo) == '0') return;	
        if(PayOne.core.utils.amtValidate(rechargeAmt) == '0')return;

        var stv = $("input[name='stvOrTopUp_radio']:checked").val();

        if(cnfFlag == 1){
            var cnf = "Wallet Recharge \nNumber: "+mobileNo+"\nAmount: Rs."+rechargeAmt+"\n";
            var r=confirm(cnf+"Press OK to confirm");
            if(r==false){
                return false;
            }
        }
        //alert("==========="+cnfFlag+","+fromLocation);
            /*if(passFlag=='F'){
                showHide('page1','pinchange');
                return;
            }*/
            //alert("==========++=");
            //var tmp = $('#rec_now_loader').html();
            //$('#rec_now_loader').html(loader);
            //alert("method=mobRecharge&mobileNumber="+mobileNo+"&operator="+$('#operatorId').val()+"&subId="+mobileNo+"&amount="+rechargeAmt+"&circle="+$('#mob-rec-cir').val()+"&special=stv&type=flexi");
        $('#walletRechargeSubmit').button('loading');
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method:"pay1Wallet",
                device_type:"web",
                mobileNumber:mobileNo,
                amount:rechargeAmt
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status){ //alert("hello=="+data);
                //$('#rec_now_loader').html(tmp);
                $('#walletRechargeSubmit').button('reset');
                $.each(data, function(i,item){				
                    if(item.status == 'failure'){                       
                        PayOne.core.failChk(item.code,item.description,true);
                       
                    }else{
                        if(item.status == 'success'){
                            var desc = 'Recharge request sent successfully';
                            $('#success_description').html(desc);
                            $('#success').modal("show");
                            //alert($('#mobileNo').attr('value', ""));
                            $("#mobileNo").val("");
                            $("#rechargeAmt").val("");
                            //$('#MRODd').attr('value', '');
                            //$('#MROtitle').html('');
                            //showHide('page1','navMobile');
                            $('#retailerBal').html(item.balance);
                            setTimeout(function(){
                                   window.location="/wallet";  //redirect to main page
                                }, 3000);
                        }
                    }
                });
            },
            error: function(){
                $('#walletRechargeSubmit').button('reset');
                //$('#rec_now_loader').html(tmp);
            }
        });
    }
    
    
});
</script>
            			<div class="breadcrumb">Wallet</div>                        
                        <ul id="myTab" class="nav nav-tabs">
                          <li class="active"><a href="#mobile" data-toggle="tab">Wallet Topup</a></li>
                          <li class=""><a href="#status" data-toggle="tab">Complaint Status</a></li>
                          <li class=""><a href="#details" data-toggle="tab">Request Complaint</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div class="tab-pane fade active in" id="mobile">
								
                                <a class="mobileList" href="#">
                                			<img src="<?php echo Configure::read('AWS_URL'); ?>/images/pages/square.png">
                                            <p >Wallet</p>
                                </a>
                  <!--          <form role="form" id="walletRecharge">
                                    
                                    <div class="form-group">
                                        <label for="mobileNo"  for="mobileNo">Enter Prepaid Mobile Number</label>
                                        <input type="text" class="form-control" id="mobileNo" placeholder="" pattern="^\d{10,10}" title="10 Digit Mobile No" />
                                    </div>
                                    <div class="form-group">
                                        <label for="rechargeAmt">Enter Amount</label>
                                        <input type="text" class="form-control" id="rechargeAmt" placeholder="" pattern="^\d{1,5}(\.\d{1,2})?$" title="Amount should be a no"/>
                                    </div>                                      
                                    
                                    <button id="walletRechargeSubmit" data-loading-text="Processing..." type="submit" class="form-control default-btn">Proceed</button>
	
                                </form>  -->    

                                

                                
                          </div>
                          <div class="tab-pane fade" id="status">

                                <!--<div class="form-inline StatusSearch">
                                    <span >Date</span><input style="margin-left:10px;width:150px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                    <input id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>
                                </div>-->
                                <!--                                    <span >To</span><input   style="margin-left:10px;width:150px;" id="transDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>-->
<!--                                    <span id="searchReversal" class="glyphicon glyphicon-search searchProcess" style="margin-left:10px;cursor:pointer">Search</span>-->
                                    <!--<button id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button>-->
                                <ul id="myTab" class="nav nav-tabs">
                                  <li class=""><a href="#earningPrevious" class="reportNavigation reportNavigationPrev earningPrevious" onclick="PayOne.reports.renderTransStatus('mob',1,dt,true,'#transStatusTableBody',transStatusTable);"><span></span></a></li>
                                  <li class="active"><a href="#earningDate1" data-toggle="tab"><input style="margin-left:10px;width:150px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly /></a></li>
                                  <li class=""><a href="#earningNext" class="reportNavigation reportNavigationNext earningNext"><span></span></a></li>
                                </ul>
                          		<div class="table-responsive">
                            	
                                <table class="table table-striped table-hover order-column" id="transStatusTable" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                        </tr>
                                      </thead>
                                      <tbody id="transStatusTableBody">
                                        
                                      </tbody>
                                    </table>
                                    </div>
                          </div>
                          <div class="tab-pane fade" id="details">
                          <div class="table-responsive">
                            <div class="form-inline RequestSearch">
                                <input id="searchNo" type="text" class="form-control" placeholder="Enter 10 digit No"/>
<!--                                <input id="searchByNo" type="submit" class="default-btn searchProcess" value="search"/>   -->
                                <!--<button id="searchByNo" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button>-->
			    <input id="searchByNo" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>
                            </div>
                            
                            <table class="table table-striped table-hover order-column" id="lastTenTransTable" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                        </tr>
                                      </thead>
                                      <tbody id="lastTenTransTableBody">                                        
                                        
                                      </tbody>
                                    </table>
                                    </div>
                          </div>
                        </div>
                
                    
                    
                    
