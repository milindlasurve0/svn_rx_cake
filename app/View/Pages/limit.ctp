
<script>
   
    var deviceUuid = "";
    var pathname = location.pathname.split("/");
    if(pathname[2]=="success"){
    
                        $.ajax({
                            url: PayOne.config.urls.api,
                            type: "POST",
                            data: {
                                   method:"updateBal",
                                   device_type:"web"
                                  },      
                            dataType:'jsonp',
                            jsonp:'root',
                            timeout:50000,
                            success: function(data, status){
                              if(data[0].status=="success"){
                              PayOne.core.cookie.set("balance",data[0].description, 1); 
                               var desc = 'Online Recharge completed Successfully!!!';
                               $('#success_description').html(desc);
                               $('#success').modal("show");
                            }},
                error: function(error){
                }
            });
    }

            else if(pathname[2]=="failure"){
                               //window.location ="shop.pay1.in";
                               $("#myFrame").css("display", "none"); 
                               var desc = 'Error occured transaction failed!!!';
                               alert(desc);
                              
                          }
               
    
    $(function() {
    
        $('#onlineBillPayment').on('submit', function (event) {
            event.preventDefault();
            onlinePayment(1);
        });
        
        function mobileValidate(y){
            var y = y.trim();
            if(y == ""){alert("Enter mobile number");return 0;
            }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
            }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
            }
            return 1;
        }

        function amtValidate(y) {
            var err = '';
            var y = y.trim();
            if(y == ""){alert("Plese enter proper amount");return 0;
            }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
            }
            return 1;
        }

        function onlinePayment(cnfFlag){
           
            
            var rechargeAmt = $("#rechargeAmt").val().trim();           
            if(amtValidate(rechargeAmt) == '0')return;
            if(cnfFlag == 1){
                var ota_text = "";
                if(parseInt(ota_fee) > 0)
                	ota_text = " + Rs. " + ota_fee;
                var cnf = "\nAmount: Rs."+rechargeAmt+ ota_text +"\n";
                                    
                var r=confirm(cnf+"Press OK to confirm");
                if(r==false){
                    return false;
                }
            }
           
            $('#onlineBillPaymentSubmit').button('loading');

                 var data = { method:"pg",
                              device_type:"web",
                              amount:rechargeAmt,
                              type:"flexi"
                             }
                        
                        
                        $.ajax({
                            url: PayOne.config.urls.api,
                            type: "POST",
                            data:data,       
                            dataType: 'jsonp',
                            jsonp: 'root',
                            timeout: 50000,
                            success: function(data, status){ 
                                $('#onlineBillPaymentSubmit').button('reset');
                                $.each(data, function(i,item){				
                                    if(item.status == 'failure'){                       
                                        PayOne.core.failChk(item.code,item.description,true);

                                    }else{
                                        if(item.status == 'success'){
                                        localStorage.setItem("formdata",item.description);
                                        $("#myFrame").show();
                                    
                                        window.location = "/pay"
                            }
                        }
                    });
                },
                error: function(error){
                    $('#utiBillPaymentSubmit').button('reset');
                    //$('#rec_now_loader').html(tmp);
                }
            });
        }
    
    });
    var ota_fee = "";
    $(document).ready(function(){
    	ota_fee = PayOne.core.cookie.get("ota_fee");
    	if(parseInt(ota_fee) > 0)
        	$("#ota_message").html("<span style='color:#9C4F0E'>* A one time activation fee of Rs. " + ota_fee + " will be charged initially.</span>");
    });    
</script>
  
<div class="breadcrumb">Online <?php echo ucfirst($type); ?><br/>Recharge your account using online netbanking service.</div>
<form role="form" id="onlineBillPayment">
    
    <div class="form-group">
        <label for="rechargeAmt">Enter Amount</label>
        <input type="text" class="form-control" id="rechargeAmt" placeholder="Enter Amount" pattern="^\d{1,5}(\.\d{1,2})?$" title="Amount should be a no"/>
    </div>     
	<div class="form-group" id="ota_message">
        
    </div>   
                    

    
    <button id="onlineBillPaymentSubmit" data-loading-text="Processing..." type="submit" class="form-control default-btn">Proceed</button>
</form>

