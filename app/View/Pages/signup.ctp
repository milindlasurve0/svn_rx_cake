<script>

$(function(){
    
    
    //$("#addLine").val(PayOne.core.cookie.get("street"));
    //$("#area").val(PayOne.core.cookie.get("area"));
    //$("#city").val(PayOne.core.cookie.get("city"));
    //$("#state").val(PayOne.core.cookie.get("state"));
    //$("#pincode").val(PayOne.core.cookie.get("pincode"));
     /*
      "method"="addLeads"
                          "full_name"
                          "email"
                          "contact_no"
                          "city"
                          "state"
                          "comment"
                          "req_by" = "web"
      **/
     $('#signup').on('click', function (event) {
        event.preventDefault();
        //var ret =  getAreaByLatLng(mapLocation.lat , mapLocation.lng); 
        
        if($("#full_name").val().trim()==""){
            alert("Full name can not be empty .");
            return;
        /*}else if($("#email").val().trim()==""){
            alert("Area can not be empty .");
            return;*/
        }else if($("#contact_no").val().trim()==""){
            alert("Contact no can not be empty .");
            return;    
        }else if($("#city").val().trim()==""){
            alert("City can not be empty .");
            return;
            
        }else if($("#state").val().trim()==""){
            alert("State can not be empty .");
            return;
            
        }/*
       "full_name"
                          "email"
                          "contact_no"
                          "city"
                          "state"
                          "comment"
                          "req_by" = "web"*/
         $('#signup').button('loading');
        $.ajax({
                url: PayOne.config.urls.api,
                type: "POST",
                data: {
                    method:"addLeads",
                    full_name:$("#full_name").val(),
                    email:$("#email").val(),
                    contact_no:$("#contact_no").val(),
                    city:$("#city").val(),
                    state:$("#state").val(),
                    comment:$("#comment").val(),
                    req_by:"web"                
                },
                dataType: 'jsonp',
                jsonp: 'root',
                timeout: 50000,
                success: function(data, status){ 
                    $('#signup').button('reset');
                    $.each(data, function(i,item){				
                        if(item.status == 'failure'){                       
                            PayOne.core.failChk(item.code,item.description,true);
                        }else{
                            if(item.status == 'success'){
                              $("#full_name").val("");
                              $("#email").val("");
                              $("#contact_no").val("");
                              $("#city").val("");
                              $("#state").val("");
                              $("#comment").val("");
                              alert("Address updated successfully .");
                                                                
                            }
                        }
                    });
                },
                error: function(){
                    $('#signup').button('reset');
                }
            });
     });
     
});
 
    </script>

<div class="breadcrumb">Sign-Up</div>
                        
                        <form role="form" id="updatePin">
                          
                          <div class="form-group">
                            <label for="oldPin">Full Name</label>
                            <input type="text" class="form-control" id="full_name" name"full_name" placeholder=" "/>
                          </div>
                          <div class="form-group">
                            <label for="oldPin">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder=" "/>
                          </div>
                          <div class="form-group">
                            <label for="oldPin">Contact No</label>
                            <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder=" "/>
                          </div>                          
                          <div class="form-group">
                            <label for="newPin">Enter City</label>
                            <input type="text" class="form-control" id="city" placeholder=" "/>
                          </div>
                          <div class="form-group">
                            <label for="newPin">Enter State</label>
                            <input type="text" class="form-control" id="state" placeholder=" "/>
                          </div>
                          
                          <div class="form-group">
                            <label for="newPinContirm">Comment</label>
                            <textarea id="comment" name="comment" class="form-control"></textarea>
                          </div>
                          <div class="form-inline">
<!--                          	<button id="signup" class="form-control default-btn">Save</button>-->
                            <button id="signup" data-loading-text="Processing..." type="submit" class="form-control default-btn">Save</button>
                          </div>
                        </form>   