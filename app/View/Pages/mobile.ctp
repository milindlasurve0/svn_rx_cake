    <script>

$(function() {

    var htmlStr = "";
    $.each(PayOne.config.operators.mobRecharge , function(i ,v){
       if(v.id != 5)  // hide Loop Operator
       htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/mobile/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p style= 'padding-left: 9px;'>"+v.name+"</p></a>"
    });
    $("#mobile").html(htmlStr);
    
    transStatusTable  = $('#transStatusTable').DataTable({"aaSorting": []});
    lastTenTransTable = $('#lastTenTransTable').DataTable({"aaSorting": []});
    
    
    $('#transDate1').datepicker({
		"format": "dd-mm-yyyy"
	});
    $('#transDate1').datepicker().on('changeDate', function (ev) {
        PayOne.reports.renderTransStatus('mob',1,$(this).val(),true,'#transStatusTableBody',transStatusTable);
        $(this).datepicker('hide');
    });
    /*$('#transDate2').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#transDate2').datepicker().on('changeDate', function (ev) {
        alert("HEllo2");
        $(this).datepicker('hide');
    });*/
    
    Date.daysBetween = function( date1, date2 ) {
      //Get 1 day in milliseconds
      var one_day=1000*60*60*24;

      // Convert both dates to milliseconds
      var date1_ms = date1.getTime();
      var date2_ms = date2.getTime();

      // Calculate the difference in milliseconds
      var difference_ms = date2_ms - date1_ms;

      // Convert back to days and return
      return Math.round(difference_ms/one_day); 
    }
    
    $('#searchReversal').click(function (e) {
                    
                    var date_from = $('#transDate1').val();
                    //var date_to   = $('#transDate2').val();
                    if(date_from == '' ){
                            alert("Please select proper date.");
                    } else {
                            //$('#date_err').hide();
                            var date_from_arr = date_from.split("-");
                            var dt = date_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                            PayOne.reports.renderTransStatus("mob",1,dt,true,"#transStatusTableBody",transStatusTable);                            
                    }
        
    });
    
    
    /*$('.reportNavigation').on("click",function (e) {    
        e.preventDefault();        
        //var hash  = $(this).attr('href');        
        //if(hash=="#details"){
        //    PayOne.reports.lastTran(1,"#lastTenTransTableBody",lastTenTransTable);// 1 for mobile
        //} 
    });*/
    
    $('#myTab a').on("click",function (e){  
        var hash  = $(this).attr('href');        
        if(hash=="#details"){
            PayOne.reports.lastTran(1,"#lastTenTransTableBody",lastTenTransTable);// 1 for mobile
        }else if(hash=="#status"){
            //var date = new Date();
            //var date = new Date($('#transDate1').val().split("-").reverse().join("-"));
            //var dt =  date.getFullYear() +"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+(date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate());
            var dt = $('#transDate1').val().split("-").reverse().join("-");
            var callLogin = true;
            PayOne.reports.renderTransStatus("mob",1,dt,callLogin,"#transStatusTableBody",transStatusTable);
        }        
    });
    $('#searchByNo').on("click",function (e) { 
        var no = $("#searchNo").val();  
        if(no == ""){
            alert("Please enter a valid mobile no.");return;
        }
        PayOne.reports.lastTranSearchByNo(1,no,"#lastTenTransTableBody",lastTenTransTable);// 1 for mobile                    
    });
        
    
    
    //public function navigationRenderTrans(){
        
    //} 
});
</script>
                         <head>
                          <meta name="google-play-app" content="app-id=com.mindsarray.pay1"> 
                          <meta name="msApplication-ID" content="App" />
                         <meta name="msApplication-PackageFamilyName" content="b5c6d8c9-8008-472a-9155-89a1fe6dceba" />
                          <meta name="viewport" content="width=device-width, initial-scale=1.0">
                          <meta name="msApplication-WinPhonePackageUrl" content="app-id=44aefe8a-fff1-40b9-8a2e-76c835339fdc" />

                          <link rel="stylesheet" href="css/jquery.smartbanner.css" type="text/css" media="screen">
                         <link rel="android-touch-icon" href="/images/leftMenu/wallet.png">
                         </head>
                        <body>

                           <script src="js/jquery.smartbanner.js"></script>
                           
                           <script type="text/javascript">
                               $(function () { $.smartbanner({ daysHidden: 0, daysReminder: 0, title:'Pay1 Merchant',icon: '/images/leftMenu/wallet.png',scale:'auto',author:'PAY1'
 }) })
                           </script>
                         </body>
<!--						 <marquee><span style="color: red;">Notice:-  Due to Bank holiday on 2nd Oct 2015, Kindly take the Limit in Advance. PAY1</span></marquee>-->
            			<div class="breadcrumb"><b>Mobile</b></div>                        
                        <ul id="myTab" class="nav nav-tabs">
							<li class="active"><a href="#mobile" data-toggle="tab">Mobile</a></li>
							<li class=""><a href="#status" data-toggle="tab">Complaint Status</a></li>
							<li class=""><a href="#details" data-toggle="tab">Request Complaint</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div class="tab-pane fade active in" id="mobile">
								
                                <a class="mobileList" href="#">
                                			<img src="<?php echo Configure::read('AWS_URL'); ?>/images/pages/square.png">
                                            <p>Mobile1</p>
                                </a>
                               
                                
                          </div>
                          <div class="tab-pane fade" id="status">
                                <ul id="myTab" class="nav nav-tabs">
                                  <li class=""><a href="#earningPrevious" class="reportNavigation reportNavigationPrev earningPrevious" onclick="return false;"><span></span></a></li>
                                  <li class="active"><a href="#earningDate1" data-toggle="tab"><input style="margin-left:10px;width:150px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly /></a></li>
                                  <li class=""><a href="#earningNext" class="reportNavigation reportNavigationNext earningNext"><span></span></a></li>
                                </ul>
<!--                                <div class="form-inline StatusSearch">
                                    <span >Date</span>
                                    <input style="margin-left:10px;width:150px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly />
                                    <input id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>
                                </div>-->
                                <!--<span>To</span><input   style="margin-left:10px;width:150px;" id="transDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>-->
<!--                                    <span id="searchReversal" class="glyphicon glyphicon-search searchProcess" style="margin-left:10px;cursor:pointer">Search</span>-->
                                    <!--<button id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button>-->
                          		<div class="table-responsive">
                            	
                                <table class="table table-striped table-hover order-column" id="transStatusTable" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                        </tr>
                                      </thead>
                                      <tbody id="transStatusTableBody">
                                        
                                      </tbody>
                                    </table>
                                    </div>
                          </div>
                          <div class="tab-pane fade" id="details">
                          <div class="table-responsive">
                            <div class="form-inline RequestSearch">
                                <input id="searchNo" type="text" class="form-control" placeholder="Enter 10 digit No"/>
<!--                                <input id="searchByNo" type="submit" class="default-btn searchProcess" value="search"/>   -->
                               <!-- <button id="searchByNo" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button> -->
				 <input id="searchByNo" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>	
                            </div>
                            
                            <table class="table table-striped table-hover order-column" id="lastTenTransTable" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                        </tr>
                                      </thead>
                                      <tbody id="lastTenTransTableBody">                                         
                                        
                                      </tbody>
                                    </table>
                                    </div>
                          </div>
                        </div>
                
                    
                    
                    
