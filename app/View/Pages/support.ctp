<script>


function writetous(){

	var writeSub = $('#writeSub').val();
	if(writeSub == ''){
		alert('Enter Subject');
		return;
	}
	var writeMsg = $('#writeMsg').val();
	if(writeMsg == ''){
		alert('Enter Message');
		return;
	}
	var tmp = $('#wrt_us_loader').html();
		
	$.ajax({
		url: PayOne.config.urls.api,
		type: "POST",
		data: {method:'writetous',sub:writeSub,bdy:writeMsg},
		dataType: 'jsonp',
		jsonp: 'root', 
		timeout: 50000,
		success: function(data, status){
			$('#wrt_us_loader').html(tmp);
			$.each(data, function(i,item){
				if(item.status == 'failure'){PayOne.core.failChk(item.code,item.description);
				}else{
					if(item.status == 'success'){
						$('#writeSub').val('')
						$('#writeMsg').val('');
						alert('Message Sent Successfully');				
					}
				}
			});
		},
		error: function(){$('#wrt_us_loader').html(tmp);}
	});	
}

function loadBankDetails(){
	$('#accounts_body').html("<img src='img/ajax-loader-1.gif'>");
	$.ajax({
		url: PayOne.config.urls.api,
		type: "POST",
		data: {method:'bankAccounts'},
		dataType: 'jsonp',
		jsonp: 'root', 
		timeout: 50000,
		success: function(data, status){
			$.each(data, function(i,item){
				if(item.status == 'failure'){PayOne.core.failChk(item.code,item.description);
				}else{
					if(item.status == 'success'){
						var table_row = "";
						var table_head = "";
						$.each(item.description, function(j, row){
							
							table_row += "<tr>";
							if(j == 0)
								table_head += "<tr>";
							$.each(row, function(key, value){
								if(j == 0)
									table_head += "<th>" + key + "</th>";
								table_row += "<td>" + value + "</td>";
							});
							table_row += "</tr>";
							if(j == 0)
								table_head += "</tr>";
						});	
						$('#accounts_body').html(table_row);
						$('#accounts_head').html(table_head);
					}
				}
			});
		}
	});	
}

function changeMobileNumber(){
	var old_mobile = $('#old_mobile').val();
	var new_mobile = $('#new_mobile').val();
	var pin = $('#pass').val();
	if(old_mobile.length != 10 || new_mobile.length != 10){
		alert("Enter a 10 digit number for mobile");
		return false;
	}	
	if(pin == ""){
		alert("Enter your login pin");
		return false;
	}	
	$.ajax({
		url: PayOne.config.urls.api,
		type: "POST",
		data: {method:'changeMobileNumber', oldNumber:old_mobile, newNumber:new_mobile, password:pin},
		dataType: 'jsonp',
		jsonp: 'root', 
		timeout: 50000,
		success: function(data, status){
			$.each(data, function(i,item){
				if(item.status == 'failure'){PayOne.core.failChk(item.code,item.description);
				}else{
					if(item.status == 'success'){
						alert(item.description);
						$('#change_mobile_form').hide();
						$('#change_mobile_otp_form').show();
					}
				}
			});
		}
	});	
}

function authenticateMobileNumberChange(){
	var otp = $('#otp').val();
	if(otp == ""){
		alert("Enter the OTP received by you via SMS");
		return false;
	}	
	$.ajax({
		url: PayOne.config.urls.api,
		type: "POST",
		data: {method:'authenticateMobileNumberChange', otp:otp},
		dataType: 'jsonp',
		jsonp: 'root', 
		timeout: 50000,
		success: function(data, status){
			$.each(data, function(i,item){
				if(item.status == 'failure'){PayOne.core.failChk(item.code,item.description);
				}else{
					if(item.status == 'success'){
						alert(item.description);
						location.reload();
					}
				}
			});
		}
	});	
}

$(function() {

    if(PayOne.core.cookie.get('parentId') == 1){
        $('#bank_details').show();
    }
        
    
    
    //public function navigationRenderTrans(){
        
    //} 
});
</script>
            			<div class="breadcrumb"><b>Support</b></div>                        
                        <ul id="myTab" class="nav nav-tabs">
                          <li class="active"><a href="#mobile" data-toggle="tab">Call: +912242932288</a></li>
                          <li class=""><a href="#status" data-toggle="tab">SMS: PAY1 HELP to 09223178889</a></li>
                          <li class=""><a href="#details" data-toggle="tab">Write to Us</a></li>
                          <li style="display:none" id="bank_details" class=""><a href="#bank_deposit" onclick="loadBankDetails();" data-toggle="tab">Bank/Cash Deposit Details</a></li>
                          <li><a href="#change_mobile" data-toggle="tab">Change Mobile</a></li>
                          <li  ><a id="change_mobile_tab" href="#change_mobile_otp" data-toggle="tab"></a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div class="tab-pane fade" id="details">
                          <div class="table-responsive">

                           <div class="ui-content ui-scrollview-clip mainClass" style="height:373px;">
  <div class="ui-field-contain ui-body ui-br">
    	<label for="name" class="ui-input-text">Email:</label>
    	<b>help@pay1.in</b>
    </div><br/>    
    <div class="ui-field-contain ui-body ui-br">
    	<label for="name" class="ui-input-text">Subject:</label>
    	<input type="text" name="writeSub" class="form-control" id="writeSub" value="" placeholder="Subject" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset"/>
    </div><br/>
    <div class="ui-field-contain ui-body ui-br">
    	<label for="name" class="ui-input-text">Message:</label>
        <textarea type="text" name="writeMsg" class="form-control" id="writeMsg" value="" placeholder="Your Message Here" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset"></textarea>    	
    </div>
    <div id="wrt_us_loader"  class="ui-field-contain ui-body ui-br loaderAlign">
    	<label>&nbsp;</label>
    	
    	<input type="submit" onClick="writetous()" id="write-to-us" name="write-to-us" value="Send"  class="default-btn searchProcess"/></div>
    </div>

                          </div>
                        
                        </div>
<div id="bank_deposit" class="tab-pane fade">

<div class="row form-group">
        	<div class="col-lg-11" style="margin-top: 17px;margin-left: 15px;font-size: 12px;">
        		You can transfer funds to following Bank Accounts via NEFT/RTGS/Cash
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-lg-11" style="margin-left: 10px;margin-top:10px;font-size:12px;">
        		<table class="table table-hover table-bordered">
				    <thead id="accounts_head">
				      
				    </thead>
				    <tbody id="accounts_body">
				      
				    </tbody>
				  </table>
        	</div>	
        </div>
        <div class="row form-group">
        	<div class="col-lg-11" style="margin-top: 10px;margin-left: 15px;font-size: 12px;">
        		The minimum deposit amount for limit balance is Rs. 1000/-
        	</div>
        </div>
</div>                
<div id="change_mobile" class="tab-pane fade">
<div class="table-responsive" style="margin-left:20px">
<div class="ui-content ui-scrollview-clip mainClass" style="height:373px;">
<div class="row">
	<div class="form-group" id="change_mobile_form">
		<div class="ui-field-contain ui-body ui-br">
	    	<label for="name" class="ui-input-text">Old Mobile</label>
	    	<input type="text" name="old_mobile" class="form-control" id="old_mobile" value="" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset"/>
	    </div><br/>
	    <div class="ui-field-contain ui-body ui-br">
	    	<label for="name" class="ui-input-text">New Mobile</label>
	        <input type="text" name="new_mobile" class="form-control" id="new_mobile" value="" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset"/>   	
    	</div>
    	<div class="ui-field-contain ui-body ui-br">
	    	<label for="name" class="ui-input-text">Pin</label>
	        <input type="password" name="pin" class="form-control" id="pass" value="" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset"/>  	
    	</div><br/>
    	<div class="ui-field-contain ui-body ui-br loaderAlign">
	    	<label>&nbsp;</label>
	    	<input type="submit" onClick="changeMobileNumber()" value="Submit" class="default-btn searchProcess"/></div>
	    </div>
	</div>
	<div class="form-group" id="change_mobile_otp_form" style="display:none;">
		<div class="ui-field-contain ui-body ui-br" id="mobile_number">
			
		</div><br/>
	    <div class="ui-field-contain ui-body ui-br">
	    	<label for="name" class="ui-input-text">Enter the One Time Password (OTP) received by you via SMS.</label>
	        <input type="password" name="otp" class="form-control" id="otp" value="" placeholder="OTP" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset"/>    	
    	</div>
    	<div class="ui-field-contain ui-body ui-br loaderAlign">
	    	<input type="submit" onClick="authenticateMobileNumberChange()" value="Submit" class="default-btn searchProcess"/></div>
	    	<input type="submit" onClick="changeMobileNumber()" value="Resend" class="default-btn searchProcess"/></div>
	    </div>
	</div>
</div>	
</div>
</div>
</div>            
                    
