<script>
    var deviceUuid = "";
    $(function() {

    $(".opencheckplans").click(function () {
                $(".checkplans").slideDown();
                 $("#disclaimer").show();
                 $(".opencheckplans").prop('disabled', true);
                   });

                   $(".hidecheckplans").click(function(){
                   $(".checkplans").slideUp();
                   $(".opencheckplans").prop('disabled', false);
               });

                   $(document).click(function (e) {
                       if (!$(e.target).hasClass("opencheckplans") 
                           && $(e.target).parents(".checkplans").length === 0) 
                       {
                           $(".checkplans").slideUp();
                           $(".opencheckplans").prop('disabled', false);

                       }
                   });
    
    
        $('#dthRecharge').on('submit', function (event) {
            event.preventDefault();
            dthRecharge(1);
        });
        if($("#operatorType").val() == "dth"){
            $.each(PayOne.config.operators.dthRecharge , function(i ,v){
                if(v.id == $("#operatorId").val() && !v.spcl ){
                    $("#stvOrTopUp_radio2").hide();
                }            
            });
        }
        PayOne.core.initBrowsePlans(2);
       // PayOne.core.callGetPlans((15 + parseInt($("#operatorId").val())),$("#circle").val());
       PayOne.core.checkplan("getPlanDetails",(15 + parseInt($("#operatorId").val())),$("#circle").val(),"dth");
        function dthSubNoValidate(y){
            var y = y.trim();
            if(y == ""){alert("Enter mobile number");return 0;
            }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
            }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
            }
            return 1;
        }
    
        function mobileValidate(y){
            var y = y.trim();
            if(y == ""){alert("Enter mobile number");return 0;
            }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
            }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
            }
            return 1;
        }

        function amtValidate(y) {
            var err = '';
            var y = y.trim();
            if(y == ""){alert("Plese enter proper amount");return 0;
            }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
            }
            return 1;
        }
        function dthSubIdValidate(y) {
            var y = y.trim();
            if(y == ""){
                alert("Enter DTH number");return 0;        
            }else if(isNaN(y)||y.indexOf(" ")!= -1){
                alert("DTH number should contain numeric values");return 0;
            }
            return 1;
        }

        function dthRecharge(cnfFlag){            
            var mobileNo = $("#mobileNo").val().trim();
            var rechargeAmt = $("#rechargeAmt").val().trim();
            var dthSubId = $("#dthSubNo").val().trim();
            if(dthSubIdValidate(dthSubId) == '0')return;
            if(mobileValidate(mobileNo) == '0') return;	
            if(amtValidate(rechargeAmt) == '0')return;
            var stv = $("input[name='stvOrTopUp_radio']:checked").val();
            if(cnfFlag == 1){
                var cnf = "Operator: "+$('#operatorName').val()+"\nNumber: "+mobileNo+ "\nSubscriber Id:"+dthSubId+"\nAmount: Rs."+rechargeAmt+"\n";
                var r=confirm(cnf+"Press OK to confirm");
                if(r==false){
                    return false;
                }
            }
            $('#dthRechargeSubmit').button('loading');
            $.ajax({
                url: PayOne.config.urls.api,
                type: "POST",
                data: {
                    method:"dthRecharge",
                    device_type:"web",
                    mobileNumber:mobileNo,
                    subId:dthSubId,
                    operator:$("#operatorId").val(),
                    subId:dthSubId,
                    amount:rechargeAmt,/*circle:$('#mob-rec-cir').val(),*/
                    special:stv,
                    type:"flexi"
                },
                dataType: 'jsonp',
                jsonp: 'root',
                timeout: 50000,
                success: function(data, status){ //alert("hello=="+data);
                    //$('#rec_now_loader').html(tmp);
                    $('#dthRechargeSubmit').button('reset');
                    $.each(data, function(i,item){				
                        if(item.status == 'failure'){                       
                            PayOne.core.failChk(item.code,item.description,true);
                       
                        }else{
                            if(item.status == 'success'){
                                var desc = 'Recharge request sent successfully';
                                $('#success_description').html(desc);
                                $('#success').modal("show");
                                $("#dthSubNo").val("")
                                $("#mobileNo").val("");
                                $("#rechargeAmt").val("");
                                $('#retailerBal').html(item.balance);
                                PayOne.core.cookie.set("balance", item.balance, 1);
                                setTimeout(function(){
                                   window.location="/dth";  //redirect to main page
                                }, 3000);
                            }
                        }
                    });
                },
                error: function(){
                    $('#dthRechargeSubmit').button('reset');
                }
            });
        }
    
    });
</script>

<div class="breadcrumb"><a href="/<?php echo $type; ?>"><?php echo ucfirst($type); ?></a> &gt; <?php echo $operatorName; ?> &gt; Recharge</div>
<div style="width:900px;height:700px;" >
<form role="form" id="dthRecharge">
    <div class="form-group">
        <input type="hidden" name="operatorId" id="operatorId" value="<?php echo $operatorId; ?>" />
        <input type="hidden" name="operatorName" id="operatorName" value="<?php echo $operatorName; ?>" />
        <input type="hidden" name="operatorType" id="operatorType" value="<?php echo $type; ?>" />
        <?php if ($type == "mobile") { ?>
            <input style="width: 40px ; float:left;" type="radio" class="form-control" id="stvOrTopUp_radio1" name="stvOrTopUp_radio" value="0" placeholder="" checked="true"/><label for="stvOrTopUp_radio1" style="float:left;">Top Up</label>
            <input style="width: 40px; float:left;" type="radio" class="form-control" id="stvOrTopUp_radio2" name="stvOrTopUp_radio" value="1" placeholder=""/><label for="stvOrTopUp_radio2" >STV</label>
        <?php } ?>
    </div>
    <div class="form-group">
    <label for="dthSubNo">
    <?php switch($operatorId){
    	case 1://Airtel DTH
    		echo "Enter DTH Customer ID";
    		break;
    	case 2://Big TV DTH 
    		echo "Enter DTH Smart Card No.";
    		break;
    	case 3://Dish TV DTH
    		echo "Enter DTH Viewing Card No.";
    		break;
    	case 4://Sun TV DTH 
    		echo "Enter DTH Smart Card No.";
    		break;
    	case 5://Tata Sky DTH
    		echo "Enter DTH Subscriber ID";
    		break;
    	case 6://Videocon DTH
    		echo "Enter DTH Customer ID";
    		break;
    	default:
    		echo "Enter DTH Subscriber Number";
    }?>
    </label>
        
        <input type="text" class="form-control" id="dthSubNo" placeholder="" pattern="^\d{1,20}" title="Subscriber Number Should be a No" />
    </div>
    <div class="form-group">
        <label for="mobileNo"  for="mobileNo">Enter Prepaid Mobile Number</label>
        <input type="text" class="form-control" id="mobileNo" placeholder="" pattern="^\d{10,10}" title="10 Digit Mobile No" />
    </div>
    <div class="form-group" style="clear:both">
                        <div style="float:left">
						<label for="rechargeAmt">Enter Amount</label>
						<input type="text" class="form-control" id="rechargeAmt" placeholder="" pattern="^\d{1,5}(\.\d{1,2})?$" title="Amount should be a no"/></div>
                        <div style="float:left;padding-top: 32px;padding-left:8px;"id="dth_checkplan"><a href="#"><span style="align:top;" class="opencheckplans">Check Plan</span></a></div>
					</div>
					<div class="" style="clear:both">
                          <label><span style="display:none;color:#40bdab;" id="description"></span></label>     
					</div>

    <button id="dthRechargeSubmit" data-loading-text="Processing..." type="submit" class="form-control default-btn">Proceed</button>
    <span id="disclaimer" style="display:none;color:#40bdab;">Disclaimer: Tariff shown here are indicative,check exact details with your operator before proceeding  we will not not be able to liable in case you do not get the benefits mentioned.</span>
</form>
<div class="checkplans" id="show_dth_plan">
                                <div class="sliderowbar">
						<ul class="nav nav-tabs" role="tablist" id ="dth_plan">
						
						</ul>
                                              </div>
                         <div class="tab-content mT20"  id ="dth_plandetails">
    
						</div>

               </div>
</div>

<script type="text/javascript">

  

  function getPlandescription(amt,mob,description)
{
    $("#rechargeAmt").val(amt);
    $("#description").show();
    $("#description").html("Plan description : "+description);
    $("#show_"+mob+"_plan").hide();
}
</script>

