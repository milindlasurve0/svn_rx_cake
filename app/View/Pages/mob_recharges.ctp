<script>
var deviceUuid = "";
var mobileDetails = {};
var selectedPlans;
var planDetails=[];
$(function() {


 $(".opencheckplans").click(function () {
             $(".checkplans").slideDown();
              $(".opencheckplans").prop('disabled', true);
              $("#disclaimer").show();
		});

		$(".hidecheckplans").click(function(){
                $(".checkplans").slideUp();
                $(".opencheckplans").prop('disabled', false);
                
            });

		$(document).click(function (e) {
		    if (!$(e.target).hasClass("opencheckplans") 
		        && $(e.target).parents(".checkplans").length === 0) 
		    {
		        $(".checkplans").slideUp();
		        $(".opencheckplans").prop('disabled', false);
		        
		    }
		});
    
     $('#mobileRecharge').on('submit', function (event) {
         event.preventDefault();
         mobRecharge(1);
     });
     
  
     $('#circle').on('change', function (event) {
     
            
            if(this.value!= ''){
             $("#mob_plan").html('');
             $("#mob_plandetails").html('');
             var productid = $("#operatorId").val();
             PayOne.core.checkplan("getPlanDetails",productid,this.value,"mob");
             $("#mob_checkplan").show();
             }
             else {
                $("#mob_checkplan").hide();
             }
                
     });


     
    if($("#operatorType").val() == "mobile"){     
        $.each(PayOne.config.operators.mobRecharge , function(i ,v){
            if(v.id == $("#operatorId").val() && !v.spcl ){
                $(".stvOrTopUp_radio2class").hide();
                $(".stvOrTopUp_radio1class").hide();
            }            
        });
    }
    function mobileValidate(y){
        var y = y.trim();
        if(y == ""){alert("Enter mobile number");return 0;
        }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
        }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
        }
        return 1;
    }

    function amtValidate(y) {
        var err = '';
        var y = y.trim();
        if(y == ""){alert("Plese enter proper amount");return 0;
        }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
        }
        return 1;
    }

	$('#rechargeAmt').keyup(function(){
		var amount = parseInt($('#rechargeAmt').val());
		var min_amount = parseInt(PayOne.core.cookie.get('min_amount_for_prompt'));
		
		if(isNaN(min_amount)){
			return false;
		}
			
		if(isNaN(amount) || amount < min_amount){
			$('#amount_again').hide();
		}	
		else {
			$('#amount_again').show();
		}	
	});
		
    function mobRecharge(cnfFlag){

        var mobileNo = $("#mobileNo").val().trim();
        var rechargeAmt = $("#rechargeAmt").val().trim();
        if(PayOne.core.utils.mobileValidate(mobileNo) == '0') return;	
        if(PayOne.core.utils.amtValidate(rechargeAmt) == '0')return;

        var min_amount = parseInt(PayOne.core.cookie.get('min_amount_for_prompt'));
		if(!isNaN(min_amount) && rechargeAmt >= min_amount){
			if($('#recharge_amount_again').val() != rechargeAmt){
				$('#amount_again').show();
				alert("The entered amount does not match");
				return false;
			}	
		}	
		
        var stv = $("input[name='stvOrTopUp_radio']:checked").val();

        if(cnfFlag == 1){
            var cnf = "Operator: "+$('#operatorName').val()+"\nNumber: "+mobileNo+"\nAmount: Rs."+rechargeAmt+"\n";
            var r=confirm(cnf+"Press OK to confirm");
            if(r==false){
                return false;
            }
        }

        var no_prompt_within_one_hour_flag = $('#no_prompt_within_one_hour_flag').val();
        var no_operator_check_flag = $('#no_operator_check_flag').val();
        //alert("==========="+cnfFlag+","+fromLocation);
            /*if(passFlag=='F'){
                showHide('page1','pinchange');
                return;
            }*/
            //alert("==========++=");
            //var tmp = $('#rec_now_loader').html();
            //$('#rec_now_loader').html(loader);
            //alert("method=mobRecharge&mobileNumber="+mobileNo+"&operator="+$('#operatorId').val()+"&subId="+mobileNo+"&amount="+rechargeAmt+"&circle="+$('#mob-rec-cir').val()+"&special=stv&type=flexi");
        $('#mobRechargeSubmit').button('loading');
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method:"mobRecharge",
                device_type:"web",
                mobileNumber:mobileNo,
                operator:$("#operatorId").val(),
                subId:mobileNo,
                amount:rechargeAmt,/*circle:$('#mob-rec-cir').val(),*/
                special:stv,
                type:"flexi",
                recharge_prompt_flag:"1",
                no_prompt_within_one_hour_flag: no_prompt_within_one_hour_flag,
                no_operator_check_flag: no_operator_check_flag
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status){ //alert("hello=="+data);
                //$('#rec_now_loader').html(tmp);
                $('#mobRechargeSubmit').button('reset');
                $.each(data, function(i,item){			console.log(item);	
                    if(item.status == 'failure'){  
                        if(item.code == '42'){
                            $('#repeat_recharge_mobile').html(mobileNo);
                            $('#repeat_recharge_amount').html("Rs. " + rechargeAmt);
                            $('#repeat_recharge_mobile2').html(mobileNo);
                            $('#time_elapsed').html(item.lastRecharge.time_elapsed + " mins ago");
                            $('#repeat_recharge_confirm').click(function(){
                            	$('#no_prompt_within_one_hour_flag').val("1");
                            	$('#repeat_recharge_modal').modal('hide');
                            	mobRecharge(0);
                            });
							$('#repeat_recharge_modal').modal('show');
                        }
                        else if(item.code == '43'){//non_prepaid_operator_flag
                        	var current_operator = "";
                        	var alternate_operator = "";
                        	console.log(item);
                        	PayOne.config.operators.mobRecharge.forEach(function(element){
								if(element.id == $("#operatorId").val()){
									current_operator = element;
								}	
								if(item.product_id == element.id){
									alternate_operator = element;
								}	
                            });
                        	PayOne.config.operators.mobBillPayment.forEach(function(element){
								if(item.product_id == element.id){
									alternate_operator = element;
								}	
                            });
                            console.log(current_operator);
                            console.log(alternate_operator);
                            $('#alternate_operator_mobile').html(mobileNo);
                            $('#alternate_operator_amount').html("Rs. " + rechargeAmt);
                            $('#alternate_operator_nm').html(alternate_operator.name);
                            $('#operator1_logo').attr("src", current_operator.logoUrl);
                            $('#operator2_logo').attr("src", alternate_operator.logoUrl);
							$('#current_operator_name').html(current_operator.name);
							$('#alternate_operator_name').html(alternate_operator.name);
                            $('#operator1_logo').click(function(){
                            	$('#alternate_operator_modal').modal('hide');
                            	$('#no_operator_check_flag').val('1');
                            	mobRecharge(0);
                            });
                            var recharge_method = "mobile";
                            if(item.non_prepaid_operator_flag == "1"){
								recharge_method = "mobile_billpayment";
                            }     
                            
                            $('#operator2_logo').click(function(){
                            	$("#operatorId").val(alternate_operator.id);
                            	$('#alternate_operator_modal').modal('hide');
                            	$('#no_operator_check_flag').val('1');

                            	if(recharge_method == "mobile"){
                            		mobRecharge(0);
                            	}	
                            	else {
                            		window.location = window.location.protocol + "//" 
                                 	+ window.location.hostname + "/recharges/" 
                                 	+ recharge_method + "/" + alternate_operator.name + "/" + alternate_operator.id;
                            	}    
                            });
//                             $('#operator2_link').attr("href", window.location.protocol + "//" 
//                                     + window.location.hostname + "/recharges/" 
//                                     + recharge_method + "/" + alternate_operator.name + "/" + alternate_operator.id);
                        	$('#alternate_operator_modal').modal('show');
                        }
                        else                             
                        	PayOne.core.failChk(item.code, item.description, true);
                       
                    }else{
                        if(item.status == 'success'){
                            var desc = 'Recharge request sent successfully';
                            $('#success_description').html(desc);
                            $('#success').modal("show");
                            //alert($('#mobileNo').attr('value', ""));
                            $("#mobileNo").val("");
                            $("#rechargeAmt").val("");
                            //$('#MRODd').attr('value', '');
                            //$('#MROtitle').html('');
                            //showHide('page1','navMobile');
                            $('#retailerBal').html(item.balance);                            
                            PayOne.core.cookie.set("balance", item.balance, 1);
                            setTimeout(function(){
                               window.location="/mobile";  //redirect to main page
                            }, 3000                                      );
                        }
                    }
                });
            },
            error: function(){
                $('#mobRechargeSubmit').button('reset');
                //$('#rec_now_loader').html(tmp);
            }
        });
    }
    renderCircleOptions();
    function renderCircleOptions(){
       
        var htmlStr = "";
        htmlStr = "<option value=''>Select Circle</option>"
        $.each(PayOne.config.circles,function(i,v){
            htmlStr = htmlStr +"<option value=\""+v.code+"\">"+v.name+"</option>";
        });
        //alert(htmlStr);
        $("#circle").html(htmlStr);
    }


});

function getcircle(value)
{
  if(value.length!='' && value.length==9){
   var number = value;
   var methodtype = "getMobileDetails";
    $.ajax({
		url: PayOne.config.urls.api,
		type: "POST",
		data:{mobile : number,method :methodtype},
		dataType: 'jsonp',
        jsonp: 'root',
        timeout: 50000,
		success: function(data, status){
         var prodid  ="<?php echo $operatorId ;?>";
         browseplan(prodid,data[0].details.area);
         $("#rechargeAmt").focus();
  
       },
       error: function (xhr,error) {
       console.log(xhr);
    }
});
   
}
}

function browseplan(productid,circle)
{
    $("#mob_plan").html('');
    $("#mob_plandetails").html('');
    $("#mob_checkplan").show();
   if(circle!=''){
   $("#circle").val(circle);
    PayOne.core.checkplan("getPlanDetails",productid,circle,"mob");
   }
   else if(circle==''){
   $("#circle").show();
   

   }

}




</script>
              
            <div class="breadcrumb"><a href="/<?php echo $type ;?>"><?php echo ucfirst($type);?></a> &gt; <?php echo $operatorName ;?> &gt; Recharge</div>
                    <div style="width:900px;height:700px;" >
	            <form role="form" id="mobileRecharge">
                    <div class="form-group">
                        <input type="hidden" name="operatorId" id="operatorId" value="<?php echo $operatorId ;?>" />
                        <input type="hidden" name="operatorName" id="operatorName" value="<?php echo $operatorName ;?>" />
                        <input type="hidden" name="operatorType" id="operatorType" value="<?php echo $type ;?>" />
                        <?php if($type == "mobile"){?>
						<input style="width: 40px ; float:left;" type="radio" class="form-control stvOrTopUp_radio1class" id="stvOrTopUp_radio1" name="stvOrTopUp_radio" value="0" placeholder="" checked="true"/><label for="stvOrTopUp_radio1" class="stvOrTopUp_radio1class" style="float:left;">Top Up</label>
                        <input style="width: 40px; float:left;" type="radio" class="form-control stvOrTopUp_radio2class" id="stvOrTopUp_radio2" name="stvOrTopUp_radio" value="1" placeholder=""/><label for="stvOrTopUp_radio2" class="stvOrTopUp_radio2class">STV</label>
						<?php }?>
					</div>
					<div class="form-group">
						<label for="mobileNo"  for="mobileNo">Enter Prepaid Mobile Number</label>
						<input type="text" class="form-control" id="mobileNo" onkeypress="getcircle(this.value);return isNumberKey(event)" maxlength="10" placeholder="" pattern="^\d{10,10}" title="10 Digit Mobile No" />
					</div>
					<div class="form-group" style="clear:both">
                        <div style="float:left">
						<label for="rechargeAmt">Enter Amount</label>
						
						<input type="text" class="form-control" id="rechargeAmt" placeholder="" pattern="^\d{1,5}(\.\d{1,2})?$" title="Amount should be a no"/></div>
                        <div style="float:left;padding-top: 32px;padding-left: 8px;" id="mob_checkplan"><a href="#"><span style="align:top;" class="opencheckplans"  >Check Plan</span></a></div>
					</div>
					<div class="form-group" id="amount_again" style="clear:both;display:none;" >
                        <div style="float:left">
						<label for="rechargeAmt">Re-enter Amount</label>
						<input id="recharge_amount_again" type="text"  class="form-control"  placeholder="" pattern="^\d{1,5}(\.\d{1,2})?$"/></div>
					</div>
					<div class="" style="clear:both">
                          <label><span style="display:none;color:#40bdab;" id="description"></span></label>     
					</div>
					<button id="mobRechargeSubmit" data-loading-text="Processing..." type="submit" class="form-control default-btn">Proceed</button>
                    <span id="disclaimer" style="display:none;color:#40bdab;">Disclaimer: Tariff shown here are indicative,check exact details with your operator before proceeding  we will not not be able to liable in case you do not get the benefits mentioned.</span>
                </form>
                <input type="hidden" id="no_prompt_within_one_hour_flag" value="0" />
                <input type="hidden" id="no_operator_check_flag" value="0" />
                
                <div class="checkplans" id="show_mob_plan">
                                <div class="sliderowbar">
						<ul class="nav nav-tabs" role="tablist" id ="mob_plan">
						
						</ul>
                                    </div>
                                    <table id="mobile_circle">
                                   <tr width="673">
                                   <td><label for="mobileCircle"></label>
                                    <select class="form-control"   id="circle">
                                    </select></td>
                                    </tr>
                                    </table>
                         <div class="tab-content mT20"  id ="mob_plandetails">
    
						</div>

               </div>

 </div>
</div>

<div id="repeat_recharge_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="row" style="margin-left:10px;margin-right:10px;text-align: center;">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <span style="font-weight: bold;">Repeat Recharge Alert</span>
	    </div>  
	    <div class="row" style="text-align:center;margin-top:10px;">
        	<div class="col-lg-6">
        		<label>Mobile Number: <span id="repeat_recharge_mobile">9167787891</span></label>
        	</div>
        	<div class="col-lg-6">
        		<label>Amount: <span id="repeat_recharge_amount">Rs. 10</span></label>
        	</div>	
        </div>
        <div class="row" style="font-size: 12px;margin-left: 10px;margin-top: 10px;">
        	Last successful recharge on <span id="repeat_recharge_mobile2"></span> was <span id="time_elapsed" style='color:red;'></span>. Are you sure you want to repeat this recharge?
        </div>
        <div class="row" style="text-align:center;margin-top:30px;">
        	<div class="col-lg-6">
        		<span id="repeat_recharge_confirm" class="btn btn-primary">Yes</span>
        	</div>
        	<div class="col-lg-6">
        		<a class="btn btn-default" data-dismiss="modal">No</a>
        	</div>
        </div>
      </div>
    </div>

  </div>
</div>

<div id="alternate_operator_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
      	<div class="row" style="margin-left:10px;margin-right:10px;text-align: center;">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <span style="font-weight: bold;">Select Operator Again</span>
	    </div>    
        <div class="row" style="text-align:center;margin-top:10px;">
        	<div class="col-lg-6">
        		<label>Mobile Number: <span id="alternate_operator_mobile">9167787891</span></label>
        	</div>
        	<div class="col-lg-6">
        		<label>Amount: <span id="alternate_operator_amount">Rs. 10</span></label>
        	</div>	
        </div>
        <div class="row" style="font-size: 12px;margin-left: 10px;margin-top: 10px;">
        	This number is registered as a <span id="alternate_operator_nm"></span> operator. Kindly, select the operator again.
        </div>
        <div class="row" style="text-align:center;margin-top:30px;">
        	<div class="col-lg-6">
        		<img src="" id="operator1_logo" style="cursor:pointer"/>
        	</div>
        	<div class="col-lg-6">
        		<!-- <a id="operator2_link" href=""> --><img src="" id="operator2_logo" style="cursor:pointer"/>
        	</div>
        </div>
        <div class="row" style="text-align:center;margin-bottom: 15px;">
        	<div class="col-lg-6">
        		<span id="current_operator_name"></span>
        	</div>
        	<div class="col-lg-6">
        		<span id="alternate_operator_name"></span>
        	</div>
        </div>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  function getPlandescription(amt,mob,description)
{
    $("#rechargeAmt").val(amt);
    $("#description").show();
    $("#description").html("Plan description : "+description);
    $("#show_"+mob+"_plan").hide();
}

function isNumberKey(evt){
         var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true
     }
</script>

				
