<script>
    
    $(function() {
        
        var htmlStr = "";
        $.each(PayOne.config.operators.mobRecharge , function(i ,v){
            htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/mobile/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p>"+v.name+"</p></a>"
        });
        $("#mobile").html(htmlStr);
        
        
        transStatusTable  = $('#transStatusTable').DataTable({"aaSorting": []});
        lastTenTransTable = $('#lastTenTransTable').DataTable({"aaSorting": []});
        
        
        $('#transDate1').datepicker({
            "format": "dd-mm-yyyy"        
        });
        $('#transDate2').datepicker({
            "format": "dd-mm-yyyy"
            
        });
        
        Date.daysBetween = function( date1, date2 ) {
            //Get 1 day in milliseconds
            var one_day=1000*60*60*24;
            
            // Convert both dates to milliseconds
            var date1_ms = date1.getTime();
            var date2_ms = date2.getTime();
            
            // Calculate the difference in milliseconds
            var difference_ms = date2_ms - date1_ms;
            
            // Convert back to days and return
            return Math.round(difference_ms/one_day); 
        }
        
        $('#searchReversal').click(function (e) {
            
            var date_from = $('#transDate1').val();
            //var date_to   = $('#transDate2').val();
            if(date_from == '' ){
                alert("Please select proper date.");
            } else {
                //$('#date_err').hide();
                var date_from_arr = date_from.split("-");
                var dt = date_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                PayOne.reports.renderTransStatus("mob",1,dt,true,"#transStatusTableBody",transStatusTable);                            
            }
            
        });
        
        
        
        $('#myTab a').on("click",function (e) {        
            var hash  = $(this).attr('href');        
            if(hash=="#details"){
                PayOne.reports.lastTran(1,"#lastTenTransTableBody",lastTenTransTable);// 1 for mobile
            } 
        });
        $('#searchByNo').on("click",function (e) {         
            var no = $("#searchNo").val();  
            if(no == ""){
                alert("Please enter a valid mobile no.");return;
            }
            PayOne.reports.lastTranSearchByNo(1,no,"#lastTenTransTableBody",lastTenTransTable);// 1 for mobile                    
        });
        
        var date = new Date();
        var dt =  date.getFullYear() +"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+(date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate());  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
        var callLogin = false;
        PayOne.reports.renderTransStatus("mob",1,dt,callLogin,"#transStatusTableBody",transStatusTable);
        
    });
</script>
<div class="breadcrumb">Mobile</div>                        
<ul id="myTab" class="nav nav-tabs">
    <li class="active"><a href="#mobile" data-toggle="tab">Mobile</a></li>
    <li class=""><a href="#dth" data-toggle="tab">DTH</a></li>
    <li class=""><a href="#bill_payment" data-toggle="tab">Bill Payment</a></li>
</ul>
<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade active in" id="mobile">

        <a class="mobileList" href="#">
            <img src="<?php echo Configure::read('AWS_URL'); ?>/images/pages/square.png">
            <p>Mobile1</p>
        </a>

    </div>
    <div class="tab-pane fade" id="dth">
        <div class="form-inline StatusSearch">
            <span >Date</span><input style="margin-left:10px;width:150px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y"); ?>" data-date-format="dd-mm-yyyy" readonly>
<!--                                    <span >To</span><input   style="margin-left:10px;width:150px;" id="transDate2" type="text" class="form-control" value="<?php echo date("d-m-Y"); ?>" data-date-format="dd-mm-yyyy" readonly>-->
            <span id="searchReversal" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>
        </div>
        <div class="table-responsive">

            <table class="table table-striped table-hover order-column" id="transStatusTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                    </tr>
                </thead>
                <tbody id="transStatusTableBody">

                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="bill_payment">
        <div class="table-responsive">
            <div class="form-inline RequestSearch">
                <input id="searchNo" type="text" class="form-control" placeholder="Enter 10 digit No"/>
                <input id="searchByNo" type="submit" class="default-btn" value="search"/>   
            </div>

            <table class="table table-striped table-hover order-column" id="lastTenTransTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                    </tr>
                </thead>
                <tbody id="lastTenTransTableBody">                                        

                </tbody>
            </table>
        </div>
    </div>
</div>



