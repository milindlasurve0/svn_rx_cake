<script>

$(function() {

    var htmlStr = "";
    $.each(PayOne.config.operators.mobRecharge , function(i ,v){
       htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/mobile/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p>"+v.name+"</p></a>"
    });
    // $("#mobile").html(htmlStr);
    
    //report_transactions_tables = $('#report_transactions_tables').dataTable();
    report_mob_transactions_tables = $('#report_mob_transactions_tables').DataTable({"aaSorting": []});
    report_dth_transactions_tables = $('#report_dth_transactions_tables').DataTable({"aaSorting": []});
    report_vas_transactions_tables = $('#report_vas_transactions_tables').DataTable({"aaSorting": []});
    
    report_bill_transactions_tables = $('#report_bill_transactions_tables').DataTable({"aaSorting": []});
    report_wallet_transactions_tables = $('#report_wallet_transactions_tables').DataTable({"aaSorting": []});
    report_utility_transactions_tables = $('#report_utility_transactions_tables').DataTable({"aaSorting": []});
    report_online_payment_transactions_tables = $('#report_online_payment_transactions_tables').DataTable({"aaSorting": []});
    
    //----------for Mobile ---------
    $('#mobTransDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#mobTransDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});
    
    //----------for Dth ---------
    $('#dthTransDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#dthTransDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});
    
    //----------for Vas ---------
    $('#vasTransDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#vasTransDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});
    
    //----------for Bill ---------
    $('#billTransDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#billTransDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});
    
    //----------for Wallet ---------
    $('#walletTransDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#walletTransDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});
   //-------------for utility-----------
$('#utilityTransDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#utilityTransDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});

  //----------for Online Payment ---------
    $('#online_paymentTransDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
    $('#online_paymentTransDate2').datepicker({
		"format": "dd-mm-yyyy"
        
	});
    /*$('#searchReversal').click(function (e) {        
        var date_from = $('#transDate1').val();
        if(date_from == '' ){
                alert("Please select proper date.");
        } else {
                var date_from_arr = date_from.split("-");
                var dt = date_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                PayOne.reports.renderTransStatus("mob",1,dt,true);                            
        }        
    });*/
    
    
    
    $('#myTab a').on("click",function (e) {        
        var hash  = $(this).attr('href');        
        if(hash=="#details"){
            PayOne.reports.lastTran(1);// 1 for mobile
        } 
    });
    $('#searchMobTransactionData').on("click",function (e) {   
        
      
        var mobTransDate1 = $("#mobTransDate1").val();  
        var mobTransDate2 = $("#mobTransDate2").val();  
        
        mobTransDate1 = mobTransDate1.split("-").reverse().join("-");  
        mobTransDate2 = mobTransDate2.split("-").reverse().join("-");
        
        var eleId = "#report_mob_transactions_table_body";
        PayOne.reports.renderTransactionsData(mobTransDate1,mobTransDate2,1,true,eleId,report_mob_transactions_tables);//pageNo = 0 AND items_per_page = 0            
    });
    
    $('#searchDthTransactionData').on("click",function (e) {     
        //alert("Hello");return;
        var dthTransDate1 = $("#dthTransDate1").val();  
        var dthTransDate2 = $("#dthTransDate2").val();  
        
        
        dthTransDate1 = dthTransDate1.split("-").reverse().join("-");  
        dthTransDate2 = dthTransDate2.split("-").reverse().join("-");
        
        //var items_per_page = $("#items_per_page").val();
        //var page_no = $("#page_no").val();
        var eleId = "#report_dth_transactions_table_body";
        PayOne.reports.renderTransactionsData(dthTransDate1,dthTransDate2,2,true,eleId,report_dth_transactions_tables);//pageNo = 0 AND items_per_page = 0            
    });
    
    $('#searchVasTransactionData').on("click",function (e) {     
        //alert("Hello");return;
        var vasTransDate1 = $("#vasTransDate1").val();  
        var vasTransDate2 = $("#vasTransDate2").val();  
        
        vasTransDate1 = vasTransDate1.split("-").reverse().join("-");  
        vasTransDate2 = vasTransDate2.split("-").reverse().join("-"); 
        
        //var items_per_page = $("#items_per_page").val();
        //var page_no = $("#page_no").val();
        var eleId = "#report_vas_transactions_table_body";
        PayOne.reports.renderTransactionsData(vasTransDate1,vasTransDate2,3,true,eleId,report_vas_transactions_tables);//pageNo = 0 AND items_per_page = 0            
    });
    
    
    
    $('#searchBillTransactionData').on("click",function (e) {     
        //alert("Hello");return;
        var billTransDate1 = $("#billTransDate1").val();  
        var billTransDate2 = $("#billTransDate2").val();  
        
        billTransDate1 = billTransDate1.split("-").reverse().join("-");  
        billTransDate2 = billTransDate2.split("-").reverse().join("-"); 
        
        //var items_per_page = $("#items_per_page").val();
        //var page_no = $("#page_no").val();
        var eleId = "#report_bill_transactions_table_body";
        PayOne.reports.renderTransactionsData(billTransDate1,billTransDate2,4,true,eleId,report_bill_transactions_tables);//pageNo = 0 AND items_per_page = 0            
    });
    
    $('#searchWalletTransactionData').on("click",function (e) {     
        //alert("Hello");return;
        var walletTransDate1 = $("#walletTransDate1").val();  
        var walletTransDate2 = $("#walletTransDate2").val();  
        
        walletTransDate1 = walletTransDate1.split("-").reverse().join("-");  
        walletTransDate2 = walletTransDate2.split("-").reverse().join("-"); 
        
        //var items_per_page = $("#items_per_page").val();
        //var page_no = $("#page_no").val();
        var eleId = "#report_vas_transactions_table_body";
        PayOne.reports.renderTransactionsData(walletTransDate1,walletTransDate2,5,true,eleId,report_wallet_transactions_tables);//pageNo = 0 AND items_per_page = 0            
    });

 $('#searchutilityTransactionData').on("click",function (e) {     
        
        var utilityTransDate1 = $("#utilityTransDate1").val();  
        var utilityTransDate2 = $("#utilityTransDate2").val();  
        
        utilityTransDate1 = utilityTransDate1.split("-").reverse().join("-");  
        utilityTransDate2 = utilityTransDate2.split("-").reverse().join("-"); 
        
        var eleId = "#report_utility_transactions_table_body";
        PayOne.reports.renderTransactionsData(utilityTransDate1,utilityTransDate2,6,true,eleId,report_utility_transactions_tables);//pageNo = 0 AND items_per_page = 0            
    });

 $('#searchonline_paymentTransactionData').on("click",function (e) {     
	    //alert("Hello");return;
	    var online_paymentTransDate1 = $("#online_paymentTransDate1").val();  
	    var online_paymentTransDate2 = $("#online_paymentTransDate2").val();  
	    
	    online_paymentTransDate1 = online_paymentTransDate1.split("-").reverse().join("-");  
	    online_paymentTransDate2 = online_paymentTransDate2.split("-").reverse().join("-"); 
	    
	    //var items_per_page = $("#items_per_page").val();
	    //var page_no = $("#page_no").val();
	    var eleId = "#report_online_payment_transactions_table_body";
	    PayOne.reports.renderTransactionsData(online_paymentTransDate1,online_paymentTransDate2,7,true,eleId,report_online_payment_transactions_tables);//pageNo = 0 AND items_per_page = 0            
	});
	
    var date = new Date();
    var dt =  (date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate())+"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+date.getFullYear() ;  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
    
    var callLogin = false;
    
    var items_per_page = $("#items_per_page").val();
    var eleId = "#report_mob_transactions_table_body";
    dt = dt.split("-").reverse().join("-"); 
    PayOne.reports.renderTransactionsData(dt,dt,1,true,eleId,report_mob_transactions_tables);//pageNo = 0 AND items_per_page = 0
    
});

</script>
<div class="breadcrumb">Report &gt; Transaction</div>
                        <ul id="myTab" class="nav nav-tabs">
                          <li class="active"><a href="#mobile" data-toggle="tab">Mobile</a></li>
                          <li class=""><a href="#dth" data-toggle="tab">DTH</a></li>
                          <li class=""><a href="#entertainment" data-toggle="tab">Entertainment</a></li>
                          <li class=""><a href="#bill_payment" data-toggle="tab">Bill Payment</a></li>
                          <li class=""><a href="#wallet" data-toggle="tab">Wallet Topup</a></li> 
                           <li class=""><a href="#utility" data-toggle="tab">Utility Bill Payment</a></li>
                           <li class=""><a href="#online_payment" data-toggle="tab">C2D Payment</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div class="tab-pane fade active in" id="mobile">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                             <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="mobTransDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="mobTransDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                                <span id="searchMobTransactionData" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>-->
    <!--<button id="searchMobTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button>-->
          <input id="searchMobTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>
                            </div>
							<div class="table-responsive">
                           	<table class="table table-striped table-hover order-column" id="report_mob_transactions_tables" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_mob_transactions_table_body">
                                        
                                      </tbody>
                                    </table>
								</div>
                          </div>
                          <div class="tab-pane fade" id="dth">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                             <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="dthTransDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="dthTransDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                                <span id="searchDthTransactionData" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>-->
                                <input id="searchDthTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="Search"/>
                            </div>
							<div class="table-responsive">
                            	<table class="table table-striped table-hover order-column" id="report_dth_transactions_tables" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_dth_transactions_table_body">
                                        
                                      </tbody>
                                    </table>
								</div>
                          </div>
                          <div class="tab-pane fade" id="entertainment">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                            <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="vasTransDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="vasTransDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                                <span id="searchVasTransactionData" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>-->
                                <input id="searchVasTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="Search"/>
                            </div>
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_vas_transactions_tables" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_vas_transactions_table_body">
                                        
                                      </tbody>
                                    </table>
							</div>
                    </div>
                    <div class="tab-pane fade" id="bill_payment">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                            <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="billTransDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="billTransDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                                <span id="searchVasTransactionData" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>-->
                                <input id="searchBillTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="Search"/>
                            </div>
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_bill_transactions_tables" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_bill_transactions_table_body">
                                        
                                      </tbody>
                                    </table>
							</div>
                    </div>
                    <div class="tab-pane fade" id="wallet">

                            <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="walletTransDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="walletTransDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>

                                <input id="searchWalletTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="Search"/>
                            </div>
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_wallet_transactions_tables" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_wallet_transactions_table_body">
                                        
                                      </tbody>
                                    </table>
							</div>
                    </div>

     <div class="tab-pane fade" id="utility">

                            <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="utilityTransDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="utilityTransDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>

                                <input id="searchutilityTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="Search"/>
                            </div>
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_utility_transactions_tables" cellspacing="0" width="100%">
                                      <thead>
                                         <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_utility_transactions_table_body">
                                      </tbody>
                                    </table>
							</div>
                    </div>
<div class="tab-pane fade" id="online_payment">

                            <div class="form-inline StatusSearch" style="width: auto;">
                                <span >Date</span><input style="margin-left:10px;" id="online_paymentTransDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
                                <span >To</span><input   style="margin-left:10px;" id="online_paymentTransDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>

                                <input id="searchonline_paymentTransactionData" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="Search"/>
                            </div>
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_online_payment_transactions_tables" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>                                              							
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Trans Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Mobile</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amt</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Opening</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Closing</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Query</th>
                                          </tr>
                                      </thead>
                                      <tbody id="report_online_payment_transactions_table_body">
                                        
                                      </tbody>
                                    </table>
							</div>
                    </div>
 
                </div>

