<style type="text/css">

.terms p {
margin: 0px;
text-align:justify;
padding:0 0 10px 0;
font-size:12px;
font-family:"Encode Sans Normal";
}

.terms h2{
font-size:14px;
font-family:"Encode Sans Narrow Medium";
margin:0;
padding:10px 0 5px 0;
color:#513D98;
}

.terms ul{
margin:0 0 10px 0;
list-style-type: disc;
padding-left:20px;
}

.terms li{
font-size:12px;
text-align:justify;
margin:0 0 2px 0;
font-family:"Encode Sans Normal";
}
</style>

<div class="terms">
<h2>Cancellation & Refund Policy:</h2>
<p>We at PAY1 don’t allow any cancellation by the user/customer/You. Transactions initiated once on the Site/Mobile Application by You cannot be further cancelled. In case of a failed Transaction at the payment gateway or by the operator you will get the refund within 7 working days.</p>

<h2>Refund Policy:</h2>
<p>All sales of Recharge are final with no refund or exchange permitted. You are responsible for the mobile number or DTH account you purchase Recharge for and all charges that result from those purchases. PAY1 is not responsible for any purchase of Recharge or Bill payment for an incorrect mobile number or DTH account number. However, if in a transaction performed by you on the Site/Mobile Application, money has been charged to your card or bank account and a Recharge is not delivered within 24 hours of your completion of the transaction then you may inform us by sending an email to our customer services email address mentioned on the Contact Us page. Please include in the email the following details - the mobile number (or DTH account number), operator name, Recharge value, Transaction date and Order Number. PAY1 shall investigate the incident and if it is found that money was indeed charged to your card or bank account without delivery of the Recharge then you will be refunded the money within 7 working days.<br/><br/>If your transaction is using Pay1 Wallet as payment method in that case money will be refunded to your Pay1 wallet. If you used Credit Card/Debit Card or net banking for recharge, in that case money will be refunded to your bank or card whatever may be the case.</p>

<h2>Return Policy:</h2>
<p>We do not have any Return Policy for our users/Customers/You.
As per the cancellation Policy; we do not encourage any kind of cancellation at Pay1's website/Mobile or WEB Apllication.
Hence we also do not provide Return for any transaction which has been made on our website/ Mobile or WEB Application.
Once a transaction has been processed successfully a user/Customer/You cannot claim for any return for the same.</p>
</div>