<style type="text/css">

.terms p {
margin: 0px;
text-align:justify;
padding:0 0 10px 0;
font-size:12px;
font-family:"Encode Sans Normal";
}

.terms h2{
font-size:14px;
font-family:"Encode Sans Narrow Medium";
margin:0;
padding:10px 0 5px 0;
color:#513D98;
}

.terms ul{
margin:0 0 10px 0;
list-style-type: disc;
padding-left:20px;
}

.terms li{
font-size:12px;
text-align:justify;
margin:0 0 2px 0;
font-family:"Encode Sans Normal";
}
</style>

<div class="terms" style="overflow-y:scroll; height:700px;">
<h2>Terms &amp; Conditions</h2>

<p>Please read the following terms and conditions carefully before registering, accessing, browsing, downloading or using the site. By accessing or using the site or by using the service, you agree to be bound by these terms and conditions set forth below including any additional guidelines and future modifications.If at any time you do not agree to these terms and conditions or do not wish to be bound by these terms and conditions, you may not access or use the site and immediately terminate your use of the services.</p>

<h2>Your Agreement to these Terms and Conditions of Service</h2>

<p>The Terms and Conditions (as may be amended from time to time, the "Agreement" is a legal contract between you, an individual customer, member, user, or beneficiary of this service of at least 18 years of age ("You"), and Mindsarray Technologies Pvt. Ltd. having its registered office At 24, 1st Main, Teachers Colony, Kormangala, Bangalore, Karnataka &ndash; 560034 and headquartered at 726, Raheja&prime;s Metroplex (IJIMIMA), Link Road, Malad West, Mumbai - 400064. PAY1 is a platform owned by Mindsarray Technologies Pvt. Ltd. (hereinafter referred to as "MTPL"). MTPL is the owner of the website <a href="http://www.pay1.in" target="_blank">www.pay1.in</a>, PAY1 mobile application, PAY1 WAP site (Collectively referred to as "PAY1") hence all the rights, benefits, liabilities &amp; obligations under the following terms &amp; conditions shall accrue to the benefit of MTPL. (together with its subsidiaries and other affiliates, "us", "we", "PAY1" or "PAY1"), regarding Your use of our prepaid mobile/DTH recharge purchasing services (known as "Service") to purchase prepaid mobile/DTH recharge (known as "Recharge") and related Services. The <a href="http://www.pay1.in" target="_blank"> www.pay1.in</a> web-site (known as the "Site") and Service is subject to your compliance with the terms and conditions set forth below including all exhibits. PAY1 reserves the right, at its sole discretion, to revise, add, or delete portions of these terms and conditions any time without further notice. You shall re-visit the "Terms &amp; Conditions" link from time to time to stay abreast of any changes that the "Site" may introduce. The services hereunder are offered by PAY1, located at: Mindsarray Technologies Private Limited: 726, Raheja's Metroplex (IJIMIMA), Link Road, Malad West, Mumbai &ndash; 400064, India.</p>

<h2>Eligibility</h2>

<p>The services are not available to persons under the age of 13 or to anyone previously suspended or removed from the services by PAY1. By accepting these Terms &amp; Conditions or by otherwise using the Services or the Site, You represent that You are at least 18 years of age and have not been previously suspended or removed from the Services.You represent and warrant that you have the right, authority, and capacity to enter into this Agreement and to abide by all of the terms and conditions of this Agreement. You shall not impersonate any person or entity, or falsely state or otherwise misrepresent identity, age or affiliation with any person or entity.</p>

<h2>Coupon Redemption</h2>

<p>Coupon redemption is purely subjected to standard and specified terms and conditions mentioned by the respective retailer. Coupons are issued on behalf of the respective retailer. Hence, any damages, injuries, losses incurred by the end user by using the coupon is not the responsibility of PAY1.</p>

<h2>Bill Payments</h2>

<p>In order to use the Service, You may need to obtain access to the World Wide Web, either directly or through devices that access web-based content, and pay any service fees associated with such access.In addition, you must have all equipment necessary to make such connection to the World Wide Web, including a computer and modem or other access device.</p>

<p>PAY1 and/or the PAY1 Business Partner reserve the right to charge and recover from the User, fees for availing the Services. These changes shall be effective from the time they are posted on to the PAY1 Website or over the PAY1 Business Partner channel through which you are availing the Service. You are bound by such revisions and should therefore visit the PAY1 Website or check with the PAY1 Business Partner channel through which you are availing the Service to review the current fees from time to time.</p>

<p>In the event that you stop or seek a reversal of the Payment Instructions as may have been submitted, PAY1 shall be entitled to charge and recover you and you shall be liable to pay such charges to the Bill Payment Service as may be decided by PAY1. These charges shall be charged on to your designated Payment Account or in any other manner as may be decided by PAY1. PAY1 offers a convenient and secure way to make payments towards identified Biller(s) using a valid Payment Account.</p>

<p>Depending upon the PAY1 Business Partner through whom the Service is availed by you (i) the specific features of the Service may differ (ii) the number of Billers available over the Service can differ (iii) the type and range of Payment Accounts that can be used to issue a Payment Instructions can differ and (iv) the modes/devices over which the Service can be accessed can differ; and (v) the charges, fees for availing the Service or any aspect of the Service can differ. Specific details related to these aspects would be available with the PAY1 Business Partner or the channel over which the Service is being availed. From time to time, PAY1, at its sole discretion, add to or delete from such list of Billers or types of Payment Accounts that can be used in respect of making payments to a Biller.In any event (i) the type and range of Payment Accounts that can be used for making payments may differ for each Biller depending on Biller specifications (ii) there may be an additional fees/charge when using certain types of Payment Accounts in respect of a Biller; and (iii) the terms upon which a payment can be made to a Biller can differ depending on whether a Card or a Bank Account is used to issue the Payment Instruction. Further depending on the specific facilities allowed by through a PAY1 Business Partner, payments to a Biller can be made either (a) by issuing a Payment Instruction for an online debit/charge to a Payment Account; or (b) by scheduling an automated debit to a Payment Account; or (c) by issuing a Payment instruction for an online debit/charge to PAY1 wallet.</p>

<p>In using the Bill Payment Service, You agree to: (a) Provide true, accurate, current and complete information about yourself ("Registration Data"), Your Payment Account details ("Payment Data"), Your Biller details ("Biller Data") and</p>

<p>(b) Maintain and promptly update the Registration Data, Payment Data and Biller Data to keep it true, accurate, current and complete. If you provide any information that is untrue, inaccurate, not current or incomplete, or PAY1 has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, PAY1 has the right to suspend or terminate you account and refuse any and all current or future use of the Service (or any portion thereof). The term biller includes telecom operators.</p>

<p>PAY1 assumes no responsibility and shall incur no liability if it is unable to effect any Payment Instruction(s) on the Payment Date owing to any one or more of the following circumstances:</p>

<ul class="terms">
	<li>If the Payment Instruction(s) issued by you is/are incomplete, inaccurate, and invalid or delayed.</li>
	<li>If the Payment Account has insufficient funds/limits to cover for the amount as mentioned in the Payment Instruction(s)</li>
	<li>If the funds available in the Payment Account are under any encumbrance or charge</li>
	<li>If your Bank or the NCC refuses or delays honoring the Payment Instruction(s)</li>
	<li>If payment is not processed by biller upon receipt.</li>
	<li>Circumstances beyond the control of PAY1 (including, but not limited to, fire, flood, natural disasters, bank strikes, power failure, systems failure like computer or telephone lines breakdown due to an unforeseeable cause or interference from an outside force) In case the bill payment is not effected for any reason, you will be intimated about the failed payment by an e-mail.</li>
</ul>

<h2>Prohibited Conduct</h2>

<p>By using the Services you agree not to:</p>

<ul class="terms">
	<li>use the Services for any purposes other than to purchase Recharge of Telcos and/or to access the Services in accordance with these Terms and Conditions and as such services are offered by PAY1</li>
	<li>impersonate any person or entity, falsely claim or otherwise misrepresent an affiliation with any person or entity, or access the accounts of others without permission, forge another persons' digital signature, misrepresent the source, identity, or content of information transmitted via the Services, perform any other similar fraudulent activity or otherwise purchase Recharge with what we reasonably believe to be potentially fraudulent funds</li>
	<li>infringe our or any third party's intellectual property rights, rights of publicity or privacy</li>
	<li>use the Services if You are under the age of eighteen (18) without a parental sponsor or in any event use the Services if You are under the age of thirteen (13) years old even with a parental sponsor and in accordance with applicable law</li>
	<li>post or transmit any message which is libelous, defamatory or which discloses private or personal matters concerning any person</li>
	<li>post or transmit any message, data, image or program which is pornographic in nature</li>
	<li>refuse to cooperate in an investigation or provide confirmation of Your identity or any other 0information You provide to PAY1</li>
	<li>remove, circumvent, disable, damage or otherwise interfere with security-related features of the Services and the Site or features that enforce limitations on the use of the Services</li>
	<li>reverse engineer, decompile, disassemble or otherwise attempt to discover the source code of the Services or any part thereof, except and only to the extent that such prohibition is expressly prohibited by applicable law notwithstanding this limitation</li>
	<li>use the Services in any manner that could damage, disable, overburden, or impair it, including, without limitation, using the Services in a n automated manner</li>
	<li>modify, adapt, translate or create derivative works based upon the Services and the Site or any part thereof, except and only to the extent that such prohibition is expressly prohibited by applicable law notwithstanding this limitation</li>
	<li>intentionally interfere with or damage operation of the Services or any other user's enjoyment of it, by any means, including uploading or otherwise disseminating viruses, adware, spyware, worms, or other malicious code or file with contaminating or destructive features</li>
	<li>use any robot, spider, other automatic device, or manual process to monitor or copy the Site without prior written permission</li>
	<li>interfere or disrupt this Site or networks connected to this Site</li>
	<li>take any action that imposes an unreasonably or disproportionately large load on our infrastructure/ network</li>
	<li>use any device, software or routine to bypass the Site's robot exclusion headers, or interfere or attempt to interfere, with the Services</li>
	<li>forge headers or manipulate identifiers or other data in order to disguise the origin of any content transmitted through our Site or to manipulate your presence on our Site</li>
	<li>sell the Services, information, or software associated with or derived from it</li>
	<li>use the facilities and capabilities of the Site to conduct any activity or solicit the performance of any illegal activity or other activity which infringes the rights of others</li>
	<li>breach this Agreement or any other PAY1 agreement or policy</li>
	<li>provide false, inaccurate or misleading information</li>
	<li>use the Site to collect or obtain personal information, including without limitation, financial information, about other users of the Site</li>
	<li>purchase Recharge with what PAY1 reasonably believes to be potentially fraudulent funds</li>
	<li>use the Services in a manner that results in or may result in complaints, disputes, reversals, chargebacks, fees, fines, penalties and other liability to PAY1, a third party or You</li>
	<li>use the Services in a manner that PAY1 or any payment card network reasonably believe to be an abuse of the payment card system or a violation of payment card network rules</li>
	<li>take any action that may cause PAY1 to lose any of the Services from its service providers, Telcos, payment processors or other suppliers</li>
	<li>send automated request of any kind to the Site's system without express permission in advance from PAY1.</li>
</ul>

<h2>Privacy Policy &amp; Information Usage </h2>

<p>We respect your privacy at PAY1 and value your trust. Kindly read our Privacy Policy carefully for information related to our collection, use, and disclosure of your personal information. The Privacy Policy is incorporated by reference in this Agreement. Use of the PAY1 website, WAP site and applications and/ or its services constitutes acceptance of the Privacy Policy. For more information check our detailed Privacy Policy.</p>

<h2>Account Passwords and Registration</h2>

<p>You agree that the information You provide to PAY1 on registration and at all other times, including payment, will be true, accurate, current, and complete. You also agree that You will ensure that this information is kept accurate and up-to-date at all times.If You have reason to believe that Your Account is no longer secure (e.g., in the event of a loss, theft or unauthorized disclosure or use of Your account ID, PIN, Password, or any credit, debit or prepaid cash card number or netbanking login/password, if applicable), then You agree to immediately notify PAY1 and identify PAY1 from any liabilities that may arise from the misuse of Your Account.</p>

<h2>Usernames and Password</h2>

<p>Your mobile number is a username that will identify your account and thereby all of your transactions and account related activity on PAY1. Each user can choose a valid mobile number which can be edited later only by sending a request by an e-mail to us.</p>

<p>We encourage you to choose a strong password to protect your account. We recommend the use of upper and lower-case letters, inclusion of one or more numerical digits and/or inclusion of special characters, e.g. @, #, $ etc. It is advised that you not share your password with anyone or write it down. Make sure you sign out before leaving a computer unattended. PAY1 never, under any circumstances, asks for your account password.</p>

<h2>Billing and Payment</h2>

<p>PAY1 may in future provide a detailed billing summary in the format of its choice, which may change from time to time. This summary may be found by logging into your account (if you have registered and created an account) on the Mobile App &amp; WEB App. All charges will be automatically placed to your credit card, debit card, prepaid PAY1 Top-up or netbanking account for the amount of Recharge successfully purchased at the time of purchase. No additional notice or consent will be required for charging Service use to your credit card, debit card, prepaid PAY1 Top-up or netbanking account. The merchant name "Mindsarray Technologies Pvt. Ltd." will appear on your credit card statement for purchase of Recharge or any additional paid Services you may use from PAY1.</p>

<h2>Third Party Sites, Products and Services; Links</h2>

<p>The Services and/or the Site may include links or references to other web sites or services solely as a convenience to users ("Reference Sites"). PAY1 does not endorse any such Reference Sites or the information, materials, products, or services contained on or accessible through Reference Sites. In addition, Your correspondence or business dealings with, or participation in promotions of, advertisers found on or through the Services and/or the Site are solely between You and such entity. Access and use of reference sites, including the information, materials, products, and services on or available through reference sites is solely at your own risk.</p>

<h2>All Sales are Final; Refund Policy</h2>

<p>All sales of Recharge are final with no refund or exchange permitted. You are responsible for the mobile number or DTH account you purchase Recharge for and all charges that result from those purchases. PAY1 is not responsible for any purchase of Recharge or Bill payment for an incorrect mobile number or DTH account number. However, if in a transaction performed by you on the Site/Mobile Application, money has been charged to your card or bank account and a Recharge is not delivered within 24 hours of your completion of the transaction then you may inform us by sending an email to our customer services email address mentioned on the Contact Us page. Please include in the email the following details - the mobile number (or DTH account number), operator name, Recharge value, Transaction date and Order Number. PAY1 shall investigate the incident and if it is found that money was indeed charged to your card or bank account without delivery of the Recharge then you will be refunded the money within 72 working Hours in the PAY1 Wallet from the date of receipt of your email.</p>

<h2>Termination; Agreement Violations</h2>

<p>You agree that PAY1, in its sole discretion, for any or no reason, and without penalty, may suspend or terminate Your account (or any part thereof) or Your use of the Services and remove and discard all or any part of Your account, Your user profile, or Your recipient profile, at any time. PAY1 may also in its sole discretion and at any time discontinue providing access to the Services, or any part thereof, with or without notice. You agree that any termination of Your access to the Services or any account You may have or portion thereof may be effected without prior notice, and You agree that PAY1 will not be liable to You or any third party for any such termination. Any suspected fraudulent, abusive or illegal activity may be referred to appropriate law enforcement authorities. These remedies are in addition to any other remedies PAY1 may have at law or in equity. Upon termination for any reason, You agree to immediately stop using the Services.</p>

<h2>Limitation of Liability and Damages</h2>

<p>In no event will PAY1 or its contractors, agents, licensors, partners, or suppliers be liable to you for any special, indirect, incidental, consequential, punitive, reliance, or exemplary damages (including without limitation lost business opportunities, lost revenues, or loss of anticipated profits or any other pecuniary or non-pecuniary loss or damage of any nature whatsoever) arising out of or relating to (i) this agreement, (ii) the services, the site or any reference site, or (iii) your use or inability to use the services, the site (including any and all materials) or any reference sites, even if PAY1 or a PAY1 authorized representative has been advised of the possibility of such damages. In no event will PAY1 or any of its contractors, directors, employees, agents, third party partners, licensors or suppliers' total liability to You for all damages, liabilities, losses, and causes of action arising out of or relating to (i) this Agreement, (ii) the Services, (iii) Your use or inability to use the Services or the Site (including any and all Materials) or any Reference Sites, or (iv) any other interactions with PAY1, however caused and whether arising in contract, tort including negligence, warranty or otherwise, exceed the amount paid by You, if any, for using the portion of the Services or the Site giving rise to the cause of action or One Thousand Rupees (Rs.1000), whichever is less. You acknowledge and agree that PAY1 has offered its products and services, set its prices, and entered into this agreement in reliance upon the warranty disclaimers and the limitations of liability set forth herein, that the warranty disclaimers and the limitations of liability set forth herein reflect a reasonable and fair allocation of risk between you and PAY1, and that the warranty disclaimers and the limitations of liability set forth herein form an essential basis of the bargain between you and PAY1. PAY1 would not be able to provide the services to you on an economically reasonable basis without these limitations. Applicable law may not allow the limitation or exclusion of liability or incidental or consequential damages, so the above limitations or exclusions may not apply to You. In such cases, PAY1's liability will be limited to the fullest extent permitted by applicable law. This paragraph shall survive termination of this Agreement.</p>

<h2>Indemnification</h2>

<p>You agree to indemnify, save, and hold PAY1, its affiliates, contractors, employees, officers, directors, agents and its third party suppliers, licensors, and partners harmless from any and all claims, losses, damages, and liabilities, costs and expenses, including without limitation legal fees and expenses, arising out of or related to Your use or misuse of the Services or of the Site, any violation by You of this Agreement, or any breach of the representations, warranties, and covenants made by You herein. PAY1 reserves the right, at Your expense, to assume the exclusive defense and control of any matter for which You are required to indemnify PAY1, including rights to settle, and You agree to cooperate with PAY1's defense and settlement of these claims. PAY1 will use reasonable efforts to notify You of any claim, action, or proceeding brought by a third party that is subject to the foregoing indemnification upon becoming aware of it. This paragraph shall survive termination of this Agreement.</p>

<h2>Disclaimer; No Warranties</h2>

<p>To the fullest extent permissible pursuant to applicable law, PAY1 and its third-party partners, licensors, and suppliers disclaim all warranties, statutory, express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, and non-infringement of proprietary rights. No advice or information, whether oral or written, obtained by you from PAY1 or through the services or the site will create any warranty not expressly stated herein. You expressly acknowledge that as used in this section, the term "PAY1" includes PAY1's officers, directors, employees, shareholders, agents, licensors, subcontractors and affiliated companies. You acknowledge that PAY1 is a reseller and is not liable for any 3rd party (telcos &amp; suppliers) obligations due to rates, quality, and all other instances, whether to any such telco's subscribers or otherwise. You expressly agree that use of the services and the site is at your sole risk. It is your responsibility to evaluate the accuracy, completeness and usefulness of all opinions, advice, services, merchandise and other information provided through the site or on the internet generally. We do not warrant that the service will be uninterrupted or error-free or that defects in the site will be corrected. The services and the site and any data, information, third party software, reference sites, services, or software made available in conjunction with or through the services and the site are provided on an "as is" and "as available," "with all faults" basis and without warranties or representations of any kind either express or implied. PAY1, and its third party suppliers, licensors, and partners do not warrant that the data, PAY1 software, functions, or any other information offered on or through the services, the site or any reference sites will be uninterrupted, or free of errors, viruses or other harmful components and do not warrant that any of the foregoing will be corrected.PAY1 and its third party suppliers, licensors, and partners do not warrant or make any representations regarding the use or the results of the use of the services, the site or any reference sites in terms of correctness, accuracy, reliability, or otherwise.you understand and agree that you use, access, download, or otherwise obtain information, materials, or data through the services, the site or any reference sites at your own discretion and risk and that you will be solely responsible for any damage to your property (including your computer system and device) or loss of data that results from the download or use of such material or data. We do not authorize anyone to make any warranty on our behalf and you should not rely on any such statement. This paragraph shall survive termination of this agreement. In no event will PAY1 be liable for any incidental, consequential, or indirect damages (including, but not limited to, damages for loss of profits, business interruption, loss of programs		 or information, and the like) arising out of the use of or inability to use the site.</p>


</div>