<script>
    var deviceUuid = "";
    $(function() {
    
        $('#utilityBillPayment').on('submit', function (event) {
            event.preventDefault();
            utiBillPayment(1);
        });
        
        function mobileValidate(y){
            var y = y.trim();
            if(y == ""){alert("Enter mobile number");return 0;
            }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
            }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
            }
            return 1;
        }

        function amtValidate(y) {
            var err = '';
            var y = y.trim();
            if(y == ""){alert("Plese enter proper amount");return 0;
            }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
            }
            return 1;
        }

        function utiBillPayment(cnfFlag){
           
            var mobileNo = $("#mobileNo").val().trim();
            var rechargeAmt = $("#rechargeAmt").val().trim();
            var oprID = parseInt($("#operatorId").val()) - 44;
            if(oprID!=5){
            var accno = $("#acc-no").val().trim();
           
             }
          if(oprID==1){
             var cycle = $("#cycle").val().trim();
             }

           if(oprID==5 || oprID==6){
            var phoneno = $("#phone-no").val().trim();
           }
           
            if(mobileValidate(mobileNo) == '0') return;	
            if(amtValidate(rechargeAmt) == '0')return;
            var service_charge = rechargeAmt * 0.005;
            service_charge = (service_charge < 5) ? 5 : service_charge;
            if(cnfFlag == 1){
                var cnf = "Operator: "+$('#operatorName').val()
                +"\nNumber: "+mobileNo
                +"\nAmount: Rs."
                +rechargeAmt+" + " + Math.round(service_charge * 1.145 * 100) / 100 + "(0.5% + service charge)\n";
                var r=confirm(cnf+"Press OK to confirm");
                if(r==false){
                    return false;
                }
            }
           
            $('#utiBillPaymentSubmit').button('loading');
              if(oprID==1){
        var data  = { method:"utilityBillPayment",
                    device_type:"web",
                    mobileNumber:mobileNo,
                    accountNumber :accno,
                    operator:oprID,
                    param:cycle,
                    amount:rechargeAmt,
                   type:"flexi"
                   }
            } 
          else if(oprID==6) {
             var data  = { method:"utilityBillPayment",
                            device_type:"web",
                            mobileNumber:mobileNo,
                            accountNumber :phoneno,
                            operator:oprID,
                            param:accno ,
                            amount:rechargeAmt,
                            type:"flexi"
                           }
                  }
           else if(oprID==5) {
             var data  = { method:"utilityBillPayment",
                             device_type:"web",
                            mobileNumber:mobileNo,
                            accountNumber :phoneno,
                                   //param:accno,
                            operator:oprID,
                            amount:rechargeAmt,
                            type:"flexi"
                                  }
                         }
              else {
                   var data =  {     method:"utilityBillPayment",
                                     device_type:"web",
                                      mobileNumber:mobileNo,
                                      accountNumber :accno,
                                      operator:oprID,
                                      amount:rechargeAmt,
                                     type:"flexi"
                             }
                          }
                        
                        $.ajax({
                            url: PayOne.config.urls.api,
                            type: "POST",
                            data:data,       
                            dataType: 'jsonp',
                            jsonp: 'root',
                            timeout: 50000,
                            success: function(data, status){ 
                                $('#mobBillPaymentSubmit').button('reset');
                                $.each(data, function(i,item){				
                                    if(item.status == 'failure'){                       
                                        PayOne.core.failChk(item.code,item.description,true);

                                    }else{
                                        if(item.status == 'success'){
                                var desc = 'Payment request Submitted successfully';
                                $('#success_description').html(desc);
                                $('#success').modal("show");
                                $("#mobileNo").val("");
                                $("#rechargeAmt").val("");
                                
                                $('#retailerBal').html(item.balance);
                                PayOne.core.cookie.set("balance", item.balance, 1);
                                setTimeout(function(){
                                   window.location="/utility_billpayment";  
                                }, 3000);
                            }
                        }
                    });
                },
                error: function(error){
                        $('#utiBillPaymentSubmit').button('reset');
                    //$('#rec_now_loader').html(tmp);
                }
            });
        }
    
    });
</script>

<div class="breadcrumb"><a href="/<?php echo $type; ?>"><?php echo ucfirst($type); ?></a> &gt; <?php echo $operatorName; ?> &gt; Recharge</div>
<form role="form" id="utilityBillPayment">
    <div class="form-group">
        <input type="hidden" name="operatorId" id="operatorId" value="<?php echo $operatorId; ?>" />
        <input type="hidden" name="operatorName" id="operatorName" value="<?php echo $operatorName; ?>" />
        <input type="hidden" name="operatorType" id="operatorType" value="<?php echo $type; ?>" />
       
    </div>
    <div class="form-group">
        <label for="mobileNo"  for="mobileNo">Enter Mobile Number</label>
        <input type="text" class="form-control" id="mobileNo" placeholder="" pattern="^\d{10,10}" title="10 Digit Mobile No" />
        <span style="font-size:12px;">eg. 7101000001</span>
    </div>

<?php if($operatorId!=49){?>
<div class="form-group">
        <label for="mobileNo"  for="mobileNo">Enter Account Number</label>
        <input type="text" class="form-control" id="acc-no" placeholder=""   title="Enter Account Number" />
        <span style="font-size:12px;">eg. 102252117</span>
    </div>
<?php } ?>
 <?php if($operatorId==45){?>
<div class="form-group">
        <label for="mobileNo"  for="mobileNo">Enter Cycle</label>
        <input type="text" class="form-control" id="cycle" placeholder=""  />
        <span style="font-size:12px;">eg. 05</span>
    </div>
<?php } else if($operatorId==50 || $operatorId==49){ ?>
<div class="form-group">
        <label for="mobileNo"  for="mobileNo">Enter Phone Number</label>
        <input type="text" class="form-control" id="phone-no" placeholder=""  />
        <span style="font-size:12px;">eg. 02242932233</span>
    </div>
<?php } ?>
    <div class="form-group">
        <label for="rechargeAmt">Enter Amount</label>
        <input type="text" class="form-control" id="rechargeAmt" placeholder="" pattern="^\d{1,5}(\.\d{1,2})?$" title="Amount should be a no"/>
    </div>    

                    

    
    <button id="utiBillPaymentSubmit" data-loading-text="Processing..." type="submit" class="form-control default-btn">Proceed</button>
</form>

