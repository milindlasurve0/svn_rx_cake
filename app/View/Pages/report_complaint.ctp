<script>

$(function() {



    
    var htmlStr = "";
    $.each(PayOne.config.operators.mobRecharge , function(i ,v){
       htmlStr = htmlStr + "<a class=\"mobileList\" href=\"~/recharges/mobile/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p>"+v.name+"</p></a>"
    });
    $("#mobile").html(htmlStr);
    
    
    transStatusTable1  = $('#transStatusTable1').DataTable({"aaSorting": []});
    transStatusTable2  = $('#transStatusTable2').DataTable({"aaSorting": []});
    transStatusTable3  = $('#transStatusTable3').DataTable({"aaSorting": []});
    transStatusTable4  = $('#transStatusTable4').DataTable({"aaSorting": []});
    transStatusTable5  = $('#transStatusTable5').DataTable({"aaSorting": []});
    transStatusTable6  = $('#transStatusTable6').DataTable({"aaSorting": []});
    transStatusTable7  = $('#transStatusTable7').DataTable({"aaSorting": []});
    
    
    
    lastTenTransTable1 = $('#lastTenTransTable1').DataTable({"aaSorting": []});
    lastTenTransTable2 = $('#lastTenTransTable2').DataTable({"aaSorting": []});
    lastTenTransTable3 = $('#lastTenTransTable3').DataTable({"aaSorting": []});
    lastTenTransTable4 = $('#lastTenTransTable4').DataTable({"aaSorting": []});
    lastTenTransTable5 = $('#lastTenTransTable5').DataTable({"aaSorting": []});
    lastTenTransTable6 = $('#lastTenTransTable6').DataTable({"aaSorting": []});
    lastTenTransTable7 = $('#lastTenTransTable7').DataTable({"aaSorting": []});
    
    $('#transDate1').datepicker({
		"format": "dd-mm-yyyy"        
	});
//    $('#transDate2').datepicker({
//		"format": "dd-mm-yyyy"
//        
//	});
    

    $('#searchReversal').click(function (e) {
        
                    var date_from = $('#transDate1').val();
                    //var date_to   = $('#transDate2').val();
                    if(date_from == '' ){
                            alert("Please select proper date.");
                    } else {
                            //$('#date_err').hide();
                            var date_from_arr = date_from.split("-");
                            var dt = date_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                            var serviceId = $("#reversalStatusID").val();

                            
                            //PayOne.reports.renderStatus("mob",1,dt,true,"#transStatusTableBody",transStatusTable);
                            PayOne.reports.renderTransStatus("mob",serviceId,dt,callLogin,"#transStatusTableBody"+serviceId,eval("transStatusTable"+serviceId));                            
                    }
        
    });
    $('#searchReversal').trigger('click');
    
    
    $('#reversalStatusTab a').on("click",function (e) {        
        var hash  = $(this).attr('href');   
        var service_id = hash.substring(18, 19);
        $("#reversalStatusID").val(service_id);

        $('#searchReversal').trigger('click');
    });
    
    $('#reversalRequestTab a').on("click",function (e) {       
        var hash  = $(this).attr('href');//  alert(hash); 
        var service_id = hash.substring(19, 20);
        $("#reversalRequestID").val(service_id);

        $('#searchReversal').trigger('click');
    });
    
    $('#searchByNo').on("click",function (e) {         
        var no = $("#searchNo").val();  
        if(no == ""){
            alert("Please enter a valid mobile no.");return;
        }
        var serviceId = $("#reversalRequestID").val();
        PayOne.reports.lastTranSearchByNo(serviceId,no,"#lastTenTransTableBody"+serviceId,eval("lastTenTransTable"+serviceId));// 1 for mobile                    
    });
        
    var date = new Date();
    var dt =  date.getFullYear() +"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+(date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate());  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
    var callLogin = false;
    var serviceId = $("#reversalStatusID").val();
    PayOne.reports.renderTransStatus("mob",serviceId,dt,callLogin,"#transStatusTableBody"+serviceId,eval("transStatusTable"+serviceId));
    
});
</script>
            			              
	<div class="breadcrumb">Reversal</div>
    <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#reversalStatus" data-toggle="tab">Complaint Status</a></li>
        <li onclick="$('#searchByNo').trigger('click');" class=""><a href="#reversalRequest" data-toggle="tab">Request Complaint</a></li>
    </ul>
	<div id="myTabContent" class="tab-content ">
		<div class="tab-pane fade active in" id="reversalStatus">
            <input id="reversalStatusID" value="1" type="hidden"/>
        	<div class="form-inline StatusSearch">
                <input type="text" class="form-control" id="transDate1" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                <span id="searchReversal" class="glyphicon glyphicon-search"></span>-->
                <!--<button id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button>-->
	 <input id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>	
            </div>
			<ul id="reversalStatusTab" class="nav nav-tabs">
                <li class="active"><a href="#reversalStatusTab1" data-toggle="tab">Mobile</a></li>
                <li class=""><a href="#reversalStatusTab2" data-toggle="tab">DTH</a></li>
                <li class=""><a href="#reversalStatusTab3" data-toggle="tab">Entertainment</a></li>
                <li class=""><a href="#reversalStatusTab4" data-toggle="tab">Mobile Bill</a></li>
                 <li class=""><a href="#reversalStatusTab5" data-toggle="tab">Wallet Topup</a></li>
                <li class=""><a href="#reversalStatusTab6" data-toggle="tab">Utility Bill</a></li>
                <li class=""><a href="#reversalStatusTab7" data-toggle="tab">Online Payment</a></li>
            </ul>
            <div id="reversalStatusTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="reversalStatusTab1">                    
<!--                    <table class="table table-responsive table-striped table-hover">
                      <thead>
                        <tr>
                          <th>Phone No</th>
                          <th>Time</th>
                          <th>Amount</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>9811111111</td>
                          <td>7.00 pm</td>
                          <td>50</td>
                          <td><img src="images/delivered.png"></td>
                        </tr>
                        <tr>
                          <td>9811111111</td>
                          <td>8.00 pm</td>
                          <td>100</td>
                          <td><img src="images/delivered-resend.png"></td>
                        </tr>
                      </tbody>
                    </table>-->
                    <div class="table-responsive">                            	
                        <table class="table table-striped table-hover order-column" id="transStatusTable1" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                            </tr>
                          </thead>
                          <tbody id="transStatusTableBody1">

                          </tbody>
                        </table>
                   </div>
                </div>
                <div class="tab-pane fade" id="reversalStatusTab2">
                    <div class="table-responsive">                            	
                        <table class="table table-striped table-hover order-column" id="transStatusTable2" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Subscriber Id</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                            </tr>
                          </thead>
                          <tbody id="transStatusTableBody2">

                          </tbody>
                        </table>
                   </div>
                </div>
                 <div class="tab-pane fade" id="reversalStatusTab3">
                    <div class="table-responsive">                            	
                        <table class="table table-striped table-hover order-column" id="transStatusTable3" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                            </tr>
                          </thead>
                          <tbody id="transStatusTableBody3">

                          </tbody>
                        </table>
                   </div>
                </div>
                 <div class="tab-pane fade" id="reversalStatusTab4">
                    <div class="table-responsive">                            	
                        <table class="table table-striped table-hover order-column" id="transStatusTable4" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                            </tr>
                          </thead>
                          <tbody id="transStatusTableBody4">

                          </tbody>
                        </table>
                   </div>
                </div>
                    <div class="tab-pane fade" id="reversalStatusTab5">
                    <div class="table-responsive">                            	
                        <table class="table table-striped table-hover order-column" id="transStatusTable5" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                            </tr>
                          </thead>
                          <tbody id="transStatusTableBody5">

                          </tbody>
                        </table>
                   </div>
                </div>
                         <div class="tab-pane fade" id="reversalStatusTab6">
                    <div class="table-responsive">                            	
                        <table class="table table-striped table-hover order-column" id="transStatusTable6" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                            </tr>
                          </thead>
                          <tbody id="transStatusTableBody6">

                          </tbody>
                        </table>
                   </div>
                </div>
                <div class="tab-pane fade" id="reversalStatusTab7">
                    <div class="table-responsive">                            	
                        <table class="table table-striped table-hover order-column" id="transStatusTable7" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                            </tr>
                          </thead>
                          <tbody id="transStatusTableBody7">

                          </tbody>
                        </table>
                   </div>
                </div>
<!--                 <div class="tab-pane fade" id="reversalStatusTab6">
                    Tab5
                </div>-->
            </div>            
		</div>
        
		<div class="tab-pane fade" id="reversalRequest">
            <input id="reversalRequestID" value="1" type="hidden"/>
        	<div class="form-inline RequestSearch">
                <input type="text" class="form-control" placeholder="Enter 10 digit No">
                 <input type="submit" class="default-btn" id="searchByNo" value="search"/>   
            </div>
			<ul id="reversalRequestTab" class="nav nav-tabs">
                <li class="active"><a href="#reversalRequestTab1" data-toggle="tab">Mobile</a></li>
                <li class=""><a href="#reversalRequestTab2" data-toggle="tab">DTH</a></li>
                <li class=""><a href="#reversalRequestTab3" data-toggle="tab">Entertainment</a></li>
                <li class=""><a href="#reversalRequestTab4" data-toggle="tab">Mobile Bill</a></li>
                <li class=""><a href="#reversalRequestTab5" data-toggle="tab">Wallet Topup</a></li>
               <li class=""><a href="#reversalRequestTab6" data-toggle="tab">Utility Bill</a></li>
				<li class=""><a href="#reversalRequestTab7" data-toggle="tab">Online Payment</a></li>
            </ul>
            <div id="reversalRequestTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="reversalRequestTab1">
                                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable1" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody1">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>
<!--                <div class="tab-pane fade" id="reversalRequestTab1">
                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable1" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody1">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>-->
                 <div class="tab-pane fade" id="reversalRequestTab2">
                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable2" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Subscriber Id</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody2">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>
                 <div class="tab-pane fade" id="reversalRequestTab3">
                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable3" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody3">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>
                 <div class="tab-pane fade" id="reversalRequestTab4">
                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable4" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody4">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>

<div class="tab-pane fade" id="reversalRequestTab5">
                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable5" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody5">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>
 <div class="tab-pane fade" id="reversalRequestTab6">
                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable6" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody6">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>
                <div class="tab-pane fade" id="reversalRequestTab7">
                    <div class="table-responsive">                            
                                        <table class="table table-striped table-hover order-column" id="lastTenTransTable7" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Mobile No</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Query</th> 
                                            </tr>
                                          </thead>
                                          <tbody id="lastTenTransTableBody7">                                        

                                          </tbody>
                                        </table>
                                    </div>
                </div>
            </div>
		</div>
	</div>
