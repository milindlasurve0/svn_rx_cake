
<script>
$(function() {
    
    $('#onlinePayment').on('submit', function (event) {
        event.preventDefault();
        onlinePayment(1);
    });
    
    function mobileValidate(y){
        var y = y.trim();
        if(y == ""){alert("Enter mobile number");return 0;
        }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
        }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
        }
        return 1;
    }

    function amtValidate(y) {
        var err = ''; 
        var y = y.trim();
        if(y == ""){alert("Plese enter proper amount");return 0;
        }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
        }
        return 1;
    }

    var payment_table = $('#cashPaymentTable').DataTable({"aaSorting": []});
    var payment_table_body_id = '#cashPaymentTableBody';
    
    function onlinePayment(){
       
        
        var mobile = $("#mobile").val().trim();           
        if(mobileValidate(mobile) == '0')return;

        $('#onlinePaymentSubmit').button('loading');

             var data = { method: "cashpgTxnList",
            		 	  mobileNumber: mobile,
            		 	  action: "get_txn_list"
                         }
                    
                    
                    $.ajax({
                        url: PayOne.config.urls.api,
                        type: "POST",
                        data:data,       
                        dataType: 'jsonp',
                        jsonp: 'root',
                        timeout: 50000,
                        success: function(data, status){ 
                        	PayOne.core.utils.deleteAllRowDataTable(payment_table_body_id, payment_table);
                            $('#onlinePaymentSubmit').button('reset');
                            $.each(data, function(i,item){				
                                if(item.status == 'failure'){                       
                                    PayOne.core.failChk(item.code,item.description,true);

                                }else{
                                    if(item.status == 'success'){
                                        $('#onlinePayment').hide();
                                        $('#paymentTable').show();
                                        $('#user_mobile').html("Mobile no. " + mobile);
                                    	console.log(item.description);
                                    	 $.each(item.description, function(j, item1) {

                                    		 payment_table.row.add([
															item.description[j]['amount'],
															item.description[j]['id'],
															item.description[j]['company_name'] + " <img style='height: 26px;' src='"+item.description[j]['img_url'] + "' />",
															item.description[j]['expiry_time'],
                                                            "<button id='pg_collect_"+ item.description[j]['id'] +"' type='submit' onclick='payment_info(" + item.description[j]['id'] + ");' style='color: white;background-color: #22c064;' class='btn'>Proceed</button>"
                                                         ]).draw();

                                          	var input_data = "<input type='hidden' id='pg_data_" + item.description[j]['id'] + "' "
                            	 			+ 	"data-mobile='"+ item.description[j]['mobile'] +"' "
                            	 			+ 	"data-amount='"+ item.description[j]['amount'] +"' "
                            	 			+ 	"data-refid='"+ item.description[j]['id'] +"' "
                            	 			+ 	"data-company='"+ item.description[j]['company_name'] +"' "
                            	 			+ 	"data-operator='"+ item.description[j]['operator'] +"' "
                            	 			+ 	"data-expiry='"+ item.description[j]['expiry_time'] +"' "
                            	 			+ 	"data-image='"+ item.description[j]['img_url'] +"' >";

                                          	$('#data-div').after(input_data);
                                    	 });       
                        }
                    }
                });
            },
            error: function(error){
                $('#utiBillPaymentSubmit').button('reset');
                //$('#rec_now_loader').html(tmp);
            }
        });
    }

});

function payment_info(ref_id){
	var element = $('#pg_data_' + ref_id);
	$('#pg_mobile').html(element.data('mobile'));
	$('#pg_ref_id').html(element.data('refid'));
	$('#pg_image').html('<img src="'+ element.data('image') +'" style="height: 26px;">');
	$('#pg_partner').html(element.data('company'));
	$('#pg_amount').html("Rs. " + element.data('amount'));
	$('#pg_expiry').html("The REF ID will expire on " + element.data('expiry'));

	$('#pg_collect').unbind( "click" );
	$('#pg_collect').click(function(event){
		
			if(confirm("Are you sure you want to continue?")){
				var data = { 
	       		  method: "cashpgPayment", 
	  		 	  mobileNumber: element.data('mobile'),
	  		 	  amount: element.data('amount'),
	  		 	  ref_id: element.data('refid'), 
	  		 	  operator: element.data('operator')
	             }
				$('#pg_collect').button('loading');    
			 $.ajax({
		         url: PayOne.config.urls.api,
		         type: "POST",
		         data:data,       
		         dataType: 'jsonp',
		         jsonp: 'root',
		         timeout: 50000,
		         success: function(data, status){ 
		        	 $('#collect_cash').modal("hide");
		        	 $('#pg_collect').button('reset');
					console.log(data);
					console.log(status);
					if(data[0].status == "success"){
						$('#pg_collect_' + ref_id).html("Collected");
						$('#pg_collect_' + ref_id).attr('onclick','').unbind('click');
						$('#collected').modal("show");

						PayOne.core.cookie.set("balance", data[0].balance, 1); 
htmlStr  =    "<a class=\"pull-left\" href=\"#\"><img class=\"media-object user-img\" src=\"/img/ic_loggedin.png\" alt=\"...\"></a>"+
                                        "<div class=\"media-body\">"+
                                        "<p class=\"userName top\" style=\"color:black;\">"+PayOne.core.cookie.get("shopname")+"</p>"+
                                        "<p class=\"balance\" >"+data[0].balance+"</p>"+
                                       
                                        "</div>";
                         $("#media_header_section").html(htmlStr);
					}
					else {
						 PayOne.core.failChk(data[0].code,data[0].description,true);
					}	
		         }
				}); 
			}	  
	});		
			 
	$('#collect_cash').modal("show"); 
	
}
</script> 

<div class="breadcrumb">C2D <?php echo ucfirst($type); ?> </div>
<form role="form" id="onlinePayment">
    
    <div class="form-group">
        <label for="mobile">Enter Mobile Number</label>
        <input type="text" class="form-control" id="mobile" placeholder="Enter Mobile Number"  title="Mobile should be a no"/>
    </div>    
    
    <button id="onlinePaymentSubmit" data-loading-text="Processing..." type="submit" style='color: white;background-color: #252525;width:295px;' class=" btn">PROCEED</button>
</form>

<div id="paymentTable" style="display:none;">
	<h1 id="user_mobile" style="margin-bottom: 20px"></h1>
	<div class="table-responsive">
	<table class="table table-striped table-hover order-column" id="cashPaymentTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="background: none repeat scroll 0 0 #e73c3c">AMOUNT</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">REF ID</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">ONLINE PAYMENT PARTNER</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">EXPIRY</th>
                        <th style="background: none repeat scroll 0 0 #e73c3c">STATUS</th>
                    </tr>
                </thead>
                <tbody id="cashPaymentTableBody">                                        
					<tr class="odd" role="row">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
                </tbody>
            </table>
     </div>     
     <div id="data-div">
     
     </div>  
</div>

