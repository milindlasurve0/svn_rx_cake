<style type="text/css">

.privacy p {
margin: 0px;
text-align:justify;
padding:0 0 10px 0;
font-size:12px;
font-family:"Encode Sans Normal";
}

.privacy h2{
font-size:14px;
font-family:"Encode Sans Narrow Medium";
margin:0;
padding:10px 0 5px 0;
color:#513D98;
}

.privacy ul{
margin:0 0 10px 0;
padding-left:20px;
}

.privacy li{
font-size:12px;
text-align:justify;
margin:0 0 2px 0;
font-family:"Encode Sans Normal";
}
</style>

<div class="privacy" style="overflow-y: scroll; height:700px;">
<h2>Privacy Policy</h2>
<p>PAY1 recognizes the expectations of its customers with regard to privacy, confidentiality and security of their personal information that resides with PAY1. .</p>
<p>This Privacy Policy provides you with details about the manner in which your data is collected, stored &amp; used by us. You are advised to read this Privacy Policy carefully. By visiting PAY1 website/WAP site/applications you expressly give us consent to use &amp; disclose your personal information in accordance with this Privacy Policy. If you do not agree to the terms of the policy, please do not use or access PAY1 website, WAP site or mobile applications. </p>
<p>Note: Our privacy policy may change at any time without prior notification. To make sure that you are aware of any changes, kindly review the policy periodically. This Privacy Policy shall apply uniformly to PAY1 desktop website, PAY1 mobile WAP site &amp; PAY1 mobile applications </p>

<h2>General </h2>

<p>We will not sell, share or rent your personal information to any 3rd party or use your email address/mobile number for unsolicited emails and/or SMS. Any emails and/or SMS sent by PAY1 will only be in connection with the provision of agreed services &amp; products and this Privacy Policy.</p>
<p>Periodically, we may reveal general statistical information about PAY1 &amp; its users, such as number of visitors, number and type of goods and services purchased, etc.</p>
<p>We reserve the right to communicate your personal information to any third party that makes a legally-compliant request for its disclosure. </p>

<h2>Personal Information </h2>

<p>Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, email ID, credit card number, cardholder name, card expiration date, information about your mobile phone, DTH service, data card, electricity connection, Smart Tags and any details that may have been voluntarily provide by the user in connection with availing any of the services on PAY1 </p>
<p>When you browse through PAY1, we may collect information regarding the domain and host from which you access the internet, the Internet Protocol [IP] address of the computer or Internet service provider [ISP] you are using, and anonymous site statistical data.</p>

<h2>Use of Personal Information  </h2>

<p>We use personal information to provide you with services &amp; products you explicitly requested for, to resolve disputes, troubleshoot concerns, help promote safe services, collect money, measure consumer interest in our services, inform you about offers, products, services, updates, customize your experience, detect &amp; protect us against error, fraud and other criminal activity, enforce our terms and conditions, etc. 
We also use your contact information to send you offers based on your previous orders and interests. 
We may occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and demographic information (like zip code, age, gender, etc.). We use this data to customize your experience at PAY1, providing you with content that we think you might be interested in and to display content according to your preferences. 
</p>

<h2>Cookies</h2>

<p>A "cookie" is a small piece of information stored by a web server on a web browser so it can be later read back from that browser. PAY1 uses cookie and tracking technology depending on the features offered. No personal information will be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties. </p>

<h2>Links to Other Sites</h2>

<p>Our site links to other websites that may collect personally identifiable information about you. PAY1 is not responsible for the privacy practices or the content of those linked websites. </p>

<h2>Security</h2>

<p>PAY1 has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</p>

<h2>Consent</h2>

<p>By using PAY1 and/or by providing your information, you consent to the collection and use of the information you disclose on PAY1 in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>

<h2>Communication</h2>

<p>For any kind of communication, please write to <a href="mailto:listen@pay1.in">listen@pay1.in</a></p>

<h2>PAY1 is headquartered at:</h2>

<p>MindsArray Technologies Private Limited<br/>
726, Raheja's Metroplex (IJIMIMA), Link Road, Malad West, Mumbai - 400064</p>
</div>