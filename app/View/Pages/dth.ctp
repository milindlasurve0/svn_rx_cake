
<script>
$(function() {
    
        var htmlStr = "";
        $.each(PayOne.config.operators.dthRecharge , function(i ,v){
            htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/dth/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p>"+v.name+"</p></a>"
        });
        $("#mobile").html(htmlStr);
        
        transStatusTable  = $('#transStatusTable').DataTable({"aaSorting": []});
        lastTenTransTable = $('#lastTenTransTable').DataTable({"aaSorting": []});
        
        $('#transDate1').datepicker({
            "format": "dd-mm-yyyy"        
        });
        $('#transDate1').datepicker().on('changeDate', function (ev) {
            PayOne.reports.renderTransStatus('dth',2,$(this).val(),true,'#transStatusTableBody',transStatusTable);
            $(this).datepicker('hide');
        });
        /*$('#transDate2').datepicker({
            "format": "dd-mm-yyyy"        
        });*/
        
        
        $('#searchReversal').click(function (e) {        
            var date_from = $('#transDate1').val();
            //var date_to   = $('#transDate2').val();
            if(date_from == '' ){
                alert("Please select proper date.");
            } else {
                var date_from_arr = date_from.split("-");
                var dt = date_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                PayOne.reports.renderTransStatus("dth",2,dt,true,"#transStatusTableBody",transStatusTable);                            
            }
        });  
        
        
        $('#myTab a').on("click",function (e) {        
            var hash  = $(this).attr('href');        
            if(hash=="#details"){
                PayOne.reports.lastTran(2,"#lastTenTransTableBody",lastTenTransTable);// 2 for dth
            } else if(hash=="#status"){
                //var date = new Date();
                //var date = $('#transDate1').val();
                //var dt =  date.getFullYear() +"-"+( date.getMonth() <= 9 ?  ("0"+(date.getMonth()+1)) : (date.getMonth()+1) ) +"-"+(date.getDate() <= 9 ?  ("0"+date.getDate()) : date.getDate());  // Returns the yeardate_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                //var callLogin = false;
                var dt = $('#transDate1').val().split("-").reverse().join("-");
                var callLogin = true;
                PayOne.reports.renderTransStatus("dth",2,dt,callLogin,"#transStatusTableBody",transStatusTable);
            }
        });
        $('#searchByNo').on("click",function (e) {  
            
            var no = $("#searchNo").val().trim();
            if(no == ""){
                alert("Please enter a valid dth no.");return;
            }
            PayOne.reports.lastTranSearchByNo(2,no,"#lastTenTransTableBody",lastTenTransTable);// 2 for dth                    
        });
    });
</script>



<!--<div id="content" class="container main snap-content">              -->
            			<div class="breadcrumb">Dth</div>                        
                        <ul id="myTab" class="nav nav-tabs">
                          <li class="active"><a href="#mobile" data-toggle="tab">Dth</a></li>
                          <li class=""><a href="#status" data-toggle="tab">Complaint Status</a></li>
                          <li class=""><a href="#details" data-toggle="tab">Request Complaint</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div class="tab-pane fade active in" id="mobile">
								
                                <a class="mobileList" href="#">
                                			<img src="<?php echo Configure::read('AWS_URL'); ?>/images/pages/square.png">
                                            <p>Dth</p>
                                </a>
                                
                          </div>
                          <div class="tab-pane fade" id="status">
                                <!--<div class="form-inline RequestSearch">
                                    <span >Date</span><input style="margin-left:10px;width:150px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>
<!--                                    <span >To</span><input   style="margin-left:10px;width:150px;" id="transDate2" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly>-->
<!--                                    <span id="searchReversal" class="glyphicon glyphicon-search" style="margin-left:10px;cursor:pointer"></span>-->
<!--                                    <input id="searchReversal" type="submit" class="default-btn" value="search"/>   -->
                                    <!--<button id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess">Search</button>-->
				         <!--  <input id="searchReversal" data-loading-text="Processing..." type="submit" class="default-btn searchProcess" value="search"/>	
                                </div>-->
                                <ul id="myTab" class="nav nav-tabs">
                                  <li class=""><a href="#earningPrevious" class="reportNavigation reportNavigationPrev earningPrevious" onclick="PayOne.reports.renderTransStatus('mob',1,dt,true,'#transStatusTableBody',transStatusTable);"><span></span></a></li>
                                  <li class="active"><a href="#earningDate1" data-toggle="tab"><input style="margin-left:10px;width:150px;" id="transDate1" type="text" class="form-control" value="<?php echo date("d-m-Y");?>" data-date-format="dd-mm-yyyy" readonly /></a></li>
                                  <li class=""><a href="#earningNext" class="reportNavigation reportNavigationNext earningNext"><span></span></a></li>
                                </ul>
                          		<div class="table-responsive">
                            	
                                <table class="table table-striped table-hover order" id="transStatusTable" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Subscriber Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                        </tr>
                                      </thead>
                                      <tbody id="transStatusTableBody" >
                                        
                                      </tbody>
                                    </table>
                                    </div>
                          </div>
                          <div class="tab-pane fade" id="details">
                          <div class="table-responsive">
                            <div class="form-inline RequestSearch">
                                <input id="searchNo" type="text" class="form-control" placeholder="Subscriber Id"/>
                                <input id="searchByNo" type="submit" class="default-btn searchProcess" value="search"/>   
                            </div>
                             
                              <table class="table table-striped table-hover order-column" id="lastTenTransTable" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">#</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c"></th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Subscriber Id</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Amount</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Time</th>
                                          <th style="background: none repeat scroll 0 0 #e73c3c">Status</th>
                                        </tr>
                                      </thead>
                                      <tbody id="lastTenTransTableBody" >
                                        
                                      </tbody>
                                    </table>
                                    </div>
                          </div>
                        </div>
                
                    
                    
                    