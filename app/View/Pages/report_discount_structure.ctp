<script>

$(function() {
    
    var htmlStr = "";
    $.each(PayOne.config.operators.mobRecharge , function(i ,v){
       htmlStr = htmlStr + "<a class=\"mobileList\" href=\"/recharges/mobile/"+v.name+"/"+v.id+"\""+"><img src=\"<?php echo Configure::read('AWS_URL'); ?>"+v.logoUrl+"\"><p>"+v.name+"</p></a>"
    });
    // $("#mobile").html(htmlStr);
    
    //$('#report_transactions_tables').dataTable();
    //$('#report_mob_transactions_tables').dataTable();
    //$('#report_dth_transactions_tables').dataTable();
    //$('#report_vas_transactions_tables').dataTable();
    
    
        
    /*$('#searchReversal').click(function (e) {        
        var date_from = $('#transDate1').val();
        if(date_from == '' ){
                alert("Please select proper date.");
        } else {
                var date_from_arr = date_from.split("-");
                var dt = date_from_arr[2]+"-"+date_from_arr[1]+"-"+date_from_arr[0];  
                PayOne.reports.renderTransStatus("mob",1,dt,true);                            
        }        
    });*/
    
    
    
    $('#myTab a').on("click",function (e) {        
        var hash  = $(this).attr('href');        
        //if(hash=="#mob"){
        //    PayOne.reports.lastTran(1);// 1 for mobile
        //} else 
        //if(hash=="#dth"){
        //    PayOne.reports.lastTran(1);// 1 for mobile
        //} else if(hash=="#vas"){
        //    PayOne.reports.lastTran(1);// 1 for mobile
        //} 
    });
    /*$('#searchMobTransactionData').on("click",function (e) {   
        
        //alert("Hello");return;
        var mobTransDate1 = $("#mobTransDate1").val();  
        var mobTransDate2 = $("#mobTransDate2").val();       
        
        var eleId = "#report_mob_transactions_table_body";
        PayOne.reports.renderTransactionsData(mobTransDate1,mobTransDate2,1,true,eleId);//pageNo = 0 AND items_per_page = 0            
    });
    
    $('#searchDthTransactionData').on("click",function (e) {     
        //alert("Hello");return;
        var dthTransDate1 = $("#dthTransDate1").val();  
        var dthTransDate2 = $("#dthTransDate2").val();  
        
        //var items_per_page = $("#items_per_page").val();
        //var page_no = $("#page_no").val();
        var eleId = "#report_dth_transactions_table_body";
        PayOne.reports.renderTransactionsData(dthTransDate1,dthTransDate2,2,true,eleId);//pageNo = 0 AND items_per_page = 0            
    });
    
    $('#searchVasTransactionData').on("click",function (e) {     
        //alert("Hello");return;
        var vasTransDate1 = $("#vasTransDate1").val();  
        var vasTransDate2 = $("#vasTransDate2").val();  
        
        //var items_per_page = $("#items_per_page").val();
        //var page_no = $("#page_no").val();
        var eleId = "#report_vas_transactions_table_body";
        PayOne.reports.renderTransactionsData(vasTransDate1,vasTransDate2,3,true,eleId);//pageNo = 0 AND items_per_page = 0            
    });*/
        
    
    var callLogin = false;
    var eleId = "#report_mob_discount_table_body";    
    PayOne.reports.renderDiscountData(1,callLogin,eleId);//pageNo = 0 AND items_per_page = 0
    
    eleId = "#report_dth_discount_table_body";    
    PayOne.reports.renderDiscountData(2,callLogin,eleId);
    
    eleId = "#report_vas_discount_table_body";    
    PayOne.reports.renderDiscountData(3,callLogin,eleId);

    eleId = "#report_pay1_topup_discount_table_body";    
    PayOne.reports.renderDiscountData(5,callLogin,eleId);
    
    eleId = "#report_online_payment_discount_table_body";    
    PayOne.reports.renderDiscountData(7,callLogin,eleId);
    
});
</script>
<div class="breadcrumb">Report &gt; Transaction</div>
                        <ul id="myTab" class="nav nav-tabs">
                          <li class="active"><a href="#mobile" data-toggle="tab">Mobile</a></li>
                          <li class=""><a href="#dth" data-toggle="tab">DTH</a></li>
                          <li class=""><a href="#entertainment" data-toggle="tab">Entertainment</a></li>
                          <li class=""><a href="#top_up" data-toggle="tab">Pay1 Top-up</a></li> 
                          <li class=""><a href="#online_payment" data-toggle="tab">Online Payment</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div class="tab-pane fade active in" id="mobile">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                             
							<div class="table-responsive">
                           	<table class="table table-striped table-hover order-column" id="report_mob_discount_table" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Product</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Discount %</th>
                                          </tr>
                                          
                                      </thead>
                                      
                                      <tbody id="report_mob_discount_table_body">
                                        
                                      </tbody>
                                    </table>
								</div>
                          </div>
                          <div class="tab-pane fade" id="dth">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                             
							<div class="table-responsive">
                            	<table class="table table-striped table-hover order-column" id="report_dth_discount_table" cellspacing="0" width="100%">
                                      <thead>
                                           <tr>
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Product</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Discount %</th>
                                          </tr>                                          
                                      </thead>
                                      <tbody id="report_dth_discount_table_body">
                                        
                                      </tbody>
                                    </table>
								</div>
                          </div>
                          <div class="tab-pane fade" id="entertainment">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                            
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_vas_discount_table" cellspacing="0" width="100%">
                                      <thead>
                                           <tr >
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Product</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Discount %</th>
                                          </tr>
                                          
                                      </thead>
                                      <tbody id="report_vas_discount_table_body">
                                        
                                      </tbody>
                                    </table>
							</div>
                    </div>
                    <div class="tab-pane fade" id="pay1_topup">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                            
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_pay1_topup_discount_table" cellspacing="0" width="100%">
                                      <thead>
                                           <tr >
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Product</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Discount %</th>
                                          </tr>
                                          
                                      </thead>
                                      <tbody id="report_pay1_topup_discount_table_body">
                                        
                                      </tbody>
                                    </table>
							</div>
                    </div>
                    <div class="tab-pane fade" id="online_payment">
<!--                          <div class="form-inline StatusSearch">
                                <input type="text" class="form-control" value="16-2-2014" data-date-format="dd-mm-yyyy" id="dp1"><span class="glyphicon glyphicon-calendar"></span>
                            </div>-->
                            
							<div class="table-responsive">
                           <table class="table table-striped table-hover order-column" id="report_online_payment_discount_table" cellspacing="0" width="100%">
                                      <thead>
                                           <tr >
                                              <th style="background: none repeat scroll 0 0 #e73c3c">Product</th>
                                              <th class="rightAlign" style="background: none repeat scroll 0 0 #e73c3c">Discount %</th>
                                          </tr>
                                          
                                      </thead>
                                      <tbody id="report_online_payment_discount_table_body">
                                        
                                      </tbody>
                                    </table>
							</div>
                    </div>
                </div>
            