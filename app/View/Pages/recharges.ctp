<script>
var deviceUuid = "";
$(function() {
    
     $('#mobileRecharge').on('submit', function (event) {
         event.preventDefault();
         mobRecharge(1);
     });
    if($("#operatorType").val() == "mobile"){     
        $.each(PayOne.config.operators.mobRecharge , function(i ,v){
            if(v.id == $("#operatorId").val() && !v.spcl ){
                $("#stvOrTopUp_radio2").hide();
            }            
        });
    }else if($("#operatorType").val() == "dth"){
         $.each(PayOne.config.operators.dthRecharge , function(i ,v){
            if(v.id == $("#operatorId").val() && !v.spcl ){
                $("#stvOrTopUp_radio2").hide();
            }            
        });
    }
    function mobileValidate(y){
        var y = y.trim();
        if(y == ""){alert("Enter mobile number");return 0;
        }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
        }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
        }
        return 1;
    }

    function amtValidate(y) {
        var err = '';
        var y = y.trim();
        if(y == ""){alert("Plese enter proper amount");return 0;
        }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
        }
        return 1;
    }

    function mobRecharge(cnfFlag){
       // prevPage = fromLocation;

        var mobileNo = $("#mobileNo").val().trim();
        var rechargeAmt = $("#rechargeAmt").val().trim();
        if(mobileValidate(mobileNo) == '0') return;	
        if(amtValidate(rechargeAmt) == '0')return;

        var stv = $("input[name='stvOrTopUp_radio']:checked").val();

        if(cnfFlag == 1){
            var cnf = "Operator: "+$('#operatorName').html()+"\nNumber: "+mobileNo+"\nAmount: Rs."+rechargeAmt+"\n";
            var r=confirm(cnf+"Press OK to confirm");
            if(r==false){
                return false;
            }
        }
        //alert("==========="+cnfFlag+","+fromLocation);
            /*if(passFlag=='F'){
                showHide('page1','pinchange');
                return;
            }*/
            //alert("==========++=");
            //var tmp = $('#rec_now_loader').html();
            //$('#rec_now_loader').html(loader);
            //alert("method=mobRecharge&mobileNumber="+mobileNo+"&operator="+$('#operatorId').val()+"&subId="+mobileNo+"&amount="+rechargeAmt+"&circle="+$('#mob-rec-cir').val()+"&special=stv&type=flexi");
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {method:"mobRecharge",mobileNumber:mobileNo,operator:$("#operatorId").val(),subId:mobileNo,amount:rechargeAmt,/*circle:$('#mob-rec-cir').val(),*/special:stv,type:"flexi"},
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status){ //alert("hello=="+data);
                //$('#rec_now_loader').html(tmp);
                $.each(data, function(i,item){				
                    if(item.status == 'failure'){                       
                        PayOne.core.failChk(item.code,item.description);
                       
                    }else{
                        if(item.status == 'success'){
                            alert('Recharge request sent successfully');
                            alert($('#mobileNo').attr('value', ""));
                            $("#mobileNo").val("");
                            $("#rechargeAmt").val("");
                            //$('#MRODd').attr('value', '');
                            //$('#MROtitle').html('');
                            //showHide('page1','navMobile');
                            $('#retailerBal').html(item.balance);
                        }
                    }
                });
            },
            error: function(){
                //$('#rec_now_loader').html(tmp);
            }
        });
    }
    
});
</script>
              
            <div class="breadcrumb"><a href="/index/<?php echo $type ;?>"><?php echo $type ;?></a> &gt; <a href="#"><?php echo $operatorName ;?></a> &gt; Recharge</div>
				<form role="form" id="mobileRecharge">
                    <div class="form-group">
                        <input type="hidden" name="operatorId" id="operatorId" value="<?php echo $operatorId ;?>" />
                        <input type="hidden" name="operatorName" id="operatorName" value="<?php echo $operatorName ;?>" />
                        <input type="hidden" name="operatorType" id="operatorType" value="<?php echo $type ;?>" />
                        <?php if($type == "mobile"){?>
						<input style="width: 40px ; float:left;" type="radio" class="form-control" id="stvOrTopUp_radio1" name="stvOrTopUp_radio" value="0" placeholder="" checked="true"/><label for="stvOrTopUp_radio1" style="float:left;">Top Up</label>
                        <input style="width: 40px; float:left;" type="radio" class="form-control" id="stvOrTopUp_radio2" name="stvOrTopUp_radio" value="1" placeholder=""/><label for="stvOrTopUp_radio2" >STV</label>
						<?php }?>
					</div>
					<div class="form-group">
						<label for="mobileNo"  for="mobileNo">Enter Prepaid Mobile Number</label>
						<input type="text" class="form-control" id="mobileNo" placeholder="" pattern="^\d{10,10}" title="10 Digit Mobile No" />
					</div>
					<div class="form-group">
						<label for="rechargeAmt">Enter Amount</label>
						<input type="text" class="form-control" id="rechargeAmt" placeholder="" pattern="^\d{1,5}(\.\d{1,2})?$" title="Amount should be a no"/>
					</div>                        
                          
					<div class="checkPlanForm">
						<div class="form-group">
							<label for="mobileCircle">Select Circle</label>
							<select class="form-control" id="mobileCircle">
								 <option></option>
								 <option>Airtel</option>
								 <option>Vodafone</option>
								 <option>Tata Docomo</option>
								 <option>MTS</option>
							</select>
						</div>
					  
						<div class="form-group">
							<label for="mobileCatagory">Select Catagory</label>
							<select class="form-control" id="mobileCatagory">
							 <option></option>
							 <option>Airtel</option>
							 <option>Vodafone</option>
							 <option>Tata Docomo</option>
							 <option>MTS</option>
							</select>
						</div>
					  
						<div class="form-group">
							<label for="mobilePlan">Select Plan</label>
							<select class="form-control" id="mobilePlan">
							 <option></option>
							 <option>Airtel</option>
							 <option>Vodafone</option>
							 <option>Tata Docomo</option>
							 <option>MTS</option>
							</select>
						</div>
					</div>
                    <!--<p class="selectedPlan">With Rs 51 you can get a talktime of Rs 42 with a validity of 30 days</p>
                    <p><a href="#" class="checkPlan">Check Your Plans &gt;&gt;</a></p>-->
					<input type="submit" class="form-control default-btn" value="Proceed"/>
                </form>

				