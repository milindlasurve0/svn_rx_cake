
var PayOne = (typeof PayOne != "undefined") ?  PayOne : {};
baseUrl = "http://apptesting.pay1.in/";
cookieDomain = window.location.host;

PayOne.config ={
    version:1.0,
    cookiePath: "/",
    urls:{ 
        api:baseUrl+"apis/receiveWeb/mindsarray/mindsarray/json?"         
    },
    vars:{
        mro : "",
        mdo : "",
        version : "1.0",
        deviceUuid : "",
        hasStorage : 0,
        conn : null,
        vas : '',
        p : new Array(),
        loader : '<img src="/images/loader.gif"/>',
        pI : '<img src="/images/icons/progress.png"/>',
        sI : '<img src="/images/icons/success.png"/>',
        fI : '<img src="/images/icons/failure.png"/>',
        rsI : '<img src="/images/icons/reversalS.png?2"/>',
        rpI : '<img src="/images/icons/reversalP.png?2"/>',
        rdI : '<img src="/images/icons/reversalD.png?2"/>',
        dI : '<img src="/images/icons/details.png?2" class="link"/>',
        c : "'Cannot proceed'",
        page_no : 1,
        prevPage : '',
        passFlag : ''  
    },
            
                           
    pcla :{
        L1 : 'm_aircel.png',
        L2 : 'm_airtel.png',
        L3  : 'm_bsnl.png',
        L4  : 'm_idea.png',
        L5  : 'm_loop.png',
        L6  : 'm_mts.png',
        L7  : 'm_reliance_cdma.png',
        L8  : 'm_reliance.png',
        L9  : 'm_docomo.png',
        L10  : 'm_indicom.png',
        L11  : 'm_uninor.png',
        L12  : 'm_videocon.png',
        //L13  : 'm_virgin.png'
        //L14  : 'm_virgin.png'
        L15  : 'm_vodafone.png',
        L16  : 'm_airtel.png',
        L17  : 'd_bigtv.png',
        L18  : 'd_dishtv.png',
        L19  : 'd_sundirect.png',
        L20  : 'd_tatasky.png',
        L21  : 'd_videocon.png',
        L30  : 'm_mtnl.png',
        L31  : 'm_mtnl.png',
        L29  : 'm_uninor.png',
        L28  : 'm_videocon.png',
        L27  : 'm_docomo.png',
        L26  : 'm_airtel.png',
        L34  : 'm_bsnl.png',
        L36  : "m_docomo.png",
        L37  : "m_loop.png",
        L38  : "m_bsnl.png",
        L39  : "m_idea.png",
        L40  : "m_indicom.png",
        L41  : "m_vodafone.png",
        L42  : "m_airtel.png",
        L43  : "m_reliance.png",
        L45  : "reliance_energy.png",
        L46  : "bses_rajdhani.png",
        L47  : "bses_yamuna.png",
        L48  : "ndpl.png",
        L49  : "airtel_landline.png",
        L50  : "mtnl.png",
        L51  : "mahangar_gas.png",
        L52	 : "m_pay1_topup.png",
        L53	 : "op_ongo.png"	
       
    },
    mnth:{
        M01 : 'Jan',     
        M02 : 'Feb',
        M03 : 'Mar',
        M04 : 'Apr',
        M05 : 'May',
        M06 : 'Jun',
        M07 : 'Jul',
        M08 : 'Aug',
        M09 : 'Sep',
        M10 : 'Oct',
        M11 : 'Nov',
        M12 : 'Dec'
    },    
    circles:[
    {
        'type': 'States',
        'id': 1,
        'name': 'Andhra Pradesh',
        'code': 'AP'
    },
    {
        'type': 'States',
        'id': 2,
        'name': 'Assam',
        'code': 'AS'
    },
    {
        'type': 'States',
        'id': 3,
        'name': 'Jharkhand',
        'code': 'BR'
    },
    {
        'type': 'Metros',
        'id': 5,
        'name': 'Delhi NCR',
        'code': 'DL'
    },
    {
        'type': 'States',
        'id': 6,
        'name': 'Gujarat',
        'code': 'GJ'
    },
    {
        'type': 'States',
        'id': 7,
        'name': 'Haryana',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 8,
        'name': 'Himachal Pradesh',
        'code': 'HP'
    },
    {
        'type': 'States',
        'id': 9,
        'name': 'Jammu & Kashmir',
        'code': 'JK'
    },
    {
        'type': 'States',
        'id': 10,
        'name': 'Karnataka',
        'code': 'KA'
    },
    {
        'type': 'States',
        'id': 11,
        'name': 'Kerala',
        'code': 'KL'
    },
    {
        'type': 'Metros',
        'id': 12,
        'name': 'Kolkata',
        'code': 'KO'
    },
    {
        'type': 'States',
        'id': 13,
        'name': 'Maharashtra',
        'code': 'MH'
    },
    {
        'type': 'States',
        'id': 14,
        'name': 'Madhya Pradesh',
        'code': 'MP'
    },
    {
        'type': 'Metros',
        'id': 15,
        'name': 'Mumbai',
        'code': 'MU'
    },
    {
        'type': 'States',
        'id': 16,
        'name': 'Tripura',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 17,
        'name': 'Orissa',
        'code': 'OR'
    },
    {
        'type': 'States',
        'id': 18,
        'name': 'Punjab',
        'code': 'PB'
    },
    {
        'type': 'States',
        'id': 19,
        'name': 'Rajasthan',
        'code': 'RJ'
    },
    {
        'type': 'States',
        'id': 20,
        'name': 'Tamil Nadu',
        'code': 'TN'
    },
    {
        'type': 'States',
        'id': 21,
        'name': 'Uttar Pradesh (East)',
        'code': 'UE'
    },
    {
        'type': 'States',
        'id': 22,
        'name': 'Uttarakhand',
        'code': 'UW'
    },
    {
        'type': 'States',
        'id': 23,
        'name': 'West Bengal',
        'code': 'WB'
    }
    ],
    operators:{
        "mobRecharge":[
        {
            id:1,
            name:"Aircel",
            logoUrl:"/img/op_aircel.png",
            spcl:false
        },
        {
            id:2,
            name:"Airtel",
            logoUrl:"/img/op_airtel.png",
            spcl:false
        },
        {
            id:3,
            name:"BSNL",
            logoUrl:"/img/op_bsnl.png",
            spcl:true
        },
        {
            id:4,
            name:"Idea",
            logoUrl:"/img/op_idea.png",
            spcl:false
        },      
        {
            id:6,
            name:"MTS",
            logoUrl:"/img/op_mts.png",
            spcl:false
        },
        {
            id:7,
            name:"Reliance CDMA",
            logoUrl:"/img/op_relcdma.png",
            spcl:false
        },
        {
            id:8,
            name:"Reliance GSM",
            logoUrl:"/img/oprelgsm.png",
            spcl:false
        },
        {
            id:9,
            name:"Tata Docomo",
            logoUrl:"/img/op_docomo.png",
            spcl:true
        },
        {
            id:10,
            name:"Tata Indicom",
            logoUrl:"/img/op_indicom.png",
            spcl:true
        },
        {
            id:11,
            name:"Uninor",
            logoUrl:"/img/op_uninor.png",
            spcl:true
        },
        {
            id:12,
            name:"Videocon",
            logoUrl:"/img/op_videocon.png",
            spcl:true
        },
        {
            id:15,
            name:"Vodafone",
            logoUrl:"/img/op_vodafone.png",
            spcl:false
        },
        {
            id:30,
            name:"MTNL",
            logoUrl:"/img/op_mtnl.png",
            spcl:true
        }
		
        ],
       
     
    
   
    
        "dthRecharge":[
        {
            id:1,
            pay1id:16,
            name:"Airtel DTH",
            logoUrl:"/images/icons/m_airtel.png"
        },
        {
            id:2,
            pay1id:17,
            name:"Big TV DTH",
            logoUrl:"/images/icons/d_bigtv.png"
        },
        {
            id:3,
            pay1id:18,
            name:"Dish TV DTH",
            logoUrl:"/images/icons/d_dishtv.png"
        },
        {
            id:4,
            pay1id:19,
            name:"Sun TV DTH",
            logoUrl:"/images/icons/d_sundirect.png"
        },
        {
            id:5,
            pay1id:20,
            name:"Tata Sky DTH",
            logoUrl:"/images/icons/d_tatasky.png"
        },
        {
            id:6,
            pay1id:21,
            name:"Videocon DTH",
            logoUrl:"/images/icons/d_videocon.png"
        }],            
        "vasRecharge":[
        {
            id:22,
            name:"Dil Vil Pyar Vyar",
            logoUrl:"/images/icons/m_airtel.png"
        },
        {
            id:23,
            name:"Naughty Jokes",
            logoUrl:"/images/icons/m_airtel.png"
        },
        /*{
              id:24,
              name:"PNR Alert",
              logoUrl:"/images/icons/m_airtel.png"
            },
            {
              id:25,
              name:"Instant Cricket",
              logoUrl:"/images/icons/m_airtel.png"
            },
            {
              id:32,
              name:"Chatpati Baate Mini Pack",
              logoUrl:"/images/icons/m_airtel.png"
            },
            {
              id:33,
              name:"Chatpati Baate Mega Pack",
              logoUrl:"/images/icons/m_airtel.png"
            },
         */
        {
            id:35,
            name:"Ditto TV",
            logoUrl:"/images/icons/m_airtel.png"
        }],
            
        "mobBillPayment":[{
            id:36,
            name:"Docomo Postpaid",
            logoUrl:"/images/icons/m_docomo.png"
        },
        {
            id:37,
            name:"Loop Mobile PostPaid",
            logoUrl:"/images/icons/m_loop.png"
        },
        {
            id:38,
            name:"Cellone PostPaid",
            logoUrl:"/images/icons/m_bsnl.png"
        },
        {
            id:39,
            name:"IDEA Postpaid",
            logoUrl:"/images/icons/m_idea.png"
        },
        {
            id:40,
            name:"Tata TeleServices PostPaid",
            logoUrl:"/images/icons/m_indicom.png"
        },
        {
            id:41,
            name:"Vodafone Postpaid",
            logoUrl:"/images/icons/m_vodafone.png"
        },
        {
            id:42,
            name:"Airtel Postpaid",
            logoUrl:"/images/icons/m_airtel.png"
        },
        {
            id:43,
            name:"Reliance Postpaid",
            logoUrl:"/images/icons/m_reliance.png"
        }],
    "utilityBillPayment":[{
            id:45,
            name:"Reliance Energy",
            logoUrl:"/images/icons/reliance_energy.png"
        },
        {
            id:46,
            name:"BSES Rajdhani",
            logoUrl:"/images/icons/bses_rajdhani.png"
        },
        {
            id:47,
            name:"BSES Yamuna",
            logoUrl:"/images/icons/bses_yamuna.png"
        },
        {
            id:48,
            name:"North Delhi Power Limited",
            logoUrl:"/images/icons/ndpl.png"
        },
        {
            id:49,
            name:"Airtel",
            logoUrl:"/images/icons/airtel_landline.png"
        },
        {
            id:50,
            name:"MTNL Delhi",
            logoUrl:"/images/icons/mtnl.png"
        },
        {
            id:51,
            name:"Mahanagar Gas Limited",
            logoUrl:"/images/icons/mahangar_gas.png"
        }
        ],
        "walletTopup":[{
            id:52,
            product_id:44,
            name:"Pay1 Topup",
            logoUrl:"/images/icons/pay1_wallet.png"
        },
        {
            id:53,
            product_id:65,
            name:"Ongo Topup",
            logoUrl:"/images/icons/ongo_logo.png"
        }]
    }
}

