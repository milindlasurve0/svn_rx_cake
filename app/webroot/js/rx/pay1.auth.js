var PayOne = (typeof PayOne !== undefined) ? PayOne : { };
PayOne.auth = {
    loginStatus:false,        
    setLoginStatus: function(a) {
        this.loginStatus = a;
    },
    logout:function(){           
        var r=confirm("Press OK to logout");
        if(r==true){
            $.ajax({
                url: PayOne.config.urls.api,
                type: "POST",
                data: {
                    method:'logout'
                },
                dataType: 'jsonp',
                jsonp: 'root',
                timeout: 50000,
                success: function(data, status){
                    $.each(data, function(i,item){
                        if(item.status == 'failure'){
                            failChk(item.code,item.description);
                        }else{
                            if(item.status == 'success'){
                                /*                                            
                                PayOne.core.cookie.unset("profileDetails")
                                PayOne.core.cookie.unset("shopname");                                                                
                                PayOne.core.cookie.unset("street");
                                PayOne.core.cookie.unset("city");
                                PayOne.core.cookie.unset("pincode");
                                PayOne.core.cookie.unset("state");
                                PayOne.core.cookie.unset("lat");
                                PayOne.core.cookie.unset("lng");
                                PayOne.core.cookie.unset("mobile");
                                PayOne.core.cookie.unset("pinStatus");
                                PayOne.core.cookie.unset("area");
                                PayOne.core.cookie.unset("area_id");
                                PayOne.core.cookie.unset("balance");
                                */
                                PayOne.core.cookie.clear();
                                window.location.reload();
                            }
                        }	
                    });
                },
                error: function(){}
            });
        }            
    },	
    login: { 
        init:function(){
            $('#login_modal').on('submit', function (event) {
                event.preventDefault();
                $("#login_modal_submit").button('loading');
                var lat = PayOne.core.cookie.get("lat");
                var lng = PayOne.core.cookie.get("lng");
                if((lat=='')||(lat==null)|| (lat=='null')|| (lng=='')||(lng==null)||(lng=='null')){ 
                    var options = {
                        enableHighAccuracy: true,
                        timeout: 5000,
                        maximumAge: 0
                    };
                    navigator.geolocation.getCurrentPosition(PayOne.core.gMapCallBack, PayOne.core.gMapErrorCallBack, options);
                }else{                            
                    PayOne.auth.login.submit(lat,lng);
                }                        
            });
                    
            $('#logoutAction').on('click', function (event) {
                PayOne.auth.logout();
            });
                    
        }, 
        check:function(){
            alert(PayOne.core.cookie.get("profileDetails"));
            alert(PayOne.core.cookie.get("street"));
            alert(PayOne.core.cookie.get("city"));
            alert(PayOne.core.cookie.get("pincode"));
            alert(PayOne.core.cookie.get("state"));
            alert(PayOne.core.cookie.get("lat"));
            alert(PayOne.core.cookie.get("lng"));
            alert(PayOne.core.cookie.get("mobile"));
            alert(PayOne.core.cookie.get("pinStatus"));
            alert(PayOne.core.cookie.get("balance"));
        },
        submit: function(lat,lng,resendOTP) {
                           
            var mobile = $("#mobile_no").val();
            var pin    = $("#pin").val();

            var otp_call = 0;
            if(resendOTP){
                otp_call = 1;
            }
            
            if(mobile==''){
                alert("Please Enter 10 Digit Mobile Number");
                $("#login_modal_submit").button('reset');
                return false;
            }
            else if(pin==''){
                alert("Please Enter 4 Digit Password");
                $("#login_modal_submit").button('reset');
                return false;
            }
             else {
              
            var old_uuid = PayOne.core.cookie.get("uuid");
            if(old_uuid){    
                var d_u_n = old_uuid;
            }else{
                var d_u_n =  new Fingerprint().get();
            }
              
            //$("#login_modal_submit").button('loading');
            // Replace the submit button and any text with a spinner
            $.ajax({ 
                url: PayOne.config.urls.api,
                type: "POST", //,gcm_reg_id:mobile,latitude:lat,longitude:long,device_type:web
                data: {
                    method:'authenticate_new',
                    uuid:d_u_n,
                    mobile:mobile,
                    password:pin,
//                    type:1,
                    gcm_reg_id:d_u_n,
                    latitude:lat,
                    longitude:lng,
                    otp_via_call:otp_call,
                    device_type:'web',
                    mode:'test'
                },
                dataType: 'jsonp',
                jsonp: 'root',
                timeout: 50000,
                success: function(data, status){ 
                    /* If there were form validation errors returned                                        
                                        */
                                       //console.log(data);
                    $("#login_modal_submit").button('reset');
                    $.each(data, function(i,item){ 
                        if(item.status == 'failure'){ 
                            PayOne.core.failChk(item.code,item.description);
                        }
//            display OTP models for User Device Mapping if User Device is Not Mapped
                        else if(item.status == 'successOTP'){ 
                            $("#OtpMobNum").val(mobile);
                            PayOne.core.otpChk(item.code,item.description.description);
                        }
                        else{ 
                            if(item.status == 'success'){ 
                                var lat =  item.description.latitude;
                                var lng =  item.description.longitude;
                                var profile_details = mobile+"|<>|"+lat+"|<>|"+lng;
                                PayOne.core.cookie.clear();
                                //console.log(item.pg_flag);
                                PayOne.core.cookie.set("uuid", d_u_n,180);
                                PayOne.core.cookie.set("profileDetails", profile_details, 1)
                                PayOne.core.cookie.set("shopname", item.description.shopname, 1);                                                                
                                PayOne.core.cookie.set("street", item.description.address, 1);
                                PayOne.core.cookie.set("city", item.description.city_name, 1);
                                PayOne.core.cookie.set("mobile", item.description.mobile, 1);
                                PayOne.core.cookie.set("pincode", item.description.pin, 1);
                                PayOne.core.cookie.set("state", item.description.state_name, 1);
                                PayOne.core.cookie.set("area", item.description.area_name, 1);
                                PayOne.core.cookie.set("area_id", item.description.area_id, 1);
                                PayOne.core.cookie.set("balance", item.description.balance, 1);
                                PayOne.core.cookie.set("flag", item.pg_flag, 1);
                                PayOne.core.cookie.set("parentId", item.description.parent_id, 1);
                                PayOne.core.cookie.set("min_amount_for_prompt", item.min_amount_for_prompt, 1);
                                
                                //console.log(item.description);
                                //console.log(PayOne.core.cookie.get("shopname"));
                                //return false;
                                //PayOne.core.cookie.set("lat", lat, 1);
                                //PayOne.core.cookie.set("lng", lng, 1);
                                PayOne.core.cookie.set("mobile", mobile, 1);
                                PayOne.core.cookie.set("pinStatus", item.passFlag, 1);
                                                                    
                                var htmlStr  =  "<a class=\"pull-left\" href=\"#\"><img class=\"media-object user-img\" src=\"/img/ic_loggedin.png\" alt=\"...\"></a>"+
                                                    "<div class=\"media-body\">"+
                                                    "<p class=\"userName top\" style=\"color:black;\">"+item.description.shopname+"</p>"+
                                                    "<p id=\"retailerBal\" class=\"balance\">"+item.description.balance+"</p>"+
                                                    //"<span title=\"Refresh Balance\" class=\"glyphicon glyphicon-refresh balRefresh\" style=\"border: 1px solid #503c97;background:none repeat scroll 0 0 #503c97;cursor: pointer;\"></span>"+
                                                "</div>";    
                                $("#media_header_section").html(htmlStr);  
                                $("#refreshBalLi").show();
                                //passFlag
                                //PayOne.core.registerRefreshBalance();
                                if(item.passFlag == 0){ 
                                    window.location.replace("/updatePin");
                                }
                                                                 
                                                                 
                                $('#login').modal('hide');
                                $("#signUpActionLi").hide();
                                $("#logoutActionLi").show();	
                                   
                                if(item.passFlag == '0'){ 
                                    //@TODO redirect to Update Pin Page
                                    //@TODO on successfull pin change , redirect to previous url
                                }
                                window.location.reload();
                            }
                        }
                    });
                                        
                },

                error: function() {
                $("#login_modal_submit").button('reset');
                //Put the submit elements back
                //submitContainer.html(submitElems);
                }
            } );
        }
           
                        
            return false;
        },
        localLoginSetup: function(token, userId, fullname , profile_image,registered_as,facebook_id,is_fblogin,profile_status,organisation_link_status) {
            var url = window.location.toString();
            var params= url.split("/");
            PayOne.core.user.setToken(token);
			
            PayOne.core.user.setUserId(userId);
			
            PayOne.core.user.setName(fullname);
                         
            PayOne.core.user.setProfileStatus(profile_status);
			
            PayOne.core.user.setOrganisationLinkStatus(organisation_link_status);
                                                         
            PayOne.core.cookie.set('profile_image', profile_image, 30);
                        
            PayOne.core.cookie.set('registered_as', registered_as, 30);
                        
            PayOne.core.cookie.set('facebook_id', facebook_id, 30);
                        
            PayOne.core.cookie.set('is_fblogin', is_fblogin, 30);
                        
            PayOne.auth.setLoginStatus(true);
			
            PayOne.auth.login.showLoggedInNavBar();
            if($('#user_option').get(0) && $('#user_option1').get(0)){
                $('#school_registration').prepend("<fieldset ><dl>You are logged in as  <span style='color:#106FB8'>"+fullname+"</span>  , if  you want to register  with the same existing account  then  sign  up  below   <br>Otherwise  <span id='nav-logout'><a  href='' style='color:#6F5204'> Click here to logout and login as different user</a></span>.</dl></fieldset> ");
                $('#user_option').hide();
                $('#user_option1').hide();                            
            }
                        
            if(params[3]=='registration'){
                window.location.href=mainUrl+'dashboard';
            }else{
            // location.reload();
            }
            PayOne.userprofile.checkFBstatus(PayOne.core.user.id);
        },
        showLoggedInNavBar: function() {
            $('#nav-login').remove();
            $('#nav-join').remove();
			
            if(PayOne.core.cookie.get('is_fblogin')=== 'false' && $('#nav-sync').length <= 0){
                $('.top_menu').prepend('<li class="nav-btn-icon" id="nav-sync"><a href="#">Enable Social Features</a></li>');
            }else{
                $('#nav-sync').remove();
            }
            if($('#nav-dashboard').length <= 0) {
                $('.top_menu').prepend('<li class="nav-btn-icon" id="nav-dashboard"><a href="'+mainUrl+'dashboard">'+PayOne.core.user.name+'</a></li>');
            }   
            if($('#nav-logout').length <= 0) {
                $('.top_menu').prepend('<li class="nav-btn-icon" id="nav-logout" style="border-right:0px;"><a href="javascript:void();">Logout</a></li>');
            }
                        
        }
    },
    
	
    init: function() {
            
        var $this = this;		
        var st = PayOne.auth.getLoginStatus();
        if(st === 'true' || st === true) { 
            PayOne.userprofile.checkFBstatus(PayOne.core.user.id);
        }
		
        $('#nav-sync a').live('click',function() {
            if(PayOne.core.cookie.get('facebook_id') && PayOne.core.cookie.get('is_fblogin') !='false'){
                $this.invokeFBLogin();
            }else{
                $this.callSync();
            }			
            return false;
        });
		
        // Bind any login elements to our callLogin Method
        $('#nav-logout a').live('click',
            function() {
                $this.logout();
                return false;
            }
            );
                

    //connect local PayOne user to connect with facebook

    // Bind any login elements to our callLogin Method  for College , School & University
        
    }, 
    getLoginStatus: function() {
        //return false;
        var $this = PayOne.auth;
        var mobile = PayOne.core.cookie.get('mobile');
        var profileDetails = PayOne.core.cookie.get('profileDetails');
        if(mobile == null || profileDetails == null) {
            $this.loginStatus = false;
            return $this.loginStatus;
        } else {
            $this.loginStatus = true;
            return $this.loginStatus;
        }
        return PayOne.auth.loginStatus;
    },
    //Verify User OTP for User Device Mapping
    verifyOTPofUserDevice: function(mobile,otp) {
        var lat = PayOne.core.cookie.get("lat");
        var lng = PayOne.core.cookie.get("lng");
        var device_uniuqe_num =  new Fingerprint().get();
        $.ajax({ 
                url: PayOne.config.urls.api,
                type: "POST", //,gcm_reg_id:mobile,latitude:lat,longitude:long,device_type:web
                data: {
                    method:'verifyOTPOfUserDeviceMapping',
                    uuid:device_uniuqe_num,
                    mobile:mobile,
                    otp:otp,
//                    type:1,
                    gcm_reg_id:device_uniuqe_num,
                    latitude:lat,
                    longitude:lng,
                    device_type:'web'
//                    mode:'test'
                },
                dataType: 'jsonp',
                jsonp: 'root',
                timeout: 50000,
                success: function(data, status){ 
                        $("#login_modal_submit").button('reset');
                        $.each(data, function(i,item){ 
                            if(item.status == 'failure'){ 
                                //display Verify OTP models for User Device Mapping if User Device for OTP verify
                                document.getElementById("otpnumber").value = '';
                                PayOne.core.VerifyOTPfailChk(item.code,item.description);
                            }else{ 
                            if(item.status == 'success'){ 
                                var lat =  item.description.latitude;
                                var lng =  item.description.longitude;
                                var profile_details = mobile+"|<>|"+lat+"|<>|"+lng;
                                PayOne.core.cookie.clear();
                                PayOne.core.cookie.set("profileDetails", profile_details, 1)
                                PayOne.core.cookie.set("shopname", item.description.shopname, 1);                                                                
                                PayOne.core.cookie.set("street", item.description.address, 1);
                                PayOne.core.cookie.set("city", item.description.city_name, 1);
                                PayOne.core.cookie.set("pincode", item.description.pin, 1);
                                PayOne.core.cookie.set("state", item.description.state_name, 1);
                                PayOne.core.cookie.set("area", item.description.area_name, 1);
                                PayOne.core.cookie.set("area_id", item.description.area_id, 1);
                                PayOne.core.cookie.set("balance", item.description.balance, 1);
                                PayOne.core.cookie.set("flag", item.pg_flag, 1);
                                PayOne.core.cookie.set("parentId", item.description.parent_id, 1);
                                PayOne.core.cookie.set("min_amount_for_prompt", item.min_amount_for_prompt, 1);
                                
                                PayOne.core.cookie.set("mobile", mobile, 1);
                                PayOne.core.cookie.set("pinStatus", item.passFlag, 1);
                                                                    
                                var htmlStr  =  "<a class=\"pull-left\" href=\"#\"><img class=\"media-object user-img\" src=\"/img/ic_loggedin.png\" alt=\"...\"></a>"+
                                                    "<div class=\"media-body\">"+
                                                    "<p class=\"userName top\" style=\"color:black;\">"+item.description.shopname+"</p>"+
                                                    "<p id=\"retailerBal\" class=\"balance\">"+item.description.balance+"</p>"+
                                                    //"<span title=\"Refresh Balance\" class=\"glyphicon glyphicon-refresh balRefresh\" style=\"border: 1px solid #503c97;background:none repeat scroll 0 0 #503c97;cursor: pointer;\"></span>"+
                                                "</div>";    
                                $("#media_header_section").html(htmlStr);  
                                $("#refreshBalLi").show();
                                //passFlag
                                //PayOne.core.registerRefreshBalance();
                                if(item.passFlag == 0){ 
                                    window.location.replace("/updatePin");
                                }
                                $('#otp').modal('hide');
                                $("#signUpActionLi").hide();
                                $("#logoutActionLi").show();	
                                   
                                if(item.passFlag == '0'){ 
                                    //@TODO redirect to Update Pin Page
                                    //@TODO on successfull pin change , redirect to previous url
                                }
                                window.location.reload();
                            }
                        }
                    });
                                        
                },

                error: function() {
                $("#login_modal_submit").button('reset');
                //Put the submit elements back
                //submitContainer.html(submitElems);
                }
            } );
    }

};

//$(document).ready(function() {
//	PayOne.auth.init();
//});
