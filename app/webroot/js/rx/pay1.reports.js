//var PayOne = (typeof PayOne != "undefined") ?  PayOne : {};
//var PayOne = { };

PayOne.reports = {
    init: function() {
    },
    gMapErrorCallBack: function(position) {

    },
    renderEarningData: function(dt1, dt2, items_per_page, page_no, eleBodyId, eleObj) {//(type,srvice,dt1,dt2){        
        if (!PayOne.core.utils.validateToFromDate(dt1, dt2, 31, true)) {
            
            return;
        }
        $('.searchProcess').button('loading');
        
        var dt1 = dt1.split("-").reverse();
        var dt2 = dt2.split("-").reverse();
         
        var dt = dt1[0]+dt1[1]+dt1[2] + "-" + dt2[0]+dt2[1]+dt2[2];
        
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: 'earnings',
                date: dt,
                items_per_page: items_per_page,
                page_no: page_no
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                
                $('.searchProcess').button('reset');
                //to clear all row of the table
                PayOne.core.utils.deleteAllRowDataTable(eleBodyId, eleObj);

                $.each(data, function(i, item) {
                    if (item.status == 'failure') {
                        PayOne.core.failChk(item.code, item.description, true);
                    } else {
                        if (item.status == 'success') {
                            var b = 0;

                            $.each(item.description, function(j, item1) {
                                $.each(item.description[j], function(k, item2) {
                                    $.each(item.description[j][k], function(l, item3) {
                                        var classVar = 'alt';
                                        if (b % 2 == 0)
                                            classVar = '';

                                        var day = item.description[j][k][l]['date'].split("-");
                                        eleObj.row.add([
                                            day[2] + ' ' + eval("PayOne.config.mnth.M" + [day[1]]) + ' ' + day[0],
                                            item.description[j][k][l]['amount'],
                                            item.description[j][k][l]['income'],
                                            PayOne.config.vars.dI

                                        ]).draw();
                                        b++;
                                    });
                                });
                            });

                            var dt_fr, dt_to;
                            dt_fr = $("#dt_from").val();
                            dt_to = $("#dt_to").val();
                            $('#todayDate').html(dt_fr + ' to ' + dt_to);

                            $.each(item.today, function(j, item1) {
                                $.each(item.today[j], function(k, item2) {
                                    var day = item.today[j][k]['date'];
                                    $('#todaySale').html(item.today[j][k]['amount']);
                                    $('#todayEarning').html(item.today[j][k]['income']);
                                });

                            });
                            var ll = items_per_page * (page_no - 1) + 1; // lower limit
                            var ul = items_per_page * page_no;// upper limit                                                    
                        }
                    }
                });
            },
            error: function() {
                $('.searchProcess').button('reset');
            }
        });
    },
    renderTransHistoryData: function(dt1, dt2, items_per_page, page_no, eleBodyId, eleObj) {//(type,srvice,dt1,dt2){  
    	transDate1 = dt1.split("-").reverse().join("-");  
        transDate2 = dt2.split("-").reverse().join("-"); 
        if (!PayOne.core.utils.validateToFromDate(transDate1, transDate2, 31, true)) {
            return;
        }
        $('.searchProcess').button('loading');
        var date_from = dt1.split("-").join("");
        var date_to = dt2.split("-").join("");
        var dt = date_from + "-" + date_to;
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: "ledgerBalance",
                date: dt,
                page: 0,
                limit: 0,
                is_page_wise: false,
                device_type: "web"
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                $('.searchProcess').button('reset');
                //to clear all row of the table
                PayOne.core.utils.deleteAllRowDataTable(eleBodyId, eleObj);

                $.each(data, function(i, item) {
                    if (item.status == 'failure') {
                        PayOne.core.failChk(item.code, item.description, true);
                    } else {
                        if (item.status == 'success') {
                            var b = 0;

                            $.each(item.description, function(j, item1) {
                                $.each(item.description[0]["transactions"], function(k, item2) {

                                    var classVar = 'alt';
                                    if (b % 2 == 0)
                                        classVar = '';
                                    var txt = "";
                                    if (item2["opening_closing"]['opening'] != null) {
                                        if (item2["opening_closing"]['opening'] > item2["opening_closing"]['closing'])
                                            txt = "Dr";
                                        else
                                            txt = "Cr";
                                    }
                                    else {
                                        if (item2["transactions"]['credit'] > 0)
                                            txt = "Cr";
                                        else
                                            txt = "Dr";
                                    }
                                    crdr = item2["transactions"]['credit'] > 0 ? item2["transactions"]['credit'] : item2["transactions"]['debit'];
                                    crdr = crdr + " " + txt;

                                    if (item2["transactions"]["type"] == '4') {
                                    	if(item2["transactions"]["refid"] == '44')
                                    		name = item2["transactions"]["name"] + ' <img src="/images/icons/' + eval("PayOne.config.pcla.L52") + '">';
                                		else if(item2["transactions"]["refid"] == '65')
                                			name = item2["transactions"]["name"] + ' <img src="/images/icons/' + eval("PayOne.config.pcla.L53") + '">';
                                		else
                                			name = item2["transactions"]["name"] + ' <img src="/images/icons/' + eval("PayOne.config.pcla.L" + [item2["transactions"]["refid"]]) + '">';
                                        if (txt == "Cr")
                                            name += " - Reversal";
                                    }
                                    else
                                        name = item2["transactions"]["name"] + " (" + item2["transactions"]["refid"] + ")";
                                    
                                    eleObj.row.add(
                                            [
                                                item2["transactions"]["id"],
                                                name,
                                                crdr,
                                                (item2["opening_closing"]["opening"] == null ? "" : item2["opening_closing"]["opening"]),
                                                (item2["opening_closing"]["closing"] == null ? "" : item2["opening_closing"]["closing"]),
                                                item2["transactions"]["timestamp"]
                                            ]
                                            ).draw();
                                    b++;
                                });
                            });

                        }
                    }
                });
            },
            error: function() {
                $('.searchProcess').button('reset');
            }
        });
    },
    reversal : function (id){
    	var t = document.getElementById('tag_' + id);
    	var tag = t.options[t.selectedIndex].text;
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: 'reversal',
                id: id,
                tag: tag
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                $.each(data, function(i, item) {
                    if (item.status == 'failure') {
                        PayOne.core.failChk(item.code, item.description, true);//callLogin = true             
                    } else {
                        if (item.status == 'success') {
                        	var turnaround_time = item.turnaround_time;
                        	if(item.msg == ''){
                        		alert('Complaint Sent. Your complaint should resolve in ' + turnaround_time + ' Hrs');
                        	}
                        	else {
                        		alert(item.msg);
                        	}
                            $('#trSt' + id).html(PayOne.config.vars.rpI);
                            $('#tag_' + id).hide();
                        }
                    }
                });
            },
            error: function() {
            }
        });
    },
    renderTopUpData: function(date_from, date_to, callLogin, eleBodyId, eleObj) {
        $('.searchProcess').button('loading');
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: "getDistToRetlBalTransfer",
                service: 1,
                date_from: date_from,
                date_to: date_to,
                page_no: 0,
                items_per_page: 0
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                $('.searchProcess').button('reset');
                //to clear all row of the table
                PayOne.core.utils.deleteAllRowDataTable(eleBodyId, eleObj);

                var item = data[0];

                if (item.status == 'failure') {
                    PayOne.core.failChk(item.code, item.description, callLogin);
                } else {
                    if (item.status == 'success') {
                        $.each(item.data, function(j, item1) {
                            var item2 = item1;

                            var classVar = 'alt';
                            if (j % 2 == 0)
                                classVar = '';


                            eleObj.row.add([
                                item2.shop_transactions.id,
                                item2.shop_transactions.amount,
                                item2.shop_transactions.timestamp,
                                item2.distributors.company,
                                item2.opening_closing.opening,
                                item2.opening_closing.closing
                            ]).draw();
                        });
                    }
                }
            },
            error: function() {
                $('.searchProcess').button('reset');
            }
        });
    },
    renderDiscountData: function(service, callLogin, eleId) {
        $(eleId).html("");
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: "getCommissions",
                service: service
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                var htmlStr = '';
                $.each(data, function(i,item){
                    if(item.status == 'failure'){
                        failChk(item.code,item.description);                                
                    }else{
                        if(item.status == 'success'){
                            $.each(item.description, function(j,item1){
                                $.each(item1.R, function(k,item2){
                                    var classVar = 'alt';
                                    if(k%2 == 0)
                                        classVar = '';

                                  
                                    htmlStr += '<tr class="'+classVar+'"><td>'+item1.R[k]['0']['prodName']+'</td><td class="rightAlign">'+item1.R[k]['0']['prodPercent']+'%</td></tr>';
                                });
                            });
                        }
                    }
                });
                if(htmlStr == ''){
                    htmlStr += '<tr><td colspan="4">No Records Found</td></tr>';
                }
                $(eleId).html(htmlStr);
            },
            error: function(){}
        });
    },
    renderTransactionsData: function(dt1, dt2, service, callLogin, eleBodyId, eleObj) {
        $('.searchProcess').button('loading');
        if (!PayOne.core.utils.validateToFromDate(dt1, dt2, 31, true)) {
            return;
        }
        var htmlStr = "";
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: "lastTransactions",
                date: dt1,
                service: service,
                page: 0,
                date2: dt2,
                items_per_page: 0,
                is_page_wise: false
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                $('.searchProcess').button('reset');
                var tmp = '';
                var item = data[0];

                //to clear all row of the table
                PayOne.core.utils.deleteAllRowDataTable(eleBodyId, eleObj);

                if (item.status == 'failure') {
                    PayOne.core.failChk(item.code, item.description, callLogin);
                } else {
                    var rc = 0;
                    var className;

                    $.each(item.description, function(j, item1) {
                        $.each(item.description[j], function(k, item2) {

                            var col1 = '';
                            var col2 = '';
                            var col3 = '';
                            var col4 = '';
                            var col5 = '';
                            var col6 = '';
                            var status = '';
                            var pSt = '';
                            if (item.description[j][k]['vendors_activations']['prevStatus'] == '0') {
                                pSt = PayOne.config.vars.sI;
                            } else if (item.description[j][k]['vendors_activations']['prevStatus'] == '1') {
                                pSt = PayOne.config.vars.sI;
                            } else if (item.description[j][k]['vendors_activations']['prevStatus'] == '2') {
                                pSt = PayOne.config.vars.fI;
                            }
                            var complaintTags = "<select style='font-size:small' id='tag_" + item.description[j][k]['vendors_activations']['id'] + "'>" +
						                            "<option selected>Customer Not Got Balance</option>" +
						                    		"<option>Wrong Operator Recharge</option>" +
						                    		"<option>Wrong Sub ID Recharge</option>" +
						                    		"<option>Wrong Number Recharge</option>" +
						                    		"<option>Wrong Amount Recharge</option>" +
						                    		"<option>Late Recharge Success</option>" +
						                    		"<option>Wrong Benefit Recharge</option>" +
						                    		"<option>Double Recharge Success</option>" +
						                    	"</select>";	
                            if(item.description[j][k]['vendors_activations']['status'] == '0'){
                                status = '<span id="trSt'+item.description[j][k]['vendors_activations']['id']+'">'+PayOne.config.vars.sI+'<img onclick="PayOne.reports.reversal('+item.description[j][k]['vendors_activations']['id']+')" class="link" src="/images/icons/reversal.png"/></span>';
                            }else if(item.description[j][k]['vendors_activations']['status'] == '1'){
                                status = '<span id="trSt'+item.description[j][k]['vendors_activations']['id']+'">'+PayOne.config.vars.sI+'<img onclick="PayOne.reports.reversal('+item.description[j][k]['vendors_activations']['id']+')" class="link" src="/images/icons/reversal.png"/></span>';
                            }else if(item.description[j][k]['vendors_activations']['status'] == '2'){
//                                status = '<span id="trSt'+item.description[j][k]['vendors_activations']['id']+'">'+PayOne.config.vars.fI+'<img onclick="PayOne.reports.reversal('+item.description[j][k]['vendors_activations']['id']+')" class="link" src="/images/icons/reversal.png"/></span>';
                            	status = PayOne.config.vars.rsI;
                                complaintTags = "<span></span>";
                            }else if(item.description[j][k]['vendors_activations']['status'] == '3'){
                                status = pSt+PayOne.config.vars.rsI;
                                complaintTags = "<span></span>";
                            }else if(item.description[j][k]['vendors_activations']['status'] == '4'){
                                status = pSt+PayOne.config.vars.rpI;
                                complaintTags = "<span></span>";
                            }else if(item.description[j][k]['vendors_activations']['status'] == '5'){
                                status = pSt+PayOne.config.vars.rdI;
                                complaintTags = "<span></span>";
                            }

                            var opr = ''
                            if (service == '3') {
                                opr = item.description[j][k]['products']['name'];
                            } else {
                            	if(service == '5'){
                            		if(item.description[j][k]['vendors_activations']['product_id'] == '44')
                            			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L52") + '" />';
                            		else
                            			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L53") + '" />';
                            	}
                            	else
                                opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L" + item.description[j][k]['vendors_activations']['product_id']) + '">';//PayOne.config.vars.pcla[item.description[j][k]['vendors_activations']['product_id']]
                            }

                            col1 = opr;
                            if (service == '2') {
                                col2 = item.description[j][k]['vendors_activations']['param'];
                            }
                            else {
                            	col2 = item.description[j][k]['vendors_activations']['mobile'];
                            }
                            col3 = item.description[j][k]['vendors_activations']['amount'];
                            var d = item.description[j][k]['0']['timestamp'].split(" ");
                            var time = d[1].split(":");
                            col4 = d;
                            col5 = status;
                            col6 = item.description[j][k]['vendors_activations']['ref_code'];
                            var col7 = item.description[j][k]['opening_closing']['opening'];
                            var col8 = item.description[j][k]['opening_closing']['closing'];
                            eleObj.row.add([
                                col6,
                                col1,
                                col2,
                                col3,
                                col4,
                                col7,
                                col8,
                                col5,
                                complaintTags
                            ]).draw();

                        });
                    });
                }

            },
            error: function() {
                $('.searchProcess').button('reset');
            }
        });
    },
    renderTransStatus: function(pre, service, dt, callLogin, eleBodyId, eleObj) {
        $('.searchProcess').button('loading');
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: 'reversalTransactions',
                service: service,
                date: dt
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                // console.log(data);
                $('.searchProcess').button('reset');
                $('#' + pre + '-rev-tab-' + pre + '-l').html('');
                PayOne.core.utils.deleteAllRowDataTable(eleBodyId, eleObj);
                $.each(data, function(i, item) {
                    var tmp = '';
                    if (item.status == 'failure') {
                        PayOne.core.failChk(item.code, item.description, callLogin);
                    } else {

                        if (item.status == 'success') {
                            var rc = 0;
                            var htmlStr;
                            $.each(item.description, function(j, item1) {
                                $.each(item.description[0], function(k, item2) {
                                    rc++;
                                    tmp = 1;
                                    var col1 = '';
                                    var col2 = '';
                                    var col3 = '';
                                    var col4 = '';
                                    var col5 = '';
                                    var col6 = '';
                                    
                                    var dateEle = item.description[j][k]['0']['timestamp'].split(" ");
                                    var day = dateEle[0].split("-");
                                    var time = dateEle[1].split(":");
                                    var opr;

                                    if (service == '3') {

                                        opr = item.description[j][k]['products']['name'];
                                    } else { //PayOne.config.pcla
                                        //alert("PayOne.config.pcla.L"+item.description[j][k]['vendors_activations']['product_id']);
                                    	if(service == '5'){
                                    		if(item.description[j][k]['vendors_activations']['product_id'] == '44')
                                    			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L52") + '" />';
                                    		else
                                    			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L53") + '" />';
                                    	}
                                    	else
                                    	opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L" + item.description[j][k]['vendors_activations']['product_id']) + '">';
                                        //console.log(opr);
                                        //console.log("transaction");
                                    }

                                    var num = '';
                                    if (service == '2') {
                                        num = item.description[j][k]['vendors_activations']['param'];
                                    } else {
                                        num = item.description[j][k]['vendors_activations']['mobile'];
                                    }


                                    col1 = opr;//+' - ' + item.description[j][k]['products']['name']
                                    col2 = num;
                                    col3 = item.description[j][k]['vendors_activations']['amount'];
                                    col4 = time[0] + ':' + time[1];

                                    var status = '';
                                    if (item.description[j][k]['vendors_activations']['status'] == '3') {
                                        status = PayOne.config.vars.rsI;
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '4') {
                                        status = PayOne.config.vars.rpI;
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '5') {
                                        status = PayOne.config.vars.rdI;
                                    }

                                    col5 = status;
                                    col6 = item.description[j][k]['vendors_activations']['ref_code'];



                                    //to clear all row of the table
                                    eleObj.row.add([
                                        col6,
                                        col1,
                                        col2,
                                        col3,
                                        col4,
                                        col5
                                    ]).draw();
                                });
                            });
                            //earningPrevious
                            //alert();

                            $(".reportNavigationPrev").attr("onclick", item.prev == "" ? "return false;" : "PayOne.reports.renderTransStatus('" + pre + "'," + service + ",'" + item.prev + "',true,'#transStatusTableBody',transStatusTable);$('#transDate1').val('" + item.prev.toString().split("-").reverse().join("-") + "');return false;");
                            $(".reportNavigationNext").attr("onclick", item.next == "" ? "return false;" : "PayOne.reports.renderTransStatus('" + pre + "'," + service + ",'" + item.next + "',true,'#transStatusTableBody',transStatusTable);$('#transDate1').val('" + item.next.toString().split("-").reverse().join("-") + "');return false;");
                            if (item.prev == "") {
                                $(".reportNavigationPrev").removeClass("reportNavigationDisable");
                                $(".reportNavigationPrev").addClass("reportNavigationDisable");
                            } else {
                                $(".reportNavigation").removeClass("reportNavigationDisable");
                            }
                            if (item.next == "") {
                                $(".reportNavigationNext").removeClass("reportNavigationDisable");
                                $(".reportNavigationNext").addClass("reportNavigationDisable");
                            } else {
                                $(".reportNavigationNext").removeClass("reportNavigationDisable");
                            }
                        }
                    }


                });
            },
            error: function() {
                $('.searchProcess').reset();
            }
        });
    },
    lastTran: function(service, eleBodyId, eleObj) {

        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: "lastten",
                service: service
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {

                //to clear all row of the table
                PayOne.core.utils.deleteAllRowDataTable(eleBodyId, eleObj);

                $.each(data, function(i, item) {
                    if (item.status == 'failure') {
                        PayOne.core.failChk(item.code, item.description, true); //callLogin = true               
                    } else {
                        var tmp = '';
                        if (item.status == 'success') {
                            var b = 0;
                            $.each(item.description, function(j, item1) {
                                $.each(item.description[j], function(k, item2) {
                                    var classVar = 'alt';
                                    if (b % 2 == 0)
                                        classVar = '';

                                    var dateEle = item.description[j][k]['0']['timestamp'].split(" ");
                                    var day = dateEle[0].split("-");

                                    var time = dateEle[1].split(":");
                                    var complaintTags = "<select style='font-size:small' id='tag_" + item.description[j][k]['vendors_activations']['id'] + "'>" +
						                                    "<option selected>Customer Not Got Balance</option>" +
						                            		"<option>Wrong Operator Recharge</option>" +
						                            		"<option>Wrong Sub ID Recharge</option>" +
						                            		"<option>Wrong Number Recharge</option>" +
						                            		"<option>Wrong Amount Recharge</option>" +
						                            		"<option>Late Recharge Success</option>" +
						                            		"<option>Wrong Benefit Recharge</option>" +
						                            		"<option>Double Recharge Success</option>" +
						                            	"</select>";	
                                    var status = '';
                                    if (item.description[j][k]['vendors_activations']['status'] == '0') {
                                        status = '<span id="trSt' + item.description[j][k]['vendors_activations']['id'] + '">' + PayOne.config.vars.sI + ' <img onclick="PayOne.reports.reversal(' + item.description[j][k]['vendors_activations']['id'] + ')" class="link" src="/images/icons/reversal.png?1"/></span>';
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '1') {
                                        status = '<span id="trSt' + item.description[j][k]['vendors_activations']['id'] + '">' + PayOne.config.vars.sI + ' <img onclick="PayOne.reports.reversal(' + item.description[j][k]['vendors_activations']['id'] + ')" class="link" src="/images/icons/reversal.png?1"/></span>';
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '2') {
//                                        status = '<span id="trSt' + item.description[j][k]['vendors_activations']['id'] + '">' + PayOne.config.vars.fI + ' <img onclick="PayOne.reports.reversal(' + item.description[j][k]['vendors_activations']['id'] + ')" class="link" src="/images/icons/reversal.png?1"/></span>';
                                    	status = PayOne.config.vars.rsI;
                                        complaintTags = "<span></span>";
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '3') {
                                        status = PayOne.config.vars.rsI;
                                        complaintTags = "<span></span>";
                                    }else if(item.description[j][k]['vendors_activations']['status'] == '4'){
                                        status = PayOne.config.vars.rpI;
                                        complaintTags = "<span></span>";
                                    }else if(item.description[j][k]['vendors_activations']['status'] == '5'){
                                        status = PayOne.config.vars.rdI;
                                        complaintTags = "<span></span>";
                                    }
                                    b++;
                                    var opr = ''
                                    if (service == '3') {
                                        opr = item.description[j][k]['products']['name'];
                                    } else {
                                    	if(service == '5'){
                                    		if(item.description[j][k]['vendors_activations']['product_id'] == '44')
                                    			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L52") + '" />';
                                    		else
                                    			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L53") + '" />';
                                    	}
                                    	else 
                                    		opr = '<img src="/images/icons/' + (service == '4' ? (eval("PayOne.config.pcla.L" + (item.description[j][k]['vendors_activations']['product_id'] - 35))) : eval("PayOne.config.pcla.L" + item.description[j][k]['vendors_activations']['product_id'])) + '">';
                                    }

                                    var num = '';
                                    if (service == '2') {
                                        num = item.description[j][k]['vendors_activations']['param'];
                                    } else {
                                        num = item.description[j][k]['vendors_activations']['mobile'];
                                    }
                                    
                                    eleObj.row.add( [
                                        item.description[j][k]['vendors_activations']['ref_code'],
                                        opr,
                                        num,
                                        item.description[j][k]['vendors_activations']['amount'],
                                        day[2] + ' ' + eval("PayOne.config.mnth.M" + day[1]) + ' - ' + time[0] + ':' + time[1],
                                        status,
                                        complaintTags
                                    ]).draw();
                                });
                            });
                        }


                    }
                });

            },
            error: function() {
            }
        });
    },
    lastTranSearchByNo: function(service, searchId, eleBodyId, eleObj) {
        $(".searchProcess").button("loading");
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {
                method: "mobileTransactions",
                service: service,
                mobile: searchId
            },
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                //to clear all row of the table
                $(".searchProcess").button("reset");
                PayOne.core.utils.deleteAllRowDataTable(eleBodyId, eleObj);
                $.each(data, function(i, item) {
                    if (item.status == 'failure') {
                        PayOne.core.failChk(item.code, item.description, true); //callLogin = true               
                    } else {
                        var tmp = '';
                        if (item.status == 'success') {
                            var b = 0;
                            $.each(item.description, function(j, item1) {
                                $.each(item.description[j], function(k, item2) {
                                    var classVar = 'alt';
                                    if (b % 2 == 0)
                                        classVar = '';
                                    var dateEle = item.description[j][k]['0']['timestamp'].split(" ");
                                    var day = dateEle[0].split("-");

                                    var time = dateEle[1].split(":");
                                    var complaintTags = "<select style='font-size:small' id='tag_" + item.description[j][k]['vendors_activations']['id'] + "'>" +
						                                    "<option selected>Customer Not Got Balance</option>" +
						                            		"<option>Wrong Operator Recharge</option>" +
						                            		"<option>Wrong Sub ID Recharge</option>" +
						                            		"<option>Wrong Number Recharge</option>" +
						                            		"<option>Wrong Amount Recharge</option>" +
						                            		"<option>Late Recharge Success</option>" +
						                            		"<option>Wrong Benefit Recharge</option>" +
						                            		"<option>Double Recharge Success</option>" +
						                            	"</select>";	
                                    var status = '';
                                    if (item.description[j][k]['vendors_activations']['status'] == '0') {
                                        status = '<span id="trSt' + item.description[j][k]['vendors_activations']['id'] + '">' + PayOne.config.vars.sI + '<img onclick="PayOne.reports.reversal(' + item.description[j][k]['vendors_activations']['id'] + ')" class="link" src="/images/icons/reversal.png"/></span>';
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '1') {
                                        status = '<span id="trSt' + item.description[j][k]['vendors_activations']['id'] + '">' + PayOne.config.vars.sI + '<img onclick="PayOne.reports.reversal(' + item.description[j][k]['vendors_activations']['id'] + ')" class="link" src="/images/icons/reversal.png"/></span>';
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '2') {
//                                        status = '<span id="trSt' + item.description[j][k]['vendors_activations']['id'] + '">' + PayOne.config.vars.fI + '<img onclick="PayOne.reports.reversal(' + item.description[j][k]['vendors_activations']['id'] + ')" class="link" src="/images/icons/reversal.png"/></span>';
                                    	 status = PayOne.config.vars.rsI;
                                         complaintTags = "<span></span>";
                                    } else if (item.description[j][k]['vendors_activations']['status'] == '3') {
                                        status = PayOne.config.vars.rsI;
                                        complaintTags = "<span></span>";
                                    }else if(item.description[j][k]['vendors_activations']['status'] == '4'){
                                        status = PayOne.config.vars.rpI;
                                        complaintTags = "<span></span>";
                                    }else if(item.description[j][k]['vendors_activations']['status'] == '5'){
                                        status = PayOne.config.vars.rdI;
                                        complaintTags = "<span></span>";
                                    }
                                    b++;
                                    var opr = ''
                                    if (service == '3') {
                                        opr = item.description[j][k]['products']['name'];
                                    } else {
                                    	if(service == '5'){
                                    		if(item.description[j][k]['vendors_activations']['product_id'] == '44')
                                    			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L52") + '" />';
                                    		else
                                    			opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L53") + '" />';
                                    	}
                                    	else
                                        opr = '<img src="/images/icons/' + eval("PayOne.config.pcla.L" + item.description[j][k]['vendors_activations']['product_id']) + '">';
                                    }

                                    var num = '';
                                    if (service == '2') {
                                        num = item.description[j][k]['vendors_activations']['param'];
                                    } else {
                                        num = item.description[j][k]['vendors_activations']['mobile'];
                                    }

                                    eleObj.row.add([
                                        item.description[j][k]['vendors_activations']['ref_code'],
                                        opr,
                                        num,
                                        item.description[j][k]['vendors_activations']['amount'],
                                        day[2] + ' ' + eval("PayOne.config.mnth.M" + day[1]) + ' - ' + time[0] + ':' + time[1],
                                        status,
                                        complaintTags
                                    ]).draw();
                                });
                            });
                        }
                    }
                });
            },
            error: function() {
                $(".searchProcess").button("reset");
            }
        });
    },
    showSalesRep: function(obj, id1, dt) {
        showHide(obj, id1);
        var dateRange = '';

        var dtA = dt.split("-");
        dateRange = dtA[2] + '' + dtA[1] + '' + dtA[0] + '-' + dtA[2] + '' + dtA[1] + '' + dtA[0];

        //$('#sales-report').html(loader);
        $.ajax({
            url: PayOne.config.urls.api,
            type: "POST",
            data: {method: "saleReport", date: dateRange},
            dataType: 'jsonp',
            jsonp: 'root',
            timeout: 50000,
            success: function(data, status) {
                var rec = '';
                $.each(data, function(i, item) {
                    if (item.status == 'failure') {
                        failChk(item.code, item.description);
                    } else {
                        $.each(item.description, function(j, item1) {
                            $.each(item.description[j], function(k, item2) {
                                var tmp = '';
                                rec += '</br><table cellpadding="0" cellspacing="0" class="data1"><tbody><tr><th width="55%">Product</th><th class="rightAlign" width="15%">Transactions</th><th class="rightAlign" width="15%">Total</th><th class="rightAlign" width="15%">Total Income</th></tr>';
                                var tra = 0;
                                var tot = 0;
                                var inc = 0;
                                $.each(item2.data, function(l, item3) {
                                    tra = parseFloat(tra) + parseFloat(item2.data[l][0]['counts']);
                                    tot = parseFloat(tot) + parseFloat(item2.data[l][0]['amount']);
                                    inc = parseFloat(inc) + parseFloat(item2.data[l][0]['income']);

                                    var classVar = 'alt';
                                    if (l % 2 == 0)
                                        classVar = '';

                                    tmp += '<tr class="' + classVar + '"><td>' + item3.products['name'] + '</td><td class="rightAlign">' + item2.data[l][0]['counts'] + '</td><td class="rightAlign">' + item2.data[l][0]['amount'] + '</td><td class="rightAlign">' + item2.data[l][0]['income'] + '</td></tr>';
                                });

                                if (tmp == '') {
                                    tmp += '<tr><td colspan="4">No Records Found</td></tr>';
                                } else {
                                    tmp += '<tr class=""><td><b>Total</b></td><td class="rightAlign"><b>' + tra + '</b></td><td class="rightAlign"><b>' + tot.toFixed(2) + '</b></td><td class="rightAlign"><b>' + inc.toFixed(2) + '</b></td></tr>';
                                }
                                rec += tmp + '</tbody></table>';


                            });
                        });
                    }
                });
                if (rec == '')
                    rec = '<table cellpadding="0" cellspacing="0" class="data1"><tbody><tr><th>Product</th><th class="rightAlign">Transactions</th><th class="rightAlign">Total</th><th class="rightAlign">Total Income</th></tr><tr><td colspan="4">No Records Found</td></tr>';

                $("#sales-report").html(rec);
            },
            error: function() {
            }
        });
    }
}
$(function() {
    PayOne.core.init();
});


