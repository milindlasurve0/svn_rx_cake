//localStorage.clear();
var PayOne = (typeof PayOne != "undefined") ?  PayOne : {};
//var PayOne = { };

//Pravin
PayOne.core = {
    init:function(){
        
        PayOne.core.callCheckSession();        
        //@TODO initialization of page
        PayOne.core.callUpdateBal();
        PayOne.core.callChkUpdate();
        PayOne.auth.login.init();        
      //  PayOne.core.showNotify(PayOne.core.cookie.get("mobile"));//create and connect to notification soccket
        PayOne.core.setNotification();
        PayOne.core.showchat(PayOne.core.cookie.get("mobile"));
        
        $(".open_chat_box").on("click",function(e){
            e.preventDefault();
            if(PayOne.core.cookie.get("mobile") == "" || PayOne.core.cookie.get("mobile") == null){
                alert("Please login , to use chat functinality .");return false;
            }            
            chatwindow=window.open("http://dev.pay1.in:7070/webchat/queue.jsp?chatID=webchat_"+PayOne.core.cookie.get("mobile")+"&workgroup=b2bchat@workgroup.dev.pay1.in&username="+PayOne.core.cookie.get("mobile"),'name','height=300,width=300, resizable=no,scrollbars=yes,location=no,toolbar=no,addressbar=no');
            if (window.focus) {chatwindow.focus()}
            return false;
        });
        if(window.location.hash == "#fromUpdatePin"){
            $('#login').modal('show');
        }
        $("#login_modal_submit").button('reset');
        $("#otp_modal_submit").button('reset');
        PayOne.core.registerRefreshBalance();
        
    },
    registerRefreshBalance:function(){
        $(".balRefresh").on("click",function(e){
            e.preventDefault();
            PayOne.core.refreshBalance()
            return false;
        });
    },
    refreshBalance:function(){
        $.ajax({
		url: PayOne.config.urls.api,
		type: "POST",
		data: {method:'updateBal'},
		dataType: 'jsonp',
		jsonp: 'root',
		timeout: 50000,
		success: function(data, status){ 
                    //console.log(data);
                        $.each(data, function(i,item){
				if(item.status == 'failure'){
                                    PayOne.core.failChk(item.code,item.description);
				}else{
					if(item.status == 'success'){
						var htmlStr;
                                                if(item.login == '1'){                   $('#top_banner').hide();                                     
                                                        PayOne.core.cookie.set("balance", item.description, 1); 
                                                       // console.log(PayOne.core.cookie.get("balance"));
                                                        //console.log(PayOne.core.cookie.get("shopname"));
							htmlStr  =    "<a class=\"pull-left\" href=\"#\"><img class=\"media-object user-img\" src=\"/img/ic_loggedin.png\" alt=\"...\"></a>"+
                                                                       "<div class=\"media-body\">"+
                                                                       "<p class=\"userName top\" style=\"color:black;\">"+PayOne.core.cookie.get("shopname")+"</p>"+
                                                                       "<p class=\"balance\" style=\"border: 1px solid #503c97;background:none repeat scroll 0 0 #503c97;\">"+item.description+"</p>"+
                                                                      // "<span title=\"Refresh Balance\" class=\"glyphicon glyphicon-refresh balRefresh\" style=\"border: 1px solid #503c97;background:none repeat scroll 0 0 #503c97;cursor: pointer;\"></span>"+
                                                                       "</div>";
                                                        $("#media_header_section").html(htmlStr);
                                                        //PayOne.core.registerRefreshBalance();
                                                        $("#refreshBalLi").show();
                                                        $("#signUpActionLi").hide();						
						}else{
                                                    $("#logoutActionLi").hide();
                                                    $("#refreshBalLi").hide();
                                                    
                                                    PayOne.core.cookie.clear();
                                                    //window.location.reload();
                                                }
                                        }	
				}	
			});
		},
		error: function(){
                    //PayOne.core.callUpdateBal();
                    //PayOne.core.cookie.clear();
               }
        });
    },
    setNotification:function(){
                    $("#notification_right_side").html("<li  class=\"notification gray\" style=\"width:100%;\"><p>You will find all your notification here .</p></li>");
                    var localMsg = localStorage.getItem("notifications");
                    var localMsgCount = localStorage.getItem("notifications_count");
                    if(! localMsg || localMsg == ""  ){
                        return;
                    }
                    
                    var arr = localMsg.split("|<>|");
                    var str = "";
                    $.each(arr,function(i,v){
                        var mbody = null;
                        try{
                            var mbody = $.parseJSON(v);
                            mbody = $.parseJSON(mbody[1]);
                        }catch(e){
                            return;
                        }
                       
                        str = str + "<li class=\"notification gray\" style=\"width:100%;\">"+
                                        "<p>"+mbody.msg+"</p>"+
                                        "<span>"+mbody.timestamp+"</span>"+
                                    "</li>";                        
                    });
                    
                    if(localMsgCount == 0 || isNaN(localMsgCount)){
                        localMsgCount = "";
                    }
                   
                    $("#notifications_count").html(localMsgCount);//localMsgCount
                        
                    $("#show_notifications").on("click",function(event){
                        $("#notifications_count").html("");
                        localStorage.setItem("notifications_count","0")
                    });
                                       
                    $("#notification_right_side").html(str);
    },
    failChk:function(code,desc,callLogin){ 
        
        if(code == '403'){
            if(callLogin){      
                $('#login').modal('show');
            }
            return; 
        }else if(code == '35'){
            return; 
        }else if(code == '404'){
            desc = 'Invalid Access';
            $('#failed_description').html(desc);
            $('#failed').modal("show");
            return; 
        }else if(code == '30' || code == '2' || code == '28' || code == '4' || code == '5' || code == '6' || code == '7' || code == '32' || code == '8' || code == '9' || code == '29' || code == '33' || code == '14' || code == '31'){
            $('#failed_description').html(desc);
            $('#failed').modal("show");
            return; 
        }else{
            $('#failed_description').html(desc);
            $('#failed').modal("show");
            return;
        }
    },
    //display OTP models for User Device Mapping if User Device is Not Mapped
    otpChk:function(code,desc,callLogin){
        //set Timeout for showing Resend OTP button for User device mapping
        setTimeout(function(){ 
            document.getElementById('ResendOtp_modal_submit').style.visibility='visible'; // show Resend Button
        }, 10000);
        if(code == '403'){
            if(callLogin){      
                $('#login').modal('show');
            }
            return; 
        }else if(code == '35'){
            return; 
        }else if(code == '404'){
            desc = 'Invalid Access';
            $('#otp_description').html(desc);
            $('#otp').modal("show");
            return; 
        }else if(code == '1010'){
            $('#otp_description').html(desc);
            $('#login').modal('hide');
            $('#otp').modal("show");
            return; 
        }else{
            $('#otp_description').html(desc);
            $('#login').modal('hide');
            $('#otp').modal("show");
            return;
        }
    },
    //display Failed OTP models for User Device Mapping if OTP is not verified
    VerifyOTPfailChk:function(code,desc,callLogin){ 
        if(code == '403'){
            if(callLogin){      
                $('#login').modal('show');
            }
            return; 
        }else if(code == '35'){
            return; 
        }else if(code == '404'){
            desc = 'Invalid Access';
            $('#verify_otp_description').html(desc);
            $('#verify_otp').modal("show");
            return false; 
        }else if(code == 'E027'){
            $('#verify_otp_description').html(desc);
            $('#verify_otp').modal("show");
            return false; 
        }else{
            $('#verify_otp_description').html(desc);
            $('#verify_otp').modal("show");
            return false;
        }
    },
    gMapErrorCallBack : function(error){
        //alert("HEllo");
        //function showError(error) {
        PayOne.auth.login.submit(0,0);
        /*switch(error.code) {
            case error.PERMISSION_DENIED:
                //x.innerHTML = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                //x.innerHTML = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                //x.innerHTML = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                //x.innerHTML = "An unknown error occurred."
                break;
        }*/
        //}
    },
    gMapCallBack : function(position){        
        PayOne.core.cookie.set("lat", position.coords.latitude, 1);
        PayOne.core.cookie.set("lng", position.coords.longitude, 1); 
        PayOne.auth.login.submit(position.coords.latitude,position.coords.longitude);
    },
    callCheckSession:function(){
    },
    callUpdateBal:function(){    
        var htmlStr;
        if( PayOne.core.cookie.get("mobile")==null || PayOne.core.cookie.get("mobile")==""){
            $("#logoutActionLi").hide();
        }else{           
            //PayOne.core.cookie.set("balance", data.description, 1);
        	$('#top_banner').hide();
            htmlStr  = "<a class=\"pull-left\" href=\"#\"><img class=\"media-object user-img\" src=\"/img/ic_loggedin.png\" alt=\"...\"></a>"+
                           "<div class=\"media-body\">"+
                           "<p class=\"userName top\" style=\"color:black;\">"+PayOne.core.cookie.get("shopname")+"</p>"+
                           "<p class=\"balance\">"+PayOne.core.cookie.get("balance")+"</p>"+
                           //"<span title=\"Refresh Balance\" class=\"glyphicon glyphicon-refresh balRefresh\" style=\"border: 1px solid #503c97;background:none repeat scroll 0 0 #503c97;cursor: pointer;\"></span>"+
                           "</div>";
            $("#media_header_section").html(htmlStr);
            $("#signUpActionLi").hide(); 
            $("#refreshBalLi").show();
        }
//        /*$.ajax({
//		url: PayOne.config.urls.api,
//		type: "POST",
//		data: {method:'updateBal'},
//		dataType: 'jsonp',
//		jsonp: 'root',
//		timeout: 50000,
//		success: function(data, status){
//                        var htmlStr;
//                        if( PayOne.core.cookie.get("mobile")==null || PayOne.core.cookie.get("mobile")==""){
//                            $("#logoutActionLi").hide();
//                        }else{           
//                            PayOne.core.cookie.set("balance", data.description, 1);
//                            htmlStr  = "<a class=\"pull-left\" href=\"#\"><img class=\"media-object user-img\" src=\"/images/user.jpg\" alt=\"...\"></a>"+
//                                           "<div class=\"media-body\">"+
//                                           "<p class=\"userName top\">"+PayOne.core.cookie.get("shopname")+"</p>"+
//                                           "<p class=\"balance\">"+PayOne.core.cookie.get("balance")+"</p>"+
//                                           "</div>";
//                            $("#media_header_section").html(htmlStr);
//                            $("#signUpActionLi").hide(); 
//                        }
//                        $.each(data, function(i,item){
//				if(item.status == 'failure'){
//                                    PayOne.core.failChk(item.code,item.description);
//				}else{
//					if(item.status == 'success'){
//						var htmlStr;
//                                                if(item.login == '1'){                                                        
//                                                        PayOne.core.cookie.set("balance", item.description, 1);                                                        
//							htmlStr  =    "<a class=\"pull-left\" href=\"#\"><img class=\"media-object user-img\" src=\"/images/user.jpg\" alt=\"...\"></a>"+
//                                                                       "<div class=\"media-body\">"+
//                                                                       "<p class=\"userName top\">"+PayOne.core.cookie.get("shopname")+"</p>"+
//                                                                       "<p class=\"balance\">"+item.description+"</p>"+
//                                                                       "</div>";
//                                                        $("#media_header_section").html(htmlStr);
//                                                        $("#signUpActionLi").hide();						
//						}else{
//                                                    $("#logoutActionLi").hide();
//                                                    PayOne.core.cookie.clear();
//                                                }
//                                        }	
//				}	
//			});
//		},
//		error: function(){
//                    //PayOne.core.callUpdateBal();
//                    //PayOne.core.cookie.clear();
//                }
//	});
       
    },
    showchat :function (mobileNo){ 
             
        $(".chat_toggle").on("click",function(event){           
                newwindow=window.open("http://dev.pay1.in:7070/webchat/queue.jsp?chatID=webchat_"+mobileNo+"&workgroup=b2bchat@workgroup.dev.pay1.in&username="+mobileNo,'name','height=300,width=300, resizable=no,scrollbars=yes,location=no,toolbar=no,addressbar=no');
                if (window.focus) {newwindow.focus()}
                return false;           
        });
        
        /*var profileDetails = getCookieData("profileDetails"); 
        var profileDetails = profileDetails.split("|<>|");
        var mobileNo = profileDetails[0];
        //alert(getCookieData("loginMobile"));
        pp = window.open("http://dev.pay1.in:7070/webchat/queue.jsp?chatID=webchat_"+mobileNo+"&workgroup=b2bchat@workgroup.dev.pay1.in&username="+mobileNo,"MsgWindow","width=520,height=300,left=850,top=750");
        pp.blur();
        window.focus();*/
    },
//    showNotify : function (id){ 
//            if(typeof(id)==='undefined') id = '';
//            if ("WebSocket" in window)
//            { 
//                var ws = new WebSocket("ws://smstadka.com:5000/"+id);
//                ws.onopen = function(){ 
//                    var chatDisplay = $('div.chatDiv').html(); 
//                    var notificationDisplay = '<span onclick="listNotification(\'page1\',\'notification\')">Notifications</span>';
//                    $('div.chatDiv').html(notificationDisplay+" | "+chatDisplay);
//                };
//                ws.onmessage = function (evt){ 
//                    var received_msg = evt.data;
//                    received_msg = received_msg.replace('\\n', '<br />');
//                    var localMsg = localStorage.getItem("notifications");
//                    var localMsgCount = localStorage.getItem("notifications_count");
//                    if(localMsg){//if stored value is not empty then
//                        localMsg = received_msg+"|<>|" + localMsg ;
//                        if(isNaN(parseInt(localMsgCount))){
//                            localMsgCount = 0;
//                        }
//                        localMsgCount = parseInt(localMsgCount) + 1;
//                    }else{//if stored value is empty
//                        localMsg = received_msg;
//                        localMsgCount = 1;
//                    }
//                    
//                    localStorage.setItem("notifications", localMsg);
//                    localStorage.setItem("notifications_count", localMsgCount);
//                    
//                    var mbody = $.parseJSON(received_msg);
//                        mbody = $.parseJSON(mbody[1]);
//                        
//                    var str = "<li class=\"notification gray\" style=\"width:100%;\">"+
//                                    "<p>"+mbody.msg+"</p>"+
//                                    "<span>"+mbody.timestamp+"</span>"+
//                              "</li>";
//                    var notificationHtml = $("#notification_right_side").html();
//                    if(notificationHtml == "<li  class=\"notification gray\" style=\"width:100%;\"><p>You will find all your notifications here .</p></li>"){
//                        $("#notification_right_side").html("");
//                    }
//                    $("#notification_right_side").prepend(str);
//                    
//                    
//                    if(localMsgCount == 0 || isNaN(localMsgCount)){
//                        localMsgCount = "";
//                    }
//                    $("#notifications_count").html(localMsgCount);
//                    
//                };
//                ws.onclose = function(){ 
//                    // websocket is closed.
//                    var chatDisplay = $('div.chatDiv').html();
//                    var notificationDisplay = '<span id="notification">Notifications</span>';
//                    $('div.chatDiv').html(notificationDisplay+" | "+chatDisplay);
//                     
//                };
//            }else {
//                alert("Please upgrade your Browser to receive Live notification");
//            }
//        },
    callChkUpdate:function(){
        /* $.ajax({
		url: PayOne.config.urls.api,
		type: "POST",
		data: {method:'chkUpdate',version:PayOne.config.version},
		dataType: 'jsonp',
		jsonp: 'root',
		timeout: 50000,
		success: function(data, status){
			$.each(data, function(i,item){
				if(item.status == 'success'){
					showHide('page1','verControl');
					$('#updateMsg').html(item.description);
				}
			});
		},
		error: function(){}
	});*/
    },
    
    initBrowsePlans:function(service){
         $('.checkPlan').on('click', function (event) {
             event.preventDefault();
             $(".checkPlanForm").toggle("slow");
         });
         $('#circle').on('change', function (event) {
             event.preventDefault();        
             //call plan api
             $("#mobilePlans").html("");//<option>Loding...</option>
             $("#planCatagory").html("<option>Loading...</option>");//
             //mobilePlans
             PayOne.core.callGetPlans($("#operatorId").val(),this.value);
         });
         $('#planCatagory').on('change', function (event) {
             event.preventDefault();
             var value = this.value;
             PayOne.core.planDetails = [];
             var plansHtml = "";
             //call plan api
             $.each(PayOne.core.selectedPlans, function(keyPlansInd, plansObj) {
                 if(keyPlansInd == value){ 
                    var i = 0;
                    $.each(plansObj, function(keyPlansTitle, plansDetails) {
                        PayOne.core.planDetails.push(plansDetails);
                        plansHtml += "<option value = \""+i+"\">"+"Rs. "+plansDetails.plan_amt +"  "+"( "+plansDetails.plan_validity+" )" +"</option>";
                        i++;
                    });
                    $("#mobilePlans").html(plansHtml);
                 }

            });
         });
         //
         $('#mobilePlans').on('change', function (event) {  
             event.preventDefault();
             //call plan api
             $("#selectedPlanDesc").html(PayOne.core.planDetails[this.value].plan_desc);
             $("#rechargeAmt").val(PayOne.core.planDetails[this.value].plan_amt);
         });
    },
    mobileDetails : {},
    selectedPlans:[],
    planDetails:[],
    callGetPlans : function (operator,circle){
        
         $.ajax({
                url  : PayOne.config.urls.api,//"http://dev.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json"
                type : "POST",
                data : "method=getPlanDetails&operator="+operator+"&circle="+circle, 
                dataType : 'jsonp',
                jsonp  : 'root',
                timeout: 50000,
                success: function(data,status){
                            $("#planCatagory").html("");
                            var plansType="";
                            $.each(data, function(key, value) {
                                var plans = value[operator]["circles"][circle]["plans"];
                                PayOne.core.selectedPlans = plans;                                
                                $.each(plans, function(keyPlans, plansHeading) { 
                                    plansType += "<option value = \""+keyPlans+"\">"+keyPlans+"</option>"; 
                                }); 

                            });                            
                            $("#planCatagory").html(plansType);

              },error:function(){
                  $("#planCatagory").html("");//
              }
            });
     },
          checkplan: function(plandetails,operator,circletype,operatortype){
                    var plandetails = "getPlanDetails";
                    var operatorid = operator;
                    var circle = circletype;
                    var html = "";
                    var associativeArray = new Array();
                    var plantype = new Array();
                    $.ajax({
                       url: PayOne.config.urls.api,
                       type:"POST",
                       dataType : 'jsonp',
                       jsonp  : 'root',
                       timeout: 50000,
                       data:{
                             method:plandetails,
                             operator:operatorid,
                             circle:circle
                            },
                  success:function(data)
                  {
                     $.each(data, function( key, value) {
                     $.each(value, function(k,v){
                    if(typeof v == "object")
                    {
                          $.each(v.circles, function(pkey,pval){
                              i = 0;
                              $.each(pval.plans, function(a,b){
                                  if(a!="None"){
                                  var classname = (i==0)?"active":"";
                                  if(a=='Data/2G'){
                                      a = '2G'
                                  }
                                  html+='<li class='+classname+'><a href=#'+a+' role="tab" data-toggle="tab">'+a+'</a></li>';
                                  }
                                  var newElement = {};
                                  newElement[a] = b;
                                  associativeArray.push(newElement);
                                  plantype.push(a);
                                  i++;
                              });

                          });
                      }

                     });

                      });

               $("#"+operatortype+"_plan").append(html);
               var htmlbody = '';
               var htmlcontent = '';
               var tabclass = '';
               j = 0;
              $.each(plantype,function(plankey,planval){
                  if(planval!="None"){
                 var tabclass = (j== 0)? "tab-pane active scroll":"tab-pane scroll";
               htmlcontent +='<div class="'+tabclass+'"  height="100" width="300" id='+planval+'><div class=""><table class="table table-hover"><thead><tr><th>Price</th><th>Validity</th><th>Description</th></tr></thead><tbody>'
               $.each(associativeArray,function(k,v){
                    $.each(v,function(key,val){
                        if(key == planval ){ 
                             $.each(val,function(datakey,dataval){ 
                                 var planamt = "\'" + dataval.plan_amt + "\'";
                                 var planvalidity = "\'"+dataval.plan_validity +"\'";
                                 var plandesc = "\'"+dataval.plan_desc+"\'";
                                 var operator = "\'"+operatortype+"\'";
                                 htmlcontent+='<tr onclick="getPlandescription('+planamt+','+operator+','+plandesc+')"><td>'+dataval.plan_amt+'</td><td>'+dataval.plan_validity+'</td><td>'+dataval.plan_desc+'</td></tr>'
                                 });
                                 htmlcontent+='</tbody></table></div></div>';
                                $("#"+operatortype+"_plandetails").append(htmlcontent);
                                 htmlcontent = '';
                        }
                     });
                    });
                     j++;
                     }
                     else
                     {
                      htmlcontent+="<table><tr><td colspan='3' class='tab-content mT20' width='670' align='center'><b>No Plans Found</b></td></tr></table>";
                      $("#"+operatortype+"_plandetails").append(htmlcontent);
                     }
                    
                  });
              
            },error:function(xhr, error) {
            }
         
        });
                    
        },
    utils: {
                deleteAllRowDataTable:function(tableBodyId , dataTableObj){
                    
                    $(tableBodyId).find("tr").each(function(){
                        $(this).addClass('selected');
                        dataTableObj.row('.selected').remove().draw( false );
                    });
                },
		str_replace: function(search, replace, subject, count){
			var i = 0, j = 0, temp = '', repl = '', sl = 0, fl = 0,
			f = [].concat(search),
			r = [].concat(replace),
			s = subject,
			ra = r instanceof Array, sa = s instanceof Array;
			s = [].concat(s);
			if (count)
			{
				this.window[count] = 0;
			}
			for (i = 0, sl = s.length; i < sl; i++)
			{
				if (s[i] === '')
				{
					continue;
				}
				for (j = 0, fl = f.length; j < fl; j++)
				{
					temp = s[i] + '';
					repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
					s[i] = (temp).split(f[j]).join(repl);
					if (count && s[i] !== temp)
					{
						this.window[count] += (temp.length - s[i].length) / f[j].length;
					}
				}
			}
			return sa ? s : s[0];
		},
                validateToFromDate:function(dt1,dt2,noOfDays , noOfDaysFlag){
                    //--- validate data -----
                    var arr = dt1.split("-");
                    //arr.reverse();
                    //var dt1Rev = arr.join("-");
                   // alert(dt1Rev);
                   /// alert(arr);
                    
                    var dt1RevObj = new Date(arr[0], arr[1], arr[2]);
                    
                    var arr1 = dt2.split("-");
                    //arr.reverse();
                    //var dt2Rev = arr1.join("-");
                    var dt2RevObj = new Date(arr1[0], arr1[1], arr1[2]);

                    var x ;
                    x = Date.daysBetween(dt1RevObj,dt2RevObj);
                    x = x+1;
                  //  alert(x);
                    if(dt1RevObj > dt2RevObj){
                        alert("From date can not be greater than To date !");
                        return false;
                    }
                    if(noOfDaysFlag == true && x > noOfDays){
                        alert("You can not get more than 1 month data !");
                        return false;
                    }
                    
                    return true;
                    //-------------------------
                },
                mobileValidate : function (y){
                    var y = y.trim();
                    if(y == ""){alert("Enter mobile number");return 0;
                    }else if(isNaN(y)||y.indexOf(" ")!= -1){alert("Mobile number should contain numeric values");return 0;
                    }else if(y.length != 10){alert("Mobile number should be a 10 digit number");return 0;
                    }
                    return 1;
                },

                amtValidate : function (y) {
                    var err = '';
                    var y = y.trim();
                    if(y == ""){alert("Plese enter proper amount");return 0;
                    }else if(isNaN(y) || y.indexOf(" ")!=-1){alert("Plese enter proper amount");return 0;
                    }
                    return 1;
                }
           
	}
}
PayOne.core.cookie = {
	// Create a new or update a current cookie
	// @param STRING name - The name of the cookie
	// @param STRING value - The value of the cookie
	// @param STRING days - The number of days the cookie should last
	set: function(name, value, days)
	{
		var date,expires;
		if (days)
		{
			date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = '; expires=' + date.toGMTString();
		}
		else {
			expires = ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
		}
                
              //  console.log(days);
               // console.log(expires);
               
		//document.cookie = name + '=' + value + expires +';';
                
                //document.cookie = name + '=' + value + expires + '; ';//domain='+cookieDomain ;path=' + PayOne.config.cookiePath+'
                 document.cookie = name + '=' + value + expires + ';domain='+cookieDomain+';path=' + PayOne.config.cookiePath;
                 //console.log(document.cookie);
	}
	,
	// Read a cookie by its name
	// @param STRING name - The name of the cookie to read
	get: function(name)
	{
		var nameEQ = name + '=',
		cookies = document.cookie.split(';'),cookie,i;
                
		for (i = 0; i < cookies.length; i += 1)
		{                        
                        
			cookie = cookies[i];
			// If the first character is a space, remove it
			while (cookie.charAt(0) === ' ')
			{
				cookie = cookie.substring(1, cookie.length);
			}
			if (cookie.indexOf(nameEQ) === 0)
			{
				return cookie.substring(nameEQ.length, cookie.length);
			}
		}
		return null;
	}
	,
	// Erase a cookie by setting its expiration date to a time in the past
	// @param STRING name - The name of the cookie
	unset: function(name)
	{      
            this.set(name, '', -1);    	
	},
        
        clear: function()
	{     
            var cookies = document.cookie.split(";");
            
           // console.log(cookies);
           // return false;
             
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i];
                var eqPos = cookie.indexOf("=");
                var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
   
                if(name !== " uuid"){
                   PayOne.core.cookie.set(name, '', -1);
                }
               // PayOne.core.cookie.set('.pay1.in', '', -1);
            }   
	}
};
Date.daysBetween = function( date1, date2 ) {
   // alert(date1);
    //alert(date2);
      //Get 1 day in milliseconds
      var one_day=1000*60*60*24;

      // Convert both dates to milliseconds
      var date1_ms = date1.getTime();
      var date2_ms = date2.getTime();

      // Calculate the difference in milliseconds
      var difference_ms = date2_ms - date1_ms;
      
      // Convert back to days and return
      return Math.round(difference_ms/one_day);
}
$(function(){
   // PayOne.core.init();
});

/*
"method" = "pay1Wallet"
"mobileNumber"
"amount"
"timestamp"
"profile_id"
"hash_code"
"device_type" = "web"
                       
                       
"method"="addLeads"
"full_name"
"email"
"contact_no"
"city"
"state"
"comment"
"req_by" = "web"
*/
 
